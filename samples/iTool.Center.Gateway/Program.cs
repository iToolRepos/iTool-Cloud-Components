﻿// See https://aka.ms/new-console-template for more information
using System.Buffers;

using iTool.Center.Gateway;
using iTool.Cloud.Center.Model;
using iTool.Clustering.Center;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

Console.WriteLine("Hello, World!");
try
{
    if (System.IO.File.Exists("./iToolCloudCenter.storage"))
    {
        System.IO.File.Delete("./iToolCloudCenter.storage");
    }

    //await Class1.TraceMessage("asd");

    //ArrayPool<byte> memoryPool = ArrayPool<byte>.Shared;
    //DateTime Now= DateTime.UtcNow;
    //Parallel.For(0,200_000, index =>
    //{
    //    var buff = memoryPool.Rent(102400);
    //    //var xxx = memoryOwner.Memory.ToArray();
    //    memoryPool.Return(buff);
    //    //var xx = new byte[102400];
    //});
    //Console.WriteLine((DateTime.UtcNow - Now).TotalMilliseconds);
    IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();

    var host = await iCenter.RegisteriToolCenterAsync(        new ServiceEndPointOptions(configuration, true), 
        new ClusterIdentificationOptions(configuration, true));

    var logger = host.GetService<ILogger<Exception>>();

    while (true)
    {
        var input = Console.ReadLine();
        if (string.IsNullOrWhiteSpace(input)) continue;
        if (input == "clear")
        {
            Console.Clear();
            continue;
        }

        //DateTime old = DateTime.Now;
        //Console.WriteLine("start:" + old);

        //Parallel.For(0,10_000, Index =>
        //{
        //    try
        //    {
        //        logger.LogError(Index, "test");
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //});

        //Console.WriteLine("end(ms):" + (DateTime.Now - old).TotalMilliseconds);


    }

    

    Console.ReadKey();
    Console.WriteLine("Stoped");

}
catch (Exception ex)
{
    Console.WriteLine(ex);
    Console.ReadKey();
}