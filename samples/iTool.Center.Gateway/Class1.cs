﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Center.Gateway
{
    public class Time<T> : Task<T>
    {
        public Time(Func<object?, T> function, object? state) : base(function, state)
        {
        }

        public static implicit operator Time<T>(string value)
        {
            // Initialize your object with value
            // Similar to 
            var values = value.Split(':');
            var hour = Convert.ToInt32(values[0]);
            var min = Convert.ToInt32(values[1]);
            return new Time<T>(x => { return (T)x; },min);
        }
    }

    internal class Class1
    {
        public static void TraceMessage(string message,
    [CallerMemberName] string memberName = "",
    [CallerFilePath] string sourceFilePath = "",
    [CallerLineNumber] int sourceLineNumber = 0)
        {

            Console.WriteLine($"message:{message}\nmember name: {memberName}" +
                $"\nsource file path: {sourceFilePath}\nsource line number: {sourceLineNumber}");
        }
    }
}
