﻿using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common.Options;
using iToolService.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MultipleService.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var task = Task.WhenAll(new List<Task> { RunMainAsync() });
            task.Wait();
            Console.ReadKey();
        }

        private static async Task RunMainAsync()
        {
            try
            {
                var builder = new iToolClientBuilder();

                builder.UseAdoNetClustering(new AdoNetClusterOptions
                {
                    AdoNetOptions = new AdoNetOptions
                    {
                        DataSource = "DESKTOP-L0F4SGU",

                        //DataSource = "127.0.0.1,2433",
                        //UID = "sa",
                        //PWD = "zhuJIAN320"
                    },
                    ClusterOptions = new ClusterIdentificationOptions(),
                    ResponseTimeout = TimeSpan.FromSeconds(15)
                });

                var cluster = await builder.BuildAndConnectAsync();

                Console.WriteLine("started successfully");


                // 指定服务激活颗粒并且调用
                //var serve1 = cluster.GetClusterNoder<IClusterNodeService>("172.29.96.1");
                //await serve1.InvokeNoResultAsync("xxxxxxxx");

                // 多激活
                var servermaster = cluster.GetService<ILongCacheMultipleMasterService>(123);
                var server2 = cluster.GetService<ILongCacheMultipleService>(123);

                var xxx = await server2.GetStateAsync();

                while (true)
                {
                    Console.WriteLine($"输入发布内容（\"exit\"退出）:");

                    var input = Console.ReadLine();

                    if (true)
                    {
                        Console.WriteLine($"开始发送的时间：{DateTime.Now}");

                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        List<Task> tasks = new List<Task>(100_0010);

                        Parallel.For(0, 1000, new ParallelOptions { MaxDegreeOfParallelism = 10 }, index =>
                        {
                            var task = server2.GetStateAsync();
                            tasks.Add(task);
                        });

                        try
                        {
                            await Task.WhenAll(tasks);


                            tasks = new List<Task>(100_0010);
                            Parallel.For(0, 10, new ParallelOptions { MaxDegreeOfParallelism = 10 }, index =>
                            {
                                var value = input + $",{DateTime.Now}";
                                var task = servermaster.SetStateAsync(value);
                                tasks.Add(task);
                            });
                            await Task.WhenAll(tasks);


                            await servermaster.SetStateAsync(input + "end state...");
                            var last = await server2.GetStateAsync();
                            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(last));

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("error:" + ex.Message);
                        }

                        sw.Stop();

                        Console.WriteLine($"发送完成时间：{DateTime.Now}, total:{sw.Elapsed.TotalMilliseconds}ms,{(2 / sw.Elapsed.TotalSeconds)}/second");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
