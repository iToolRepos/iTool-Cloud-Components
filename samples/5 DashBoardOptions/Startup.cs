using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common.Options;
using iTool.Dashboard.EmbeddedAssets;
using iToolService.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orleans;
using System;
using System.Threading.Tasks;

namespace _5_DashBoardOptions
{
    /// <summary>
    /// 可以直接启动，托管Host
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {

            // 托管 Host
            var builder = new iToolHostBuilder();
            builder.UseAdoNetClustering(new AdoNetClusterOptions
            {
                AdoNetOptions = new AdoNetOptions
                {
                    DataSource = "127.0.0.1,2433",
                    UID = "sa",
                    PWD = "zhuJIAN320"
                },
                EndpointsOptions = new EndpointsOptions(),
                ClusterOptions = new ClusterIdentificationOptions(),
                ResponseTimeout = TimeSpan.FromSeconds(15)
            });

            builder.BuildAndStartAsync(services).Wait();

            //Task.Run(async () => {
            //    var push = clusterService.GetService<IPushService>("zhangxiaosong1");
            //    for (int i = 0; i < 10000; i++)
            //    {
            //        await push.Push("asd_"+i);
            //        await Task.Delay(100);
            //    }
            //});

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseDashboard("/cluster");
            app.UseIframeDashboard("/cluster");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
