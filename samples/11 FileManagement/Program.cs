using System.Drawing.Drawing2D;
using System.IO;
using System.Security.Cryptography;
using System.Text;

using iTool.Cloud.Center.Model;
using iTool.Cloud.Database.ServiceProvider;
using iTool.ClusterComponent;

using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;

IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();

var iToolBuilder = new iToolClientBuilder();

var clusterOptions = new ClusteringStaticOptions(
    new ServiceEndPointOptions(configuration, true),
    new ClusterIdentificationOptions(configuration, true));

iToolBuilder.UseiCenterClustering(new ClusterIdentificationOptions(), clusterOptions);
iToolClusterHostClient cluster = await iToolBuilder.BuildAndConnectAsync();

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/Storage/{id}/view", async (string id) =>
{
    iFileService iFileService = cluster.GetService<iFileService>(id);
    var info = await iFileService.GetFileInfoAsync();

    MemoryStream fileStream = new MemoryStream(info.TotalLength);

    if (info.UploadState == 200)
    {
        var file = await iFileService.GetStreamAsync();
        await fileStream.WriteAsync(file.FileStream, 0, file.FileStream.Length);
    }
    else if (info.UploadState == 201)
    {
        int page = 0;
        while (true)
        {
            page++;
            var file = await iFileService.GetStreamAsync(page);
            await fileStream.WriteAsync(file.FileStream, 0, file.FileStream.Length);
            if (file.IsEndNUmber)
            {
                break;
            }
        }
    }
    return Results.Stream(fileStream, contentType: info.ContentType);
});

app.MapGet("/Storage/{id}", async (string id) =>
{
    iFileService iFileService = cluster.GetService<iFileService>(id);
    var info = await iFileService.GetFileInfoAsync();

    MemoryStream fileStream = new MemoryStream(info.TotalLength);

    if (info.UploadState == 200)
    {
        var file = await iFileService.GetStreamAsync();
        await fileStream.WriteAsync(file.FileStream, 0, file.FileStream.Length);
    }
    else if (info.UploadState == 201)
    {
        int page = 0;
        while (true)
        {
            page++;
            var file = await iFileService.GetStreamAsync(page);
            await fileStream.WriteAsync(file.FileStream, 0, file.FileStream.Length);
            if (file.IsEndNUmber)
            {
                break;
            }
        }
    }
    return Results.File(fileStream, contentType: info.ContentType);
});

app.MapPost("/Storage", async Task<IResult> (HttpRequest request) =>
{
    if (!request.HasFormContentType)
    {
        return Results.NoContent();
    }

    var form = await request.ReadFormAsync();
    var file = form.Files["file"];
    if (file is null || file.Length == 0)
        return Results.NoContent();

    await using var stream = file.OpenReadStream();
    {
        // Step 1 获取文件Key
        var retVal = MD5.Create().ComputeHash(stream);
        StringBuilder stringBuilder = new StringBuilder();
        foreach (var item in retVal)
        {
            stringBuilder.Append(item.ToString("x2"));
        }
        string fileKey = stringBuilder.ToString();

        // Step 2 获取Service
        iFileService iFileService = cluster.GetService<iFileService>(fileKey);
        if (await iFileService.IsExistsAsync())
        {
            // 文件已经存在
            return Results.Ok(fileKey);
        }

        // Step 3 定义缓冲区
        int bufCount = 1024 * 16;
        byte[] bufs = new byte[stream.Length > bufCount ? bufCount : stream.Length];

        {
            // 如果文件小于缓冲区大小，则直接提交
            if (stream.Length > bufCount)
            {
                await stream.ReadAsync(bufs, 0, (int)stream.Length);
            }

            await iFileService.UploadAsync(new UploadInfo
            {
                CreateDate = DateTime.Now,
                FileStream = stream.Length > bufCount ? new byte[0] : bufs,
                Role = "admin",
                User = "zxf",
                SuffixName = Path.GetExtension(file.FileName),
                TotalLength = stream.Length > bufCount ? 0 : bufs.Length
            });
        }

        // 大文件分片上传
        if (stream.Length > bufCount)
        {
            int index = 0, streamLength = (int)stream.Length;

            // 分片
            while (await stream.ReadAsync(bufs, 0, bufCount) > 0)
            {
                index++;
                await iFileService.UploadPieceAsync(new UploadPiece
                {
                    Number = index,
                    FileStream = bufs,
                    IsEndNUmber = false
                });
            }

            // 最后一页
            await iFileService.UploadPieceAsync(new UploadPiece
            {
                Number = index+1,
                FileStream = bufs,
                IsEndNUmber = true
            });

            await iFileService.UploadComplatedAsync();
        }

        return Results.Ok(fileKey);
    }
});

app.Run();
