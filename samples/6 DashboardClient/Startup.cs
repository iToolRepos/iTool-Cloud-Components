using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common.Options;
using iTool.Dashboard.EmbeddedAssets;
using iToolService.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace _6_DashboardClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = new iToolClientBuilder();

            builder.UseAdoNetClustering(new AdoNetClusterOptions
            {
                AdoNetOptions = new AdoNetOptions
                {
                    DataSource = "test.gateway.itool.store,2533",
                    UID = "sa",
                    PWD = "zhuJIAN30.3.."
                },
                //ClusterOptions = new ClusterIdentificationOptions(),
                ClusterOptions = new ClusterIdentificationOptions
                {
                    ClusterId = "iToolServiceCluster",
                    ServiceId = "iToolService"
                },
                ResponseTimeout = TimeSpan.FromSeconds(15)
            });

            builder.BuildAndConnectAsync(services).Wait();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDashboard();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
