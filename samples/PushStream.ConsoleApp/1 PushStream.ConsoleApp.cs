﻿using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common.Options;
using iToolService.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PushStream.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var task = Task.WhenAll(new List<Task> { RunMainAsync() });
            task.Wait();
            Console.ReadKey();
        }

        private static async Task RunMainAsync()
        {
            try
            {
                var builder = new iToolClientBuilder();

                builder.UseAdoNetClustering(new AdoNetClusterOptions
                {
                    AdoNetOptions = new AdoNetOptions
                    {
                        DataSource = "DESKTOP-L0F4SGU",

                        //DataSource = "127.0.0.1,2433",
                        //UID = "sa",
                        //PWD = "zhuJIAN320"
                    },
                    ClusterOptions = new ClusterIdentificationOptions(),
                    ResponseTimeout = TimeSpan.FromSeconds(15)
                });

                builder.UseStreamProvider("TestStream", 20);

                var cluster = await builder.BuildAndConnectAsync();

                Console.WriteLine("started successfully");

                //var handler1 = new ProducerStreamHandler(cluster.GetQueueProvider("TestStream"), "zhangxiaosong", "test1");

                var handler1 = new ProducerQueueHandler<string>("zhangxiaosong", "test1");

                // 通过颗粒推送消息
                var server = cluster.GetService<IPushService>("zhangxiaosong");


                // var server2 = cluster.GetService<ISubscribeService>("zhangxiaosong");
                // await server2.Ping();
                // await server.Push($",{DateTime.Now}");

                while (true)
                {
                    Console.WriteLine($"输入发布内容（\"exit\"退出）:");

                    var input = Console.ReadLine();

                    if (true)
                    {
                        Console.WriteLine($"开始发送的时间：{DateTime.Now}");

                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        List<Task> tasks = new List<Task>(100_0010);

                        Parallel.For(0, 1, new ParallelOptions { MaxDegreeOfParallelism = 10 }, index =>
                        {
                            if (input == "exit") return;

                            // 发布消息
                            // var task = handler1.SendMessageAsync(input + $",{DateTime.Now}");
                            var task = server.Push(input + $",{DateTime.Now}");
                            tasks.Add(task);
                            task = server.Push("xxx1",input + $",{DateTime.Now}");
                            tasks.Add(task);
                            //task = handler2.SendMessageAsync(input + $",{DateTime.Now}");
                            //tasks.Add(task);
                            //task = handler3.SendMessageAsync(input + $",{DateTime.Now}");
                            //tasks.Add(task);
                        });

                        try
                        {
                            await Task.WhenAll(tasks);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("error:" + ex.Message);
                        }

                        sw.Stop();

                        Console.WriteLine($"发送完成时间：{DateTime.Now}, total:{sw.Elapsed.TotalMilliseconds}ms,{(2 / sw.Elapsed.TotalSeconds)}/second");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
