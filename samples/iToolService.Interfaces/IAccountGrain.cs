﻿using System.Threading.Tasks;
using iTool.ClusterComponent;
using Orleans;

namespace iToolService.Interfaces
{
    public interface IAccountGrain : iToolServiceWithStringKey
    {
        [Transaction(TransactionOption.Join)]
        Task Withdraw(uint amount);

        [Transaction(TransactionOption.Join)]
        Task Deposit(uint amount);

        [Transaction(TransactionOption.CreateOrJoin)]
        Task<uint> GetBalance();
    }
}
