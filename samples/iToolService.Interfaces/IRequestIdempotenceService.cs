﻿using iTool.ClusterComponent;
using Orleans.Concurrency;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface IRequestIdempotenceService : iToolServiceWithIntegerCompoundKey
    {
        Task<bool> StartIfNotExistAsync(string token, int timeout);

        [AlwaysInterleave]
        Task SetResultAsync(string token, object result);

        [AlwaysInterleave]
        Task<object> GetResultAsync(string token);
    }
}
