using iTool.ClusterComponent;
using System.Threading.Tasks;

namespace HelloWorld.Interfaces
{
    /// <summary>
    /// Orleans grain communication interface IHello
    /// </summary>
    public interface IHello : iToolServiceWithStringKey
    {
        Task<T> SayHello<T>(T greeting);
    }
}
