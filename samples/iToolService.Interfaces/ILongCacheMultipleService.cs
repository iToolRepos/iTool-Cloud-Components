﻿using iTool.ClusterComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    /// <summary>
    /// 负责写
    /// </summary>
    public interface ILongCacheMultipleMasterService : iToolServiceMultipleMasterWithIntegerKey
    {
        Task SetStateAsync(string value);
    }

    /// <summary>
    /// 负责读
    /// </summary>
    public interface ILongCacheMultipleService : iToolServiceMultipleMasterWithIntegerKey
    {
    }

    public class MultipleStateWithInteger
    {
        public MultipleStateWithInteger() { }
        public MultipleStateWithInteger(long key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
        public long Key { get; set; }
        public string Value { get; set; }
    }
}
