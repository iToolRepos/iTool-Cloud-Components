﻿using iTool.ClusterComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface IStorageService : iToolServiceWithStringKey
    {
        Task<string> GetState();
        Task Remove();
        Task Modify(string value);
    }
}
