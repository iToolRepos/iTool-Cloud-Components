﻿using iTool.Common.CloudStateEntity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface IReminderService : iTool.ClusterComponent.iToolService
    {
        /// <summary>
        /// 开始一个计划任务
        /// </summary>
        /// <param name="reminderName">任务名称</param>
        /// <param name="moduleName">任务云函数</param>
        /// <param name="periodOfMinutes">间隔时间周期</param>
        /// <returns></returns>
        Task DoSomethingThatTriggersReminder(string reminderName, string moduleName, double periodOfMinutes);

        /// <summary>
        /// 卸载指定计划任务
        /// </summary>
        /// <param name="reminderName"></param>
        /// <returns></returns>
        Task UnregisterReminder(string reminderName);

        /// <summary>
        /// 获取所有的计划任务
        /// </summary>
        /// <returns></returns>
        Task<ConcurrentDictionary<string, ReminderInfo>> GetReminderList();

        /// <summary>
        /// 指定云函数是否在计划任务中
        /// </summary>
        /// <returns></returns>
        Task<bool> IsExistByModulePath(string modulePath);

        /// <summary>
        /// 指定计划任务名称是否在计划任务中
        /// </summary>
        /// <param name="reminderName"></param>
        /// <returns></returns>
        Task<bool> IsExistByReminderName(string reminderName);
    }
}
