﻿using iTool.ClusterComponent;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface ISubscribeService : iToolServiceWithStringKey
    {
        Task Ping();
    }
}
