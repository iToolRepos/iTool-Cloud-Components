﻿using iTool.ClusterComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    /// <summary>
    /// 负责写
    /// </summary>
    public interface IStringCacheMultipleMasterService : iToolServiceMultipleMasterWithStringKey
    {
        Task SetStateAsync(string value);
    }

    /// <summary>
    /// 负责读
    /// </summary>
    public interface IStringCacheMultipleService : iToolServiceMultipleMasterWithStringKey
    {
    }

    public class MultipleStateWithString
    {
        public MultipleStateWithString() { }
        public MultipleStateWithString(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
