﻿using iTool.ClusterComponent;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface IPushService : iToolServiceWithStringKey
    {
        Task Push(string value);
        Task Push(string topic, string value);
    }
}
