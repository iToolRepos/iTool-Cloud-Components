﻿using iTool.ClusterComponent;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    public interface IClusterNodeService : iToolServiceWithNoder
    {
        Task InvokeNoResultAsync(string parameter);
        Task<string> InvokeAsync(string parameter);
    }
}
