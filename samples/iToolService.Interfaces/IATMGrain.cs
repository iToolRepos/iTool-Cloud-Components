using System;
using System.Threading.Tasks;
using iTool.ClusterComponent;
using Orleans;

namespace iToolService.Interfaces
{
    public interface IATMGrain : iToolServiceWithIntegerKey
    {
        [Transaction(TransactionOption.Create)]
        Task Transfer(string fromAccount, string toAccount, uint amountToTransfer);
    }
}
