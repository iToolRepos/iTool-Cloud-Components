﻿using iTool.ClusterComponent;
using Orleans.Streams;
using System;
using System.Threading.Tasks;

namespace SubStream.ConsoleApp
{

    public class TestSubscribeStreamHandler : SubscribeQueueHandler<string>
    {
        string topic;
        public TestSubscribeStreamHandler(string topic, string streamNamespace) 
            : base(topic, streamNamespace)
        {
            this.topic = topic;
        }

        public TestSubscribeStreamHandler(IQueueProvider provider, string topic, string streamNamespace)
            : base(provider, topic, streamNamespace, false)
        {

        }

        public override Task OnErrorAsync(Exception ex)
        {
            Console.WriteLine("OnErrorAsync:" + ex.Message);
            return Task.CompletedTask;
        }

        public async override Task OnMessageAsync(string message, StreamSequenceToken token)
        {
            try
            {
                //await Task.Delay(500);
                if (token == null)
                {
                    Console.WriteLine($"topic:{this.topic},message:{message}");
                }
                else
                {
                    var key = $"{token.SequenceNumber}_{token.EventIndex}";
                    Console.WriteLine($"topic:{this.topic},message:{message},SequenceNumber:{token.SequenceNumber},EventIndex：{token.EventIndex}");
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            await Task.CompletedTask;
        }
    }

}
