﻿using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SubStream.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var task = Task.WhenAll(new List<Task> { RunMainAsync() });
            task.Wait();
            Console.ReadKey();
        }

        private static async Task RunMainAsync()
        {
            try
            {
                var builder = new iToolClientBuilder();

                builder.UseAdoNetClustering(new AdoNetClusterOptions
                {
                    AdoNetOptions = new AdoNetOptions
                    {
                        DataSource = "DESKTOP-L0F4SGU",

                        //DataSource = "127.0.0.1,2433",
                        //UID = "sa",
                        //PWD = "zhuJIAN320"
                    },
                    ClusterOptions = new ClusterIdentificationOptions(),
                    ResponseTimeout = TimeSpan.FromSeconds(15)
                });

                builder.UseStreamProvider("TestStream", 20);

                var cluster = await builder.BuildAndConnectAsync();



                Console.WriteLine("started successfully");
                //var subscribeStreamHandler = new TestSubscribeStreamHandler(clusterHostClient, "zhangxiaosong", "test1", "TestStream");

                //var handler1 = new TestSubscribeStreamHandler(cluster.GetQueueProvider("TestStream"), "zhangxiaosong", "test1");

                var subscribeStreamHandler = new TestSubscribeStreamHandler("zhangxiaosong", "user");
                await subscribeStreamHandler.StartAsync();

                var subscribeStreamHandler1 = new TestSubscribeStreamHandler("xxx1", "user");
                await subscribeStreamHandler1.StartAsync();

                Console.WriteLine("subscribeStreamHandler successfully");

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
