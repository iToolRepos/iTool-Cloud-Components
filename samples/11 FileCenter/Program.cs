

using iTool.Cloud.Center.Logger;
using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;

IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();

var iToolBuilder = new iToolClientBuilder();

var clusterOptions = new ClusteringStaticOptions(
    new ServiceEndPointOptions(configuration, true),
    new ClusterIdentificationOptions(configuration, true));

iToolBuilder.UseiCenterClustering(new ClusterIdentificationOptions(), clusterOptions);
iToolClusterHostClient cluster = await iToolBuilder.BuildAndConnectAsync();


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton(cluster);
builder.Services.AddLogging(build => build.AddCenterLogger(config => 
{
    config.ProviderName = "FileCenter";
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
