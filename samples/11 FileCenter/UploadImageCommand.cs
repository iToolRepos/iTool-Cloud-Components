﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Models.Commands
{
    public class UploadImageCommand
    {
        public List<IFormFile> File { get; set; }
    }
}
