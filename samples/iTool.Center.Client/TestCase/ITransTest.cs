﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using iTool.ClusterComponent;

using iToolService.Interfaces;

namespace iTool.Center.Client.TestCase
{
    internal class ITransTest
    {
        public async static Task Start(iToolClusterHostClient cluster)
        {
            IATMGrain atm = cluster.GetService<IATMGrain>(0);
            string from = "from";
            string to = "to";
            await atm.Transfer(from, to, 100);
            uint fromBalance = await cluster.GetService<IAccountGrain>(from).GetBalance();
            uint toBalance = await cluster.GetService<IAccountGrain>(to).GetBalance();
            Console.WriteLine($"\n\nWe transfered 100 credits from {from} to {to}.\n{from} balance: {fromBalance}\n{to} balance: {toBalance}\n\n");
        }
    }
}
