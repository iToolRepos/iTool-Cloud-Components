﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using iTool.Cloud.Database.ServiceProvider;
using iTool.ClusterComponent;

namespace iTool.Center.Client.TestCase
{
    internal class iFileServiceTest
    {
        public async static Task Start(iToolClusterHostClient cluster)
        {
            Console.WriteLine("请输入文件信息: " + DateTime.Now);
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "exit") break;
                if (!string.IsNullOrWhiteSpace(input))
                {

                    if (input.StartsWith("upload:"))
                    {
                        input = input.Substring(7);
                        var fileStream = File.ReadAllBytes(input);
                        var retVal = MD5.Create().ComputeHash(fileStream);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < retVal.Length; i++)
                        {
                            sb.Append(retVal[i].ToString("x2"));
                        }

                        string fileKey = sb.ToString();

                        Console.WriteLine("开始上传: " + DateTime.Now);
                        iFileService iFileService = cluster.GetService<iFileService>(fileKey);

                        if (!await iFileService.IsExistsAsync())
                        {
                            int bufCount = 1024 * 8; // 8kb 一篇

                            // 一次上传
                            if (fileStream.Length <= bufCount)
                            {
                                await iFileService.UploadAsync(new UploadInfo
                                {
                                    CreateDate = DateTime.Now,
                                    FileStream = fileStream,
                                    Role = "admin",
                                    User = "zxf",
                                    SuffixName = Path.GetExtension(input),
                                    TotalLength = fileStream.Length,
                                });
                            }
                            // 分页上传
                            else
                            {
                                await iFileService.UploadAsync(new UploadInfo
                                {
                                    CreateDate = DateTime.Now,
                                    Role = "admin",
                                    User = "zxf",
                                    SuffixName = Path.GetExtension(input),
                                    TotalLength = fileStream.Length,
                                });

                                int index = 0;
                                byte[] bufs = new byte[bufCount];
                                for (int i = 0; i < fileStream.Length; i += bufCount)
                                {
                                    index++;
                                    bool isEndNumber = ((index - 1) * bufCount + bufCount) >= fileStream.Length;
                                    int takeLength = bufCount;

                                    if (isEndNumber)
                                    {
                                        takeLength = bufCount - (((index - 1) * bufCount + bufCount) - fileStream.Length);
                                    }

                                    await iFileService.UploadPieceAsync(new UploadPiece
                                    {
                                        Number = index,
                                        FileStream = isEndNumber ?
                                            fileStream.Skip((index - 1) * bufCount).Take(takeLength).ToArray()
                                            : fileStream.Skip((index - 1) * bufCount).Take(takeLength).ToArray(),
                                        IsEndNUmber = isEndNumber
                                    });
                                }

                                await iFileService.UploadComplatedAsync();
                            }

                        }
                        Console.WriteLine("上传成功: " + DateTime.Now + "-- " + fileKey);
                    }
                    else
                    {

                        iFileService iFileService = cluster.GetService<iFileService>(input);
                        if (await iFileService.IsExistsAsync())
                        {
                            Console.WriteLine("文件存在");
                            var info = await iFileService.GetFileInfoAsync();
                            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(info, Newtonsoft.Json.Formatting.Indented));

                            Console.WriteLine("开始下载");

                            FileStream fileStream1 = File.Create(string.Format(@"M:\iTool.ClusterComponent\samples\iTool.Center.Server\bin\Debug\net6.0\Upload\{0}{1}", input, info.SuffixName), info.TotalLength);

                            if (info.UploadState == 200)
                            {
                                var fileStream = await iFileService.GetStreamAsync();
                                await fileStream1.WriteAsync(fileStream.FileStream, 0, fileStream.FileStream.Length);
                            }
                            else if (info.UploadState == 201)
                            {
                                int page = 0;
                                while (true)
                                {
                                    page++;
                                    var fileStream = await iFileService.GetStreamAsync(page);
                                    await fileStream1.WriteAsync(fileStream.FileStream, 0, fileStream.FileStream.Length);
                                    if (fileStream.IsEndNUmber)
                                    {
                                        break;
                                    }
                                }
                            }

                            await fileStream1.FlushAsync();
                            fileStream1.Dispose();

                            Console.WriteLine("下载成功");
                        }
                        else
                        {
                            Console.WriteLine("文件不存在");
                        }
                    }
                }
            }
        }
    }
}
