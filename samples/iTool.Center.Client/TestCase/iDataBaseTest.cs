﻿using iTool.Cloud.Database;
using iTool.Cloud.DataSearch;
using iTool.ClusterComponent;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using Newtonsoft.Json;

namespace iTool.Center.Client.TestCase
{
    internal class iDataBaseTest
    {
        public async static Task Start(iToolClusterHostClient cluster) 
        {
            var executor = cluster.GetSqlExecutor();

            //await executor.ExecuteNonQueryNoResultAsync("delete locations");

            //await executor.ExecuteNonQueryAsync(CityGeoInfo.GetCityGeoInfos().First().ToString());

            await executor.ExecuteNonQueryAsync("update locations set city='义乌啦' where search(city) like '义乌'");

            await executor.ExecuteNonQueryAsync("delete locations where search(city) like '合肥'");
            await executor.ExecuteNonQueryAsync("delete locations where search(city) like '三*'");
            await executor.ExecuteNonQueryAsync("delete locations where city = '武汉'");

            await Parallel.ForEachAsync(CityGeoInfo.GetCityGeoInfos(), async (item, canceltoken) =>
            {
                await executor.ExecuteNonQueryNoResultAsync(item.ToString());
            });

            Console.Clear();

            while (true)
            {
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input)) continue;
                if (input == "clear")
                {
                    Console.Clear();
                    continue;
                }

                DateTime old = DateTime.Now;
                Console.WriteLine("start:" + old);
                if (input.StartsWith("select"))
                {
                    //var lastResult = "";
                    await Parallel.ForEachAsync(new byte[1], async (i, c) =>
                    {
                        try
                        {
                            var lastResult = await executor.ExecuteReaderAsync(input);
                            Console.WriteLine("total:"+lastResult.total);
                            Console.WriteLine("token:" + lastResult.token);
                            Console.WriteLine("list:" + lastResult.data);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                    //Console.WriteLine(lastResult);
                    //Console.WriteLine(lastResult.Length);

                }
                else if (input.StartsWith("t- select"))
                {
                    input = input.Substring(3);
                    object lastResult = null;
                    await Parallel.ForEachAsync(new byte[1], async (i, c) =>
                    {
                        try
                        {
                            lastResult = await executor.ExecuteReaderAsync<UserTable>(input);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });

                    Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(lastResult, Formatting.Indented));
                }
                else if (input.StartsWith("begin:"))
                {
                    input = input.Substring(6);
                    var list = new List<iTool.Cloud.Database.Options.ExecuteItemOptions>();
                    foreach (var sql in input.Split(';'))
                    {
                        list.Add(new iTool.Cloud.Database.Options.ExecuteItemOptions
                        {
                            Sql = sql
                        });
                    }

                    await Parallel.ForEachAsync(new byte[1000], async (i, c) =>
                    {
                        //await executor.ExecuteTransactionAsync(list);
                        await executor.ExecuteTransactionOfLockTableAsync(list);
                    });


                }
                else
                {
                    await Parallel.ForEachAsync(new byte[1], async (i, c) =>
                    {
                        try
                        {
                            var result = await executor.ExecuteNonQueryAsync(input);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                }

                Console.WriteLine("end(ms):" + (DateTime.Now - old).TotalMilliseconds);
                Console.WriteLine("end:" + DateTime.Now);


            }
        }
    }
}
