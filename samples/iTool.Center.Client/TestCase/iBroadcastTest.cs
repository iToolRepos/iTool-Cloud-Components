﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using iTool.ClusterComponent;
using iTool.Common;

using iToolService.Interfaces;

namespace iTool.Center.Client.TestCase
{
    internal class iBroadcastTest
    {
        public async static Task Start(iToolClusterHostClient cluster) 
        {
            // 获取所有server
            iPrint.Line("\n获取所有server:");
            var management = cluster.GetManagementer();
            while (true)
            {
                Console.WriteLine($"get:");
                var input = Console.ReadLine();
                if (input == "exit") break;
                var result = await management.GetHosts(true);

                // 指定服务激活
                //var arr = input.Split('$');
                //if (arr.Length > 1)
                //{
                //    var parsable = result.Select(item => item.Key.ToParsableString()).Where(item => item.Contains(arr[0])).FirstOrDefault();
                //    if (parsable != null) {
                //        var noder = cluster.GetServiceByClusterNoder<IClusterNodeService>(parsable);
                //        await noder.InvokeAsync(input);
                //    }
                //}

                DateTime old = DateTime.Now;

                // 广播
                var noders = result
                    .Select(item =>
                        cluster
                            .GetServiceByClusterNoder<IClusterNodeService>(item.Key.ToParsableString()));

                List<Task> no_hosts = new List<Task>(10_001);
                List<Task<string>> hosts = new List<Task<string>>(10_001);
                Parallel.For(0, 10_000, index =>
                {
                    //hosts = hosts.Concat(noders.Select(item => item.InvokeAsync(input+index))).ToList();
                    no_hosts = no_hosts.Concat(noders.Select(item => item.InvokeNoResultAsync(input + index))).ToList();
                });

                await Task.WhenAll(no_hosts);

                Console.WriteLine((old - DateTime.Now).TotalMilliseconds + "ms");

                //Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            }
        }
    }
}
