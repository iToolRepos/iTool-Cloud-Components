﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using iTool.ClusterComponent;
using iTool.Common;

namespace iTool.Center.Client.TestCase
{
    internal class iPlanTaskServiceTest
    {
        public async static Task Start(iToolClusterHostClient cluster)
        {
            iPrint.Line("\n计划任务:");
            var reminder = cluster.GetService<iToolService.Interfaces.IReminderService>();
            await reminder.DoSomethingThatTriggersReminder("test", "modulename", 1);
        }
    }
}
