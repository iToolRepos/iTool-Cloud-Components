﻿using iTool.ClusterComponent;
using iTool.Common;

using iToolService.Interfaces;

namespace iTool.Center.Client.TestCase
{
    internal class IPushServiceTest
    {
        public async static Task Start(iToolClusterHostClient cluster) 
        {
            iPrint.Line("\n推流:");
            var server = cluster.GetService<IPushService>("zhangxiaosong");
            while (true)
            {
                Console.WriteLine($"输入发布内容（\"exit\"退出）:");

                var input = Console.ReadLine();

                if (input == "exit") break;

                await server.Push(input + $",{DateTime.Now}");
                await server.Push("xxx1", input + $",{DateTime.Now}");
            }
        }
    }
}
