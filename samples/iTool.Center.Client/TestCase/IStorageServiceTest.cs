﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using iTool.ClusterComponent;
using iTool.Common;

using iToolService.Interfaces;

namespace iTool.Center.Client.TestCase
{
    internal class IStorageServiceTest
    {
        public async static Task Start(iToolClusterHostClient cluster) 
        {
            iPrint.Line("\n状态:");
            var storageService = cluster.GetService<IStorageService>("testSorage");
            while (true)
            {
                var input = Console.ReadLine();

                if (input == "exit") break;

                if (input?.StartsWith("set ") == true)
                {
                    await storageService.Modify(input.Substring(4));
                }
                else
                {
                    string statevalue = await storageService.GetState();
                    iPrint.Line("iPrint:(testSorage)=" + statevalue);
                }
            }
        }
    }
}
