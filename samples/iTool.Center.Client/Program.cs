﻿// See https://aka.ms/new-console-template for more information
using iTool.Center.Client.TestCase;
using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;

using Microsoft.Extensions.Configuration;

try
{
    IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();

    var builder = new iToolClientBuilder();

    var clusterOptions = new ClusteringStaticOptions(
        new ServiceEndPointOptions(configuration, true),
        new ClusterIdentificationOptions(configuration, true));

    builder.UseiCenterClustering(new ClusterIdentificationOptions(),clusterOptions);

    var cluster = await builder.BuildAndConnectAsync();

    // database
    await iDataBaseTest.Start(cluster);

    // file
    // iFileServiceTest.Start(cluster);

    // 状态
    //IStorageServiceTest.Start(cluster);

    // 通知 
    //iPlanTaskServiceTest.Start(cluster);

    // 内存流 堆栈溢出
    //IPushServiceTest.Start(cluster);

    //事务  堆栈溢出?只增不减
    //ITransTest.Start(cluster);

    // 广播
    //iBroadcastTest.Start(cluster);

    Console.ReadKey();
    Console.WriteLine("Stoped");

}
catch (Exception ex)
{
    File.AppendAllText("./error.txt",ex.Message);
    Console.WriteLine(ex);
    Console.ReadKey();
}
