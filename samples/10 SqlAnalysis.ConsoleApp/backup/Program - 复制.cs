﻿//// See https://aka.ms/new-console-template for more information
//using System.Collections.Generic;
//using System.Text;

//using ConsoleTest;

//using iTool.Cloud.Database.ServiceProvider;
//using iTool.Cloud.Database.SqlMaster.Test;
//using iTool.Cloud.Database.SqlStructureProvider;
//using iTool.SQL.Analysis;
//using iTool.SQL.Analysis.Context;

//using Microsoft.Data.Sqlite;

//{
//    //List<int> list = new List<int>(1_000_000 + 1);
//    //for (int i = 0; i < 1_000_000; i++)
//    //{
//    //    list.Add(i);
//    //}

//    //ISqlStructureProvider sqlStructureProvider = new SqliteStructureProvider("test", new SqliteConnectionStringBuilder
//    //{
//    //    DataSource = $"./test.master",
//    //    Mode = SqliteOpenMode.ReadWriteCreate,
//    //    Cache = SqliteCacheMode.Shared,
//    //    Pooling = true
//    //});

//    //DateTime before = DateTime.Now;
//    //DateTime _before = DateTime.Now;

//    //try
//    //{
//    //    await Parallel.ForEachAsync(list, new ParallelOptions { MaxDegreeOfParallelism = 32 }, async (index, cancelToken) =>
//    //    {
//    //        if (index % 10000 == 0)
//    //        {
//    //            Console.WriteLine($"index: {index},{DateTime.Now},{(DateTime.Now - _before)}");
//    //            _before = DateTime.Now;
//    //        }
//    //        await sqlStructureProvider.GetAllFieldsAsync();
//    //    });
//    //}
//    //catch (Exception ex)
//    //{
//    //    Console.WriteLine(ex);
//    //}
//    //Console.WriteLine(DateTime.Now - before);
//    //Console.ReadKey();
//}
//{
//    //var test = new SqliteTest();
//    //int xxx =test.ExecuteNonQuery("update users set email='ade' where id in (101,102) ");
//    //test.InsertMultipleWithTransaction(100);
//    //test.InsertMultipleWithTransaction(1_000_000);
//    //await Task.Delay(2000);
//    //test.InsertMultipleWithTransaction(1_000_000);
//    //await Task.Delay(2000);
//    //test.InsertMultipleWithTransaction(1_000_000);
//    //await Task.Delay(2000);

//    // 查询所有Table
//    // [{"name":"Users"},{"name":"sqlite_sequence"}]
//    //var result = test.ExecuteQuery("select name from sqlite_master where type='table' order by name;");
//    //var result = test.ExecuteQuery("select * from Users");
//    //await test.ExecuteQueryAsync($"select * from Users where Email like '%testing%'");

//    //List<int> list = new List<int>(1_000_000+1);
//    //for (int i = 0; i < 1_000_000; i++)
//    //{
//    //    list.Add(i);
//    //}
//    //Console.WriteLine("list 1_000_000");

//    ////ThreadPool.SetMaxThreads(16, 16);// 设置线程池最大线程数量
//    //ThreadPool.SetMinThreads(35, 35);
//    //int workerThreads, completionPortThreads;

//    //ThreadPool.GetMaxThreads(out workerThreads, out completionPortThreads);// 获取线程池最大线程数量
//    //Console.WriteLine($"Max:{workerThreads},{completionPortThreads}");
//    //ThreadPool.GetMinThreads(out workerThreads, out completionPortThreads);
//    //Console.WriteLine($"Min:{workerThreads},{completionPortThreads}");
//    //Console.ReadKey();

//    //DateTime before = DateTime.Now;
//    //DateTime _before = DateTime.Now;

//    //foreach (var index in new int[100_000])
//    //{
//    //    await test.ExecuteQueryAsync($"select * from Users where Email like '%testing{index}%'");
//    //}

//    //test.InsertMultipleWithTransaction(100_000);

//    //try
//    //{
//    //    await Parallel.ForEachAsync(list, new ParallelOptions { MaxDegreeOfParallelism = 32 }, async (index, cancelToken) =>
//    //    {
//    //        if (index % 10000 == 0)
//    //        {
//    //            Console.WriteLine($"index: {index},{DateTime.Now},{(DateTime.Now - _before)}");
//    //            _before = DateTime.Now;
//    //        }
//    //        await test.ExecuteQueryAsync($"select * from Users where id = {index}").ConfigureAwait(false);
//    //        //await test.ExecuteQueryAsync($"select * from Users where Email like '%testing{index}%'").ConfigureAwait(false);
//    //    });
//    //}
//    //catch (Exception ex)
//    //{
//    //    Console.WriteLine(ex);
//    //}
//    //Console.WriteLine(DateTime.Now - before);
//    //Console.ReadKey();

//    // 查询所有列
//    //{
//    //    "cid":1,
//    //    "name":"Username",
//    //    "type":"NVARCHAR(64)",
//    //    "notnull":1,
//    //    "dflt_value":null,
//    //    "pk":0
//    //}
//    //result = test.ExecuteQuery("PRAGMA table_info(users)");

//    //Console.WriteLine(result);
//}
//{
//    string sql;
//    {
//        // 获取单表信息
//        // 获取表名称    *
//        // 获取字段      *
//        // 获取条件
//        // 获取 join 条件
//        // 重构查询
//    }


//    {
//        sql = "update users set id = 1 where name = 'a'";
//        sql = "delete users where name = 'a'";
//        sql = "insert into users(id,name)values(1,'jian')";

//        sql = @"select distinct top 100 id,sum(age) a_age from users tu 
//                union 
//                    select id,sum(age) a_age from users tu 
//                inner join 
//                    orders o1 on o1.userid = tu.id
//                left join 
//                    orders o2 on o2.userid = tu.id
//                where
//                    id>0 and id <0 and id >= 0 and id <= 0 and id = 0
//                    or
//                    id in (1,2,3) and id in (select useridxx from orders)
//                    or
//                    id between 1 and 10
//                    or
//                    id like 'jian%' and id not like 'jian%'
//                    or
//                    id is null and id is not null
//                    or
//                    exists (select id from users where id=users.id)
//                    or
//                    search(id1,id2) like 'jian%'
//                order by id,age desc";

//        //having avg(id) > 0
//        //            limit 10 offset 20

//        //sql = @"select distinct top 100 id,sum(age) a_age from users tu 
//        //        where
//        //            id>0 and id <0 and id >= 0 and id <= 0 and id = 0
//        //            or
//        //            id in (1,2,3) and id in (select id from users)
//        //            or
//        //            id between 1 and 10
//        //            or
//        //            id like 'jian%' and id not like 'jian%'
//        //            or
//        //            id is null and id is not null
//        //            or
//        //            exists (select id from users where id=users.id)
//        //            or
//        //            search(id1,id2) like 'jian%'
//        //        group by id having sum(id) > 0
//        //        order by id,age desc";

//        //sql = "select *,id from users where id = @id";

//        sql = "select id,name from users left join orders od on od.userid = users.id where (od.id > 100 or od.tag = 'new') or od.tag2 = 'new2'";

//        //sql = "select id,name from users xx where xxx>0 and tag2 = 'new2' or (xxx>0 and tag2 = 'new2' or (xxx>0 and tag2 = 'new2'))";
//        sql = "select max(id) from users where id in (1,2,'3') and id like '%asd' or id not like '%asd' and id between 1 and 10 group by id,userid having sum(id) > 1 order by id desc";

//        sql = "select max(id) from users where id in (select userid from userold where users.id = userold.id)";

//        // select id from users = 1，2，3
//        // select id from userold where id in (1，2，3) = （1）
//        // users = max(1)

//        // 每次比如比较一页数据 等等 避免内存开销过大

//        //sql = "select id,name from users";

//        SqlAnalysisBase statement = SqlAnalysisFactory.CreateStatementAnalysisInstance(sql);
//        statement.Analysis();
//        var context = statement.GetSqlAnalysisContext();
//        foreach (var item in context.Errors)
//        {
//            Console.WriteLine(item);
//        }

//        if (context is SqlSelectAnalysisContext select)
//        {
//            var xxx = select.GetSqlScripts();
//        }

//        // 把Where 各种类型 写完 (in,like...)
//        // order by | group
//        // 解析 Join 条件，分析依赖关系
//        // 分析 子查询 依赖关系


//        string[] a_pages = new string[] { "a1_list", "a2_list", "a3_list" };
//        string[] b_pages = new string[] { "b1_list", "b2_list", "b3_list" };

//        // a_pages join b_pages
//        for (int i = 0; i < a_pages.Length; i++)
//        {
//            for (int bi = 0; bi < b_pages.Length; bi++)
//            {
//                // loop list
//                if (a_pages[i] == b_pages[bi])
//                {
//                    // true
//                }
//            }
            
//        }

//        Console.ReadKey();
//    }
//}

////Console.WriteLine("----------------------");
////{
////    string sql = "insert into users(name,age,createDate) values('jian',18,now())";
////    var result = SqlUtils.Parse(sql);
////    SqlUtils.IterateSqlNode(result.Script);
////}

//Console.ReadKey();