﻿using iTool.Cloud.NetCore;

namespace _8_CloudClient.ConsoleApp
{
    internal class iToolWebSocketConnection : AbstractiToolWebSocketConnection
    {
        public iToolWebSocketConnection(string gatewayHost) : base(gatewayHost)
        {
        }

        public override Task<string> AESEncryptAsync(string key, string input)
        {
            throw new NotImplementedException();
        }

        public override Task<(string platform, string type)> GetClientPlatformAndType()
        {
            return Task.FromResult(("android/ios/window", "browser/minapp/wchat"));
        }

        public override Task<T> GetLocalStorageAsync<T>(string key)
        {
            return Task.FromResult(default(T));
        }

        public override Task SetLocalStorageAsync(string key, string value)
        {
            return Task.CompletedTask;
        }
    }
}
