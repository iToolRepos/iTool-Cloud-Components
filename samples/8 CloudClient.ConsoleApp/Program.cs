﻿// See https://aka.ms/new-console-template for more information
using _8_CloudClient.ConsoleApp;
using iTool.Cloud.NetCore;
using iTool.Common.Responses;
using iTool.Utils;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Reactive.Subjects;

Console.WriteLine("Hello, World!");


iToolWebSocketConnection connection = new iToolWebSocketConnection("ws://gateway.itool.store/connector");
//iToolWebSocketConnection connection = new iToolWebSocketConnection("ws://127.0.0.1:2990/connector");
await connection.OpenAsync().ConfigureAwait(false);

DateTime start = DateTime.Now;
Console.WriteLine("getUserKey:{0}", start);

Console.ReadKey();
connection.AddMessageListener("1", msg => { 
    // 监听指定消息
});

connection.OnReceiveEvent += (e, msg) => 
{
    // 自定义监听消息
};



for (int i = 0; i < 200; i++)
{
    connection.invokeMethod("Layim/getUserKey").Then(msg => {
        global::System.Console.WriteLine("getUserKey 200 {0} - {1}", (start - DateTime.Now).TotalMilliseconds, msg);
    }).Cache(info => {
        global::System.Console.WriteLine("getUserKey 400 " + info);
    });


    Console.WriteLine("getUserInfo:{0}", DateTime.Now);

    connection
        .invokeMethod("Layim/getUserInfo")
        .Then(msg =>
        {
            global::System.Console.WriteLine("getUserInfo 200 {0} - {1}", (start - DateTime.Now).TotalMilliseconds, msg);
        })
        .Cache(info =>
        {
            global::System.Console.WriteLine("getUserInfo 400 " + info);
        });

    Thread.Sleep(1);
}




//Action sendFn = ()  =>
//{

//    if (connection.SendAsync(callfn).Result)
//    {
//        //Console.WriteLine("SendAsync success" + "-" + DateTime.Now);
//    }
//};

//connection.OnReceiveEvent += (e, msg) =>
//{
//    //lock (indexlock)
//    {
//        index++;
//        //if (index > 19990)
//        {

//            var entity = msg.TryToEntity<DoWorkResponse>();
//            global::System.Console.WriteLine("{0}-收到消息.time:{1}", index,DateTime.Now);
//        }
//        //global::System.Console.WriteLine($"{index}-收到消息：" + msg + "-" + DateTime.Now);
//    }

//};


//global::System.Console.WriteLine("开发发送时间：" + DateTime.Now);

//Parallel.For(0,1000, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i => {
//    sendFn();
//});


Console.ReadKey();
Console.WriteLine("end");
