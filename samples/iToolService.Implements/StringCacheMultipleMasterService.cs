﻿using iTool.ClusterComponent;
using iToolService.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Implements
{
    public class StringCacheMultipleService : iToolServiceMultipleWithStringKeyBase<MultipleStateWithString, IStringCacheMultipleMasterService>, IStringCacheMultipleService
    {
        public async Task<object> GetStateAsync()
        {
            await Task.Delay(100);
            return await this.StateAsync;
        }
    }

    public class StringCacheMultipleMasterService : iToolServiceMultipleMasterWithStringKeyBase<MultipleStateWithString>, IStringCacheMultipleService
    {
        public override Task<object> GetStateAsync()
        {
            return Task.FromResult<object>(this.State);
        }

        public async Task SetStateAsync(string value)
        {
            this.State.Value = value;
            await this.WriteStateAsync();
        }

        protected async override Task ClearStateAsync()
        {
            // 准备释放当前颗粒
            base.DeactivateOnIdle();

            // 通知释放所有缓存
            await base.NotifyDeactivateAsync();

            // clear state logic
        }

        protected override Task ReadStateAsync()
        {
            this.State = new MultipleStateWithString(this.GetStringKey(),"yyds");
            return Task.CompletedTask;
        }

        protected async override Task WriteStateAsync()
        {
            // write state logic

            await base.NotifyChangedStateAsync(this.State);
        }
    }
}
