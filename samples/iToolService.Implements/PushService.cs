﻿using iTool.ClusterComponent;
using iToolService.Interfaces;
using System.Threading.Tasks;

namespace iToolService.Implements
{
    public class PushService : iToolServiceProducerBase, IPushService
    {
        public PushService(): base(queueNamespace: "user") { }
        //public PushService(): base(queueNamespace: "user", providerName: "TestStream") { }
        public async Task Push(string value)
        {
            await this.SendMessageAsync(value);
            //await this.SendMessageAsync(topic:"xxx",value);
        }

        public async Task Push(string topic,string value)
        {
            await this.SendMessageAsync(topic, value);
            //await this.SendMessageAsync(topic:"xxx",value);
        }
    }
}
