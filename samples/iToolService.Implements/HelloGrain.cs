using HelloWorld.Interfaces;
using iTool.ClusterComponent;
using Microsoft.Extensions.Logging;
using Orleans;
using System.Threading.Tasks;

namespace HelloWorld.Grains
{
    /// <summary>
    /// Orleans grain implementation class HelloGrain.
    /// </summary>
    public class HelloGrain : iToolServiceBase, IHello
    {
        private readonly ILogger logger;

        public HelloGrain(ILogger<HelloGrain> logger)
        {
            this.logger = logger;
        }

        public Task<T> SayHello<T>(T greeting)
        {
            return Task.FromResult(greeting);
        }
    }


    public interface IDummyGrain : iToolServiceWithStringKey
    {
        Task<int> Method<T1, T2>(T1 x, T2 y);
    }

    public class BarGrain : IDummyGrain
    {
        public Task<int> Method<T1, T2>(T1 x, T2 y) => Task.FromResult(6);
    }

    public class IAnimal { }


}
