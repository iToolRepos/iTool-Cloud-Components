﻿using iTool.ClusterComponent;
using iToolService.Interfaces;
using Orleans;
using System;
using System.Threading.Tasks;

namespace iToolService.Implements
{
    [ImplicitStreamSubscription("iToolSimpleStream")]
    public class SubscribeService : iToolServiceSubscribeBase, ISubscribeService
    {
        /**
         * 在Service中隐士订阅队列，如果当前 颗粒处于 未激活状态，
         * 则它会丢失激活之前的消息。
         * 默认队列永远只会从订阅成功开始接受消息，并且不可倒带。
         * 所以使用此队列应该可以接受消息可能未正确接受现象。当前这可以通过提前手动激活避免，
         * 但是并不推荐这么做，因为这违背了我们对该队列的场景定位。
         * 
         * queueNamespace 相当于 或者文件夹/控制器 用于对详细类型进行分类
         */

        public SubscribeService() : base(queueNamespace: "user") { }

        public async Task Ping()
        {
            await Task.CompletedTask;
        }

        protected async override Task OnErrorAsync(Exception ex)
        {
            Console.WriteLine($"SubscribeService OnErrorAsync:{0}", ex.Message);
            await Task.CompletedTask;
        }

        protected async override Task OnNextAsync(string message)
        {
            Console.WriteLine($"SubscribeService OnNextAsync: {message}");
            await Task.CompletedTask;
        }

    }
}
