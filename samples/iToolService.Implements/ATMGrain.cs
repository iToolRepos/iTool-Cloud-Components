using iTool.ClusterComponent;
using iToolService.Interfaces;
using Orleans.Concurrency;
using System;
using System.Threading.Tasks;

namespace iToolService.Implements
{
    [StatelessWorker]
    public class ATMGrain : iToolServiceBase, IATMGrain
    {
        Task IATMGrain.Transfer(string fromAccount, string toAccount, uint amountToTransfer)
        {
            return Task.WhenAll(
                this.GrainFactory.GetGrain<IAccountGrain>(fromAccount).Withdraw(amountToTransfer),
                this.GrainFactory.GetGrain<IAccountGrain>(toAccount).Deposit(amountToTransfer));
        }
    }
}
