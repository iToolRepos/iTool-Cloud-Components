﻿using iTool.ClusterComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iToolService.Interfaces
{
    //public class StorageService : iToolServiceStorageBase<xxxxxx>, IStorageService
    public class StorageService : iToolServiceBase<xxxxxx>, IStorageService
        
    {
        public Task<string> GetState()
        {
            return Task.FromResult(this.State.value);
        }

        public async Task Modify(string value)
        {
            this.State.value = value;
            await this.WriteStateAsync();
        }

        public async Task Remove()
        {
            await this.ClearStateAsync();
        }

        ///// <summary>
        ///// 删除此颗粒
        ///// </summary>
        ///// <returns></returns>
        //protected override Task ClearStateAsync()
        //{
        //    Console.WriteLine($"ClearStateAsync：{this.GetKeyToString()} ");
        //    this.State.value = string.Empty;
        //    return Task.CompletedTask;
        //}

        ///// <summary>
        ///// 服务激活时自动执行
        ///// </summary>
        ///// <returns></returns>
        //protected override Task ReadStateAsync()
        //{
        //    Console.WriteLine($"ReadStateAsync：{this.GetKeyToString()} ");
        //    this.State = new xxxxxx();
        //    this.State.value = "这是状态";
        //    return Task.CompletedTask;
        //}

        ///// <summary>
        ///// 持久化状态信息
        ///// </summary>
        ///// <returns></returns>
        //protected override Task WriteStateAsync()
        //{
        //    Console.WriteLine($"WriteStateAsync：{this.GetKeyToString()} ");
        //    Console.WriteLine($"这里持久状态信息：{this.State.value} ");
        //    return Task.CompletedTask;
        //}
    }



    public class xxxxxx { public string value { get; set; } }
}
