﻿using iTool.ClusterComponent;
using iToolService.Interfaces;
using System.Threading.Tasks;

namespace iToolService.Implements
{
    [ClusterPlacementStrategy]
    public class ClusterNodeService : iToolServiceBase, IClusterNodeService
    {
        public Task<string> InvokeAsync(string parameter)
        {
            System.Console.WriteLine("InvokeAsync" + parameter);
            return Task.FromResult(string.Empty);
        }

        public Task InvokeNoResultAsync(string parameter)
        {
            //System.Console.WriteLine("InvokeAsync" + parameter);
            return Task.CompletedTask;
        }
    }
}
