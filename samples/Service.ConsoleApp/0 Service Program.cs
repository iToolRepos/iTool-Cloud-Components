﻿using iTool.Cloud.Center.Model;
using iTool.ClusterComponent;
using iTool.Common;
using iTool.Common.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Service.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            //var options = new {
            //    Password = "zhuJIAN320",
            //    Port = 2433,
            //    Title = "SQLSERVER",
            //};
            //var command = $"docker run -e ACCEPT_EULA=Y -e MSSQL_PID=\"Developer\" -e MSSQL_SA_PASSWORD=\"{options.Password}\" -e MSSQL_TCP_PORT={options.Port} -p {options.Port}:{options.Port} --name {options.Title} -d microsoft/mssql-server-linux";

            Console.WriteLine($"Hello World! Time UtcNow:{DateTime.UtcNow}");
            Task.WaitAll(RunMainAsync());

            Console.ReadKey();
        }

        private static async Task RunMainAsync()
        {
            try
            {


                var input = "";// Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input)) input = "11111,22222";
               var inputarr = input.Split(',').Select(item => int.Parse(item)).ToList();

                var builder = new iToolHostBuilder();
                builder.UseAdoNetClustering(new AdoNetClusterOptions
                {
                    AdoNetOptions = new AdoNetOptions
                    {
                        DataSource = "DESKTOP-L0F4SGU",
                        UID = "sa",
                        PWD = "30.3.."
                    },
                    EndpointsOptions = new EndpointsOptions(),
                    //EndpointsOptions = new EndpointsOptions
                    //{
                    //    // AdvertisedIP = null,
                    //    Port = inputarr[0],
                    //    GatewayPort = inputarr[1]
                    //},
                    ClusterOptions = new ClusterIdentificationOptions(),
                    ResponseTimeout = TimeSpan.FromSeconds(15)
                });

                builder.UseStreamProvider("TestStream", 20);

                var iToolHost = await builder.BuildAndStartAsync();

                // Console.WriteLine("started successfully,port:{0},gatewayport:{1}", inputarr[0], inputarr[1]);
                Console.ReadKey();
                var inputx = Console.ReadLine();
                await iToolHost.StopAsync();

                Console.ReadKey();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
