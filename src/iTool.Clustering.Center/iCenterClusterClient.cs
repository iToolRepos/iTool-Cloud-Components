﻿using Orleans;
using Orleans.Runtime;
using Orleans.Streams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.Center
{
    public interface iCenterClusterClient : IClusterClient
    {
    }

    public class CenterClusterClient : iCenterClusterClient
    {
        IClusterClient clusterClient;
        public bool IsInitialized => this.clusterClient.IsInitialized;

        public IServiceProvider ServiceProvider => this.clusterClient.ServiceProvider;

        public CenterClusterClient(IClusterClient clusterClient)
        {
            this.clusterClient = clusterClient;
        }

        public Task AbortAsync() => this.clusterClient.AbortAsync();

        public void BindGrainReference(IAddressable grain) => this.clusterClient.BindGrainReference(grain);

        public Task Close() => this.clusterClient.Close();

        public Task Connect(Func<Exception, Task<bool>> retryFilter = null) => this.clusterClient.Connect(retryFilter);

        public Task<TGrainObserverInterface> CreateObjectReference<TGrainObserverInterface>(IGrainObserver obj) where TGrainObserverInterface : IGrainObserver
             => this.clusterClient.CreateObjectReference<TGrainObserverInterface>(obj);

        public Task DeleteObjectReference<TGrainObserverInterface>(IGrainObserver obj) where TGrainObserverInterface : IGrainObserver
            => this.clusterClient.DeleteObjectReference<TGrainObserverInterface>(obj);

        public void Dispose() => this.clusterClient.Dispose();

        public ValueTask DisposeAsync() => this.clusterClient.DisposeAsync();

        public TGrainInterface GetGrain<TGrainInterface>(Guid primaryKey, string? grainClassNamePrefix = null) where TGrainInterface : IGrainWithGuidKey
            => this.clusterClient.GetGrain<TGrainInterface>(primaryKey, grainClassNamePrefix);

        public TGrainInterface GetGrain<TGrainInterface>(long primaryKey, string? grainClassNamePrefix = null) where TGrainInterface : IGrainWithIntegerKey
            => this.clusterClient.GetGrain<TGrainInterface>(primaryKey, grainClassNamePrefix);

        public TGrainInterface GetGrain<TGrainInterface>(string primaryKey, string grainClassNamePrefix = null) where TGrainInterface : IGrainWithStringKey
            => this.clusterClient.GetGrain<TGrainInterface>(primaryKey, grainClassNamePrefix);

        public TGrainInterface GetGrain<TGrainInterface>(Guid primaryKey, string keyExtension, string grainClassNamePrefix = null) where TGrainInterface : IGrainWithGuidCompoundKey
            => this.clusterClient.GetGrain<TGrainInterface>(primaryKey, keyExtension, grainClassNamePrefix);

        public TGrainInterface GetGrain<TGrainInterface>(long primaryKey, string keyExtension, string grainClassNamePrefix = null) where TGrainInterface : IGrainWithIntegerCompoundKey
            => this.clusterClient.GetGrain<TGrainInterface>(primaryKey, keyExtension, grainClassNamePrefix);

        public TGrainInterface GetGrain<TGrainInterface>(Type grainInterfaceType, Guid grainPrimaryKey) where TGrainInterface : IGrain
            => this.clusterClient.GetGrain<TGrainInterface>(grainInterfaceType, grainPrimaryKey);

        public TGrainInterface GetGrain<TGrainInterface>(Type grainInterfaceType, long grainPrimaryKey) where TGrainInterface : IGrain
            => this.clusterClient.GetGrain<TGrainInterface>(grainInterfaceType, grainPrimaryKey);

        public TGrainInterface GetGrain<TGrainInterface>(Type grainInterfaceType, string grainPrimaryKey) where TGrainInterface : IGrain
            => this.clusterClient.GetGrain<TGrainInterface>(grainInterfaceType, grainPrimaryKey);

        public TGrainInterface GetGrain<TGrainInterface>(Type grainInterfaceType, Guid grainPrimaryKey, string keyExtension) where TGrainInterface : IGrain
            => this.clusterClient.GetGrain<TGrainInterface>(grainInterfaceType, grainPrimaryKey, keyExtension);

        public TGrainInterface GetGrain<TGrainInterface>(Type grainInterfaceType, long grainPrimaryKey, string keyExtension) where TGrainInterface : IGrain
            => this.clusterClient.GetGrain<TGrainInterface>(grainInterfaceType, grainPrimaryKey, keyExtension);

        public IGrain GetGrain(Type grainInterfaceType, Guid grainPrimaryKey)
            => this.clusterClient.GetGrain(grainInterfaceType, grainPrimaryKey);

        public IGrain GetGrain(Type grainInterfaceType, long grainPrimaryKey)
            => this.clusterClient.GetGrain(grainInterfaceType, grainPrimaryKey);

        public IGrain GetGrain(Type grainInterfaceType, string grainPrimaryKey)
            => this.clusterClient.GetGrain(grainInterfaceType, grainPrimaryKey);

        public IGrain GetGrain(Type grainInterfaceType, Guid grainPrimaryKey, string keyExtension)
            => this.clusterClient.GetGrain(grainInterfaceType, grainPrimaryKey, keyExtension);

        public IGrain GetGrain(Type grainInterfaceType, long grainPrimaryKey, string keyExtension)
            => this.clusterClient.GetGrain(grainInterfaceType, grainPrimaryKey, keyExtension);

        public IStreamProvider GetStreamProvider(string name) => this.clusterClient.GetStreamProvider(name);
    }
}
