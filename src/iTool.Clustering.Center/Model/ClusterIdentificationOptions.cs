﻿using Microsoft.Extensions.Configuration;

using Orleans.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.Center.Model
{
    public class ClusterIdentificationOptions : ClusterOptions
    {
        public ClusterIdentificationOptions() : this("_clusterId", "_serviceId") { }
        public ClusterIdentificationOptions(IConfigurationRoot configuration, bool isGateway) :this(
            configuration.GetValue<string>($"iToolCloud:{(isGateway ? "Gateway" : "Server")}:ClusterIdentification:ClusterId"),
        configuration.GetValue<string>($"iToolCloud:{(isGateway ? "Gateway" : "Server")}:ClusterIdentification:ServiceId")) { }
        public ClusterIdentificationOptions(string clusterId, string serviceId)
        {
            this.ClusterId = clusterId;
            this.ServiceId = serviceId;
        }
        /// <summary>
        /// 集群ID，不同的集群直接不可以相互访问
        /// </summary>
        public new string ClusterId { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        public new string ServiceId { get; set; }
    }
}
