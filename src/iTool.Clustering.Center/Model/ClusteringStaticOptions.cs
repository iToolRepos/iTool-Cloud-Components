﻿using Orleans.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iTool.Cloud.Center.Model
{

    public class ClusteringStaticOptions
    {
        public ClusterIdentificationOptions ClusterOptions { get; set; }

        public List<ClientEndPointOptions> ClientEndPointOptions { get; set; }

        public ClusteringStaticOptions(ServiceEndPointOptions gatewayEndPointOptions, ClusterIdentificationOptions identificationOptions)
        {
            this.ClusterOptions = identificationOptions;
            this.ClientEndPointOptions = new List<ClientEndPointOptions>
            {
                new ClientEndPointOptions
                {
                    Host = gatewayEndPointOptions.Host,
                    GatewayPort = gatewayEndPointOptions.GatewayPort
                }
            };
        }

        public List<Uri> GetGatewayUris()
        {
            return this.ClientEndPointOptions.Select(item => item.ToIPEndPoint().ToGatewayUri()).ToList();
        }
    }
}
