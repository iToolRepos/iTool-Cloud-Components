﻿using Orleans;
using System;

namespace iTool.Cloud.Center.Model
{
    public class ClusterTableVersionOptions
    {

        public int Version { get; set; }

        public string VersionEtag { get; set; }

        public static ClusterTableVersionOptions Create(TableVersion tableVersion)
        {
            return new ClusterTableVersionOptions
            {
                Version = tableVersion.Version,
                VersionEtag = tableVersion.Version.ToString()
            };
        }

        public TableVersion ToTableVersion()
        {
            return new TableVersion(Version, VersionEtag);
        }
    }
}
