﻿using System;

using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Logging;

namespace iTool.Cloud.Center.ServiceProvider
{
    public struct Logger 
    {
        public LogLevel LogLevel { get; set; }
        public string TypeName { get; set; }
        public string Message { get; set; }
        public long TrackId { get; set; }
        public DateTime CreateDate { get; set; }
        public string GetInsertSql() 
        {
            return string.Intern($"insert into Log{LogLevel}(typeName, message, trackId, createDate) values($TypeName,$Message,$TrackId,$CreateDate)");
            //return $"insert into Log{LogLevel}(typeName, message, trackId, createDate) values('{TypeName}','{Message}',{TrackId},'{CreateDate}')";
        }
        public void SetSqlParameter(SqliteCommand sqliteCommand) 
        {
            sqliteCommand.Parameters.AddWithValue("$TypeName", this.TypeName);
            sqliteCommand.Parameters.AddWithValue("$Message", this.Message);
            sqliteCommand.Parameters.AddWithValue("$TrackId", this.TrackId);
            sqliteCommand.Parameters.AddWithValue("$CreateDate", this.CreateDate);
        }
    }
}
