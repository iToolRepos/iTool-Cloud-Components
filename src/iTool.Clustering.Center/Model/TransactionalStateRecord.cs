﻿using Orleans.Transactions.Abstractions;
using System;
using System.Collections.Generic;

namespace iTool.Cloud.Center.Model
{
    [Serializable]
    public class TransactionalStateRecord<TState> where TState : class, new()
    {
        public string? ETag { get; set; } = string.Empty;

        public TState CommittedState { get; set; } = new TState();

        public long CommittedSequenceId { get; set; }

        public TransactionalStateMetaData Metadata { get; set; } = new TransactionalStateMetaData();

        public List<PendingTransactionState<TState>> PendingStates { get; set; } = new List<PendingTransactionState<TState>>();

    }
}
