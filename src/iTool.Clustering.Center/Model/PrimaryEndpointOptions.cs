﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace iTool.Cloud.Center.Model
{
    public class PrimaryEndpointOptions
    {
        public PrimaryEndpointOptions() : this("127.0.0.1", 19400) { }
        public PrimaryEndpointOptions(string host, int port)
        { 
            this.Host = host;
            this.Port = port;
        }
        private string Host { get; set; }
        private int Port { get; set; }
        public IPEndPoint ToIPEndPoint()
        {
            return new IPEndPoint(IPAddress.Parse(this.Host), this.Port);
        }
    }
}
