﻿using Orleans;
using Orleans.Runtime;
using System;

namespace iTool.Cloud.Center.Model
{
    public class ReminderOptions
    {

        public string GrainId { get; set; }

        public string ReminderName { get; set; }

        public string Etag { get; set; }

        public TimeSpan Period { get; set; }
        public DateTime StartAt { get; set; }

        public long GrainHash { get; set; }

        public static ReminderOptions Create(ReminderEntry entry, string etag)
        {
            return new ReminderOptions
            {
                Etag = etag,
                GrainHash = entry.GrainRef.GetUniformHashCode(),
                GrainId = entry.GrainRef.ToKeyString(),
                Period = entry.Period,
                ReminderName = entry.ReminderName,
                StartAt = entry.StartAt
            };
        }

        public ReminderEntry ToEntry(IGrainReferenceConverter grainReferenceConverter)
        {
            return new ReminderEntry
            {
                ETag = Etag,
                GrainRef = grainReferenceConverter.GetGrainFromKeyString(GrainId),
                Period = Period,
                ReminderName = ReminderName,
                StartAt = StartAt
            };
        }
    }
}
