﻿using Orleans.Runtime;
using System;

namespace iTool.Cloud.Center.Model
{
    public class SuspectTimeOptions
    {
        public string Address { get; set; }

        public string IAmAliveTime { get; set; }

        public static SuspectTimeOptions Create(Tuple<SiloAddress, DateTime> tuple)
        {
            return new SuspectTimeOptions { Address = tuple.Item1.ToParsableString(), IAmAliveTime = LogFormatter.PrintDate(tuple.Item2) };
        }

        public Tuple<SiloAddress, DateTime> ToTuple()
        {
            return Tuple.Create(SiloAddress.FromParsableString(Address), LogFormatter.ParseDate(IAmAliveTime));
        }
    }
}
