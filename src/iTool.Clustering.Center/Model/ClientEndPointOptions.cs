﻿using System.Net;

using Microsoft.Extensions.Configuration;

namespace iTool.Cloud.Center.Model
{
    public class ClientEndPointOptions
    {
        public ClientEndPointOptions() : this("127.0.0.1", 19400) { }
        public ClientEndPointOptions(string host, int gatewayPort)
        {
            this.Host = host;
            this.GatewayPort = gatewayPort;
        }

        public string Host { get; set; }
        public int GatewayPort { get; set; }
        public IPEndPoint ToIPEndPoint()
        {
            return new IPEndPoint(IPAddress.Parse(this.Host), this.GatewayPort);
        }
    }

    public class ServiceEndPointOptions
    {
        public ServiceEndPointOptions() : this("127.0.0.1", 19300, 19400) { }

        public ServiceEndPointOptions(IConfigurationRoot configuration, bool isGateway) : this(
            configuration.GetValue<string>($"iToolCloud:{(isGateway ? "Gateway" : "Server")}:ServiceEndPoint:Host"),
            configuration.GetValue<int>($"iToolCloud:{(isGateway ? "Gateway" : "Server")}:ServiceEndPoint:Port"),
            configuration.GetValue<int>($"iToolCloud:{(isGateway ? "Gateway" : "Server")}:ServiceEndPoint:GatewayPort"))
        { 

        }

        public ServiceEndPointOptions(string host, int port, int gatewayPort)
        {
            this.Host = host;
            this.Port = port;
            this.GatewayPort = gatewayPort;
        }

        public string Host { get; set; }
        public int Port { get; set; }
        public int GatewayPort { get; set; }
        public IPEndPoint ToIPEndPoint()
        {
            return new IPEndPoint(IPAddress.Parse(this.Host), this.GatewayPort);
        }
    }
}
