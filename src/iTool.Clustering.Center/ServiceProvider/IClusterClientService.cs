﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public interface IClusterClientService : IGrainWithIntegerCompoundKey
    {
        Task InitializeGatewayListProvider();
        Task<IList<Uri>> GetGateways();
    }
}
