﻿using iTool.Cloud.Center.ServiceProvider.StorageProvider;
using Orleans;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public class ClusterTransactionalService : Orleans.Grain, IClusterTransactionalService
    {
        iStorageTransactionalMaster stateMaster;
        ConcurrentDictionary<string, ConcurrentDictionary<string, string>> iStates;

        public override Task OnActivateAsync()
        {
            var cluster = this.GetPrimaryKeyString();

            this.stateMaster = iStorage.GetClusterTransactional(cluster);
            this.iStates = this.stateMaster.States;

            this.stateMaster.StartSubjecter();
            // 初始化数据
            return base.OnActivateAsync();
        }

        public Task<string> Load(string stateType, string key)
        {
            if (this.iStates.TryGetValue(stateType, out ConcurrentDictionary<string, string> state))
            {
                if (state.TryGetValue(key, out string value))
                {
                    return Task.FromResult(value);
                }
            }
            return Task.FromResult(string.Empty);
        }

        public Task Store(string stateType, string key, string body)
        {
            if (this.iStates.TryGetValue(stateType, out ConcurrentDictionary<string, string> state))
            {
                state.AddOrUpdate(key, body, (k, v) => body);
            }
            else
            {
                state = new ConcurrentDictionary<string, string>();
                state.TryAdd(key, body);
                this.iStates.TryAdd(stateType, state);
            }
            this.stateMaster.Upsert();
            return Task.CompletedTask;
        }
    }
}
