﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using iTool.Cloud.Center.ServiceProvider.StorageProvider;

using Orleans;

namespace iTool.Clustering.Center.ServiceProvider
{
    public class ClusterClientService : Orleans.Grain, IClusterClientService
    {
        private string _clusterName;
        public override Task OnActivateAsync()
        {
            this._clusterName = this.GetPrimaryKeyString();
            return base.OnActivateAsync();
        }

        public Task<IList<Uri>> GetGateways()
        {
            var result = iStorage.GetClusterMemberships(this._clusterName);
            IList<Uri> uris = result.Memberships.Select(item => item.Value.ToGatewayUri()).ToList<Uri>();
            return Task.FromResult(uris);
        }

        public Task InitializeGatewayListProvider()
        {
            var result = iStorage.GetClusterMemberships(this._clusterName);
            return Task.CompletedTask;
        }
    }
}
