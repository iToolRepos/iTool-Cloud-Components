﻿using Newtonsoft.Json;

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace iTool.Cloud.Center.ServiceProvider.StorageProvider
{
    internal class iStorageTransactionalMaster
    {
        #region static
        [JsonIgnore]
        static object _LockClusterTransactional;
        [JsonIgnore]
        static ConcurrentDictionary<string, iStorageTransactionalMaster> ClusterTransactional { get; set; }
        static iStorageTransactionalMaster() 
        {
            _LockClusterTransactional = new object();
            ClusterTransactional = new ConcurrentDictionary<string, iStorageTransactionalMaster>();
        }
        internal static iStorageTransactionalMaster GetStorageMasterProvide(string cluster)
        {
            if (ClusterTransactional.TryGetValue(cluster, out iStorageTransactionalMaster iStorage))
            {
                return iStorage;
            }

            lock (_LockClusterTransactional)
            {
                if (ClusterTransactional.TryGetValue(cluster, out iStorage))
                {
                    return iStorage;
                }
                else
                {
                    var keyValue = StorageProvider.iStorage.GetKeyValue(cluster, "Transactional");

                    iStorage = new iStorageTransactionalMaster();

                    if (!string.IsNullOrEmpty(keyValue.Value))
                    {
                        iStorage = JsonConvert.DeserializeObject<iStorageTransactionalMaster>(keyValue.Value);
                    }

                    iStorage.Cluster = cluster;
                    iStorage.States = iStorage.States ?? new ConcurrentDictionary<string, ConcurrentDictionary<string, string>>();

                    return iStorage;
                }
            }
        }
        #endregion

        [JsonIgnore]
        internal Subject<bool> _subject;
        [JsonIgnore]
        internal string Cluster { get; set; }

        public ConcurrentDictionary<string, ConcurrentDictionary<string, string>> States { get; set; }

        internal void StartSubjecter()
        {
            if (_subject == null)
            {
                // 计划任务
                _subject = new Subject<bool>();
                _subject.Buffer(TimeSpan.FromSeconds(2), 10) // 1分钟 或者 30 条
                    .Where(x => x.Count > 0)
                    .Select(list => Observable.FromAsync(() => iStorage.BatchWriteAsync(Cluster, "Transactional", this)))
                    .Concat()
                    .Subscribe();
            }
        }
        internal void Upsert()
        {
            _subject.OnNext(true);
        }
    }

}
