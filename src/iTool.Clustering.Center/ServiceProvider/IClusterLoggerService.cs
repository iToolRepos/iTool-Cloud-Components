﻿using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Orleans;
using Orleans.Concurrency;

namespace iTool.Cloud.Center.ServiceProvider
{
    public interface IClusterLoggerService : IGrainWithStringKey
    {
        [OneWay]
        Task LogTraceAsync(string typeName,string message, long trackId);
        [OneWay]
        Task LogDebugAsync(string typeName, string message, long trackId);
        [OneWay]
        Task LogInformationAsync(string typeName, string message, long trackId);
        [OneWay]
        Task LogWarningAsync(string typeName, string message, long trackId);
        [OneWay]
        Task LogErrorAsync(string typeName, string message, long trackId);
        [OneWay]
        Task LogCriticalAsync(string typeName, string message, long trackId);
        [OneWay]
        Task LogNoneAsync(string typeName, string message, long trackId);

        [ReadOnly]
        [AlwaysInterleave]
        Task<string> QueryLoggerAsync(string query);
    }
}
