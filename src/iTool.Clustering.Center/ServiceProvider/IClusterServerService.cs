﻿using iTool.Cloud.Center.Model;
using Orleans;
using Orleans.Concurrency;
using System;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public interface IClusterServerService : IGrainWithIntegerCompoundKey
    {

        Task InitializeMembershipTable(bool tryInitTableVersion);

        Task DeleteMembershipTableEntries(string clusterId);

        Task CleanupDefunctSiloEntries(DateTimeOffset beforeDate);

        [AlwaysInterleave]
        Task<MembershipTableOptions> ReadRow(string key);

        [AlwaysInterleave]
        Task<MembershipTableOptions> ReadAll();

        Task<bool> InsertRow(string key, MembershipOptions entry, ClusterTableVersionOptions tableVersion);

        [AlwaysInterleave]
        Task<bool> UpdateRow(string key, MembershipOptions entry, string etag, ClusterTableVersionOptions tableVersion);

        [AlwaysInterleave]
        Task UpdateIAmAlive(string key, DateTime iAmAliveTime);

    }
}
