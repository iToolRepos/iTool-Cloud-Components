﻿using Orleans;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public interface IClusterStorageService : IGrainWithIntegerCompoundKey
    {
        Task<string> ReadStateAsync(string grainType, string key);

        Task WriteStateAsync(string grainType, string key, string state, string etag);

        Task ClearStateAsync(string grainType, string key);
    }
}
