﻿using Orleans;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public interface IClusterTransactionalService : IGrainWithIntegerCompoundKey
    {
        Task<string> Load(string stateType, string key);
        Task Store(string stateType, string key, string body);
    }
}
