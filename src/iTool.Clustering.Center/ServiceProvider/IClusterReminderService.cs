﻿using iTool.Cloud.Center.Model;
using Orleans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public interface IClusterReminderService : IGrainWithIntegerCompoundKey
    {
        Task Init();
        Task<List<ReminderOptions>> ReadRows(string key);
        Task<List<ReminderOptions>> ReadRows(uint begin, uint end);
        Task<ReminderOptions> ReadRow(string key, string reminderName);
        Task<string> UpsertRow(string key, ReminderOptions entry);
        Task<bool> RemoveRow(string key, string reminderName, string eTag);
        Task TestOnlyClearTable();
    }
}
