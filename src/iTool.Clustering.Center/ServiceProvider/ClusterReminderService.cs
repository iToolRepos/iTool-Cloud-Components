﻿using iTool.Cloud.Center.Model;
using iTool.Cloud.Center.ServiceProvider.StorageProvider;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTool.Clustering.Center.ServiceProvider
{
    public class ClusterReminderService : Orleans.Grain, IClusterReminderService
    {
        iStorageReminderMaster reminderMaster;
        public override Task OnActivateAsync()
        {
            var cluster = this.GetPrimaryKeyString();

            this.reminderMaster = iStorage.GetClusterReminders(cluster);
            this.reminderMaster.StartSubjecter();

            // 初始化数据
            return base.OnActivateAsync();
        }

        public Task Init()
        {
            return Task.CompletedTask;
        }

        public Task<ReminderOptions> ReadRow(string key, string reminderName)
        {
            if (this.reminderMaster.Reminders.TryGetValue(key, out List<ReminderOptions> list))
            {
                return Task.FromResult(list.Where(item => item.ReminderName == reminderName).FirstOrDefault());
            }

            return Task.FromResult(default(ReminderOptions));
        }

        public Task<List<ReminderOptions>> ReadRows(string key)
        {
            if (this.reminderMaster.Reminders.TryGetValue(key, out List<ReminderOptions> list))
            {
                return Task.FromResult(list);
            }

            return Task.FromResult(new List<ReminderOptions>());
        }

        public Task<List<ReminderOptions>> ReadRows(uint begin, uint end)
        {
            IEnumerable<ReminderOptions> result = new List<ReminderOptions>();
            foreach (var reminder in this.reminderMaster.Reminders)
            {
                var filter = reminder.Value.Where(x => x.GrainHash > begin && x.GrainHash <= end);
                result = result.Concat(filter);
            }
            return Task.FromResult(result.ToList());
        }

        public Task<bool> RemoveRow(string key, string reminderName, string eTag)
        {
            if (this.reminderMaster.Reminders.TryGetValue(key, out List<ReminderOptions> list))
            {
                this.reminderMaster.Reminders[key] = list.Where(item => item.ReminderName != reminderName).ToList();
            }
            this.reminderMaster.Upsert();
            return Task.FromResult(true);
        }

        public Task TestOnlyClearTable()
        {
            this.reminderMaster.Reminders.Clear();
            this.reminderMaster.Upsert();
            return Task.CompletedTask;
        }

        public Task<string> UpsertRow(string key, ReminderOptions entry)
        {
            if (this.reminderMaster.Reminders.TryGetValue(key, out List<ReminderOptions> list))
            {
                list.Add(entry);
            }
            else
            {
                list = new List<ReminderOptions>();
                list.Add(entry);
                this.reminderMaster.Reminders.TryAdd(key, list);
            }
            this.reminderMaster.Upsert();
            return Task.FromResult(entry.Etag);
        }
    }
}
