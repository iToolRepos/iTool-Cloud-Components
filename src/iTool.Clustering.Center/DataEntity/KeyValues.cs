﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.Center.DataEntity
{
    internal class KeyValues
    {
        public string Cluster { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
