﻿using iTool.Clustering.Center;
using iTool.Cloud.Center.Model;
using iTool.Clustering.Center.ServiceProvider;
using Microsoft.Extensions.Options;
using Orleans.Configuration;
using Orleans.Messaging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTool.Cloud.Center;

namespace Orleans.Runtime.Membership
{
    public class iCenterGatewayListProvider : IGatewayListProvider
    {
        private IClusterClientService grain;
        private ClusterOptions clusterOptions;
        public TimeSpan MaxStaleness => TimeSpan.FromMinutes(1);

        public iCenterGatewayListProvider(iCenterClusterClient client, IOptions<ClusterOptions> clusterOptions)
        {
            //var builder = new iToolClientBuilder();
            //builder.UseiCenterClustering(options.Value);
            //var client = builder.BuildAndConnectAsync().Result;
            this.clusterOptions = clusterOptions.Value;
            this.grain = client.GetGrain<IClusterClientService>(new Random().Next(111_111_111, 999_999_999), this.clusterOptions.ToKeyString());
        }

        public bool IsUpdatable => true;

        public async Task<IList<Uri>> GetGateways() => await this.grain.GetGateways();

        public async Task InitializeGatewayListProvider() => await this.grain.InitializeGatewayListProvider();
    }
}
