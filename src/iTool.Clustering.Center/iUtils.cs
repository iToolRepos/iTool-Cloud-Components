﻿using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;
using System;

namespace iTool.Clustering.Center
{
    public static partial class iUtils
    {
        public static uint GetJenkinsHashCode(byte[] data)
        {
            return JenkinsHash.ComputeHash(data);
        }

        public static uint GetJenkinsHashCode(ReadOnlySpan<byte> data)
        {
            return JenkinsHash.ComputeHash(data);
        }

        public static uint GetJenkinsHashCode(string data)
        {
            return JenkinsHash.ComputeHash(data);
        }

        public static uint GetJenkinsHashCode(ulong u1, ulong u2, ulong u3)
        {
            return JenkinsHash.ComputeHash(u1, u2, u3);
        }

        public static string ToKeyString(this ClusterOptions clusterOptions)
        {
            return string.Format("c:{0},s:{1}", clusterOptions.ClusterId, clusterOptions.ServiceId);
        }

        public static string PrimaryKeyAsString(this GrainReference grainRef, ClusterOptions clusterOptions = null)
        {
            return (clusterOptions == null ? String.Empty : $"{clusterOptions.ToKeyString()},") + grainRef.ToKeyString();
        }
    }
}
