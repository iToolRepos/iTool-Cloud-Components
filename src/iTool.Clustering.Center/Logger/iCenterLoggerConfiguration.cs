﻿using Microsoft.Extensions.Logging;

namespace iTool.Cloud.Center.Logger
{
    public class iCenterLoggerConfiguration
    {
        public string ProviderName { get; set; } = "DefaultLogger";
        public LogLevel MinLogLevel { get; set; }
        public string[] FilterTypeNames { get; set; }
    }
}