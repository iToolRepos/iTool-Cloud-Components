﻿using System;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;

using Orleans;

namespace iTool.Cloud.Center.Logger
{
    //https://learn.microsoft.com/zh-cn/dotnet/core/extensions/custom-logging-provider
    public static class iCenterLoggerExtensions
    {
        public static ILoggingBuilder AddCenterLogger(this ILoggingBuilder builder)
        {
            builder.AddConfiguration();

            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<ILoggerProvider, iCenterLoggerProvider>());

            LoggerProviderOptions.RegisterProviderOptions<iCenterLoggerConfiguration, iCenterLoggerProvider>(builder.Services);

            return builder;
        }

        public static ILoggingBuilder AddCenterLogger(this ILoggingBuilder builder, Action<iCenterLoggerConfiguration> configure)
        {
            builder.AddCenterLogger();
            builder.Services.Configure(configure);
            return builder;
        }
    }
}
