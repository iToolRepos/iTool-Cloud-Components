﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace iTool.Dashboard.EmbeddedAssets
{
    public interface IAssetProvider
    {
        Task ServeAssetAsync(string name, HttpContext httpContext);
    }
}
