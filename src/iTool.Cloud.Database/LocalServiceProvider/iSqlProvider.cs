﻿using iTool.Cloud.Database.Options;

using Microsoft.Data.Sqlite;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.Database.LocalServiceProvider
{
    public interface iSqlProvider
    {
        Task<(List<T> data, int total, string token)> ExecuteReaderAsync<T>(string sql, params SqliteParameter[] parameters) where T : class, new();
        ValueTask<(string data, int total, string token)> ExecuteReaderAsync(string sql, params SqliteParameter[] parameters);
        ValueTask<object> ExecuteScalarAsync(string sql, params SqliteParameter[] parameters);
        ValueTask<long> ExecuteNonQueryAsync(string sql, params SqliteParameter[] parameters);
        /// <summary>
        /// 不保证并发执行顺序
        /// </summary>
        ValueTask ExecuteNonQueryNoResultAsync(string sql, params SqliteParameter[] parameters);
        ValueTask ExecuteTransactionAsync(List<ExecuteItemOptions> executeItems);
        ValueTask ExecuteTransactionOfLockTableAsync(List<ExecuteItemOptions> executeItems);
        ValueTask BatchExecuteNonQueryAsync(List<ExecuteItemOptions> executeItems);
    }
}
