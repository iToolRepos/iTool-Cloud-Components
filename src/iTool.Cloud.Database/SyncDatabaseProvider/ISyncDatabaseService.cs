﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;

using Orleans;

namespace iTool.Cloud.Database.SyncDatabaseProvider
{
    public interface ISyncDatabaseService
    {
        void SyncAsync(IGrainFactory grainFactory);
        Task<long> GetCurrentVersionAsync();
    }
}
