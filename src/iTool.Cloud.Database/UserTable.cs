﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.Database
{
    public class UserTable
    {
        public string? Name { get; set; }
        public int Age { get; set; }
        public string? Sex { get; set; }
    }
}
