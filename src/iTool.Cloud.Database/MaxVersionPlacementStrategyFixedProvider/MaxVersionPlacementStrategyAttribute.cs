﻿using Orleans.Placement;
using System;

namespace iTool.Cloud.Database.MaxVersionPlacementStrategyFixedProvider
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class MaxVersionPlacementStrategyAttribute : PlacementAttribute
    {
        public MaxVersionPlacementStrategyAttribute() : base(new MaxVersionPlacementStrategy())
        {
        }
    }

}
