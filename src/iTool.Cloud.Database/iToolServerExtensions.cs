﻿using System;

using iTool.Cloud.Database.DistributedFilePlacementStrategyFixedProvider;
using iTool.Cloud.Database.KeyGeneratorProvider.Contract;
using iTool.Cloud.Database.LocalServiceProvider;
using iTool.Cloud.Database.MaxVersionPlacementStrategyFixedProvider;

using Microsoft.Extensions.DependencyInjection;

using Orleans.Runtime;
using Orleans.Runtime.Placement;

namespace iTool.ClusterComponent
{
    public static class iToolServerExtensions
    {
        public static iSqlProvider GetSqlExecutor(this iToolClusterHostClient builder) 
        {
            return new AISQLProvider();
        }

        public static iToolHostBuilder UseiDateBaseProvide(this iToolHostBuilder builder, KeyGeneratorOptions? keyGeneratorOptions = null)
        {
            builder.ConfigureServices(services =>
            {
                // 设置服务放置策略
                services.AddSingletonNamedService<PlacementStrategy, MaxVersionPlacementStrategy>(nameof(MaxVersionPlacementStrategy));
                services.AddSingletonKeyedService<Type, IPlacementDirector, MaxVersionSynchronizationProvider>(typeof(MaxVersionPlacementStrategy));

                // 文件策略
                services.AddSingletonNamedService<PlacementStrategy, DistributedFilePlacementStrategy>(nameof(DistributedFilePlacementStrategy));
                services.AddSingletonKeyedService<Type, IPlacementDirector, DistributedFileProvider>(typeof(DistributedFilePlacementStrategy));

                services.AddSingleton<KeyGeneratorOptions>(keyGeneratorOptions ?? new KeyGeneratorOptions());
            });
            return builder;
        }
    }
}
