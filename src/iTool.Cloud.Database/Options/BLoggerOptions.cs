﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.Database.Options
{
    public class BLoggerOptions
    {
        public long RowId { get; set; }
        public string Action { get; set; }
        public string Keys { get; set; }
        public string Sql { get; set; }
        public string Extend { get; set; }
        public long TranId { get; set; }
    }
}
