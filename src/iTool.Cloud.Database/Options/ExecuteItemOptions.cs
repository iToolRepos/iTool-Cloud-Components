﻿
using System;
using System.Collections.Generic;

using iTool.Cloud.Database.iToolService;
using iTool.Cloud.Database.LocalServiceProvider;
using iTool.SQL.AIHandle;

using Microsoft.Data.Sqlite;

namespace iTool.Cloud.Database.Options
{
    public class AnalysisResult
    {
        public FormatSqlResult FormatSqlResult { get; set; }
        public Dictionary<string, LinkedList<string>> TableStructures { get; set; }
        //public List<(string sql, List<string> fields, string value, bool hasNot, string table)> Indexs { get; set; }
        public List<SearchIndexOptions> Indexs { get; set; }
        public List<SortByOptions> SortByOptions { get; set; }
        public List<string> CustomFunctions { get; set; }
    }

    public class ExecuteItemOptions
    {
#pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
        public string Sql { get; set; }
#pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
#pragma warning disable CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
        public SqliteParameter[] Parameters { get; set; }
#pragma warning restore CS8618 // 在退出构造函数时，不可为 null 的字段必须包含非 null 值。请考虑声明为可以为 null。
    }

    public class AnalysisExecuteItemOptions: ExecuteItemOptions
    {
        public AnalysisExecuteItemOptions(AnalysisResult input, ExecuteItemOptions options) : this(input, options.Parameters)
        {
            
        }

        public AnalysisExecuteItemOptions(AnalysisResult input, SqliteParameter[] parameters)
        {
            this.QuerySql = input.FormatSqlResult.sql;
            this.TableName = input.FormatSqlResult.tableName;
            this.Action = input.FormatSqlResult.action;
            this.WhereCase = input.FormatSqlResult.where;
            this.Keys = input.FormatSqlResult.keys;
            this.SetFields = input.FormatSqlResult.SetFields;
            this.TableStructures = input.TableStructures;
            this.SortByOptions = input.SortByOptions;
            this.CustomFunctions = input.CustomFunctions;
            this.Indexs = input.Indexs;
            this.Parameters = parameters;
            if (this.Action == SQLActionOptions.INSERT)
            {
                this.Parameters = new SqliteParameter[parameters == null ? 1 : parameters.Length + 1];
                if (parameters != null && parameters.Length > 0)
                    this.Parameters.CopyTo(parameters, 0);
                this.Parameters[^1] = new SqliteParameter();
                this.Parameters[^1].ParameterName = "@B_IDX";
            }
        }

        public SQLActionOptions Action { get; set; }
        public string TableName { get; set; }
        public string WhereCase { get; set; }
        public long[] Keys { get; set; }
        public string QuerySql { get; set; }
        public List<string> SetFields { get; set; }
        public Dictionary<string, LinkedList<string>> TableStructures { get; set; }
        public List<SearchIndexOptions> Indexs { get; set; }
        public List<SortByOptions> SortByOptions { get; set; }
        public List<string> CustomFunctions { get; set; }
        public string Error { get; set; }
    }
}
