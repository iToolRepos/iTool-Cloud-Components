﻿using iTool.SQL.AIHandle;

namespace iTool.Cloud.Database.iToolService
{
    public struct ExecLogger
    {
        public string table;
        public SQLActionOptions action;
        public long[] keys;
        public long tranId;
        public string sql;
        public string extend;

        public ExecLogger(string table, SQLActionOptions action, long[] keys, string sql, string extend, long tranId)
        {
            this.table = table;
            this.action = action;
            this.keys = keys;
            this.sql = sql;
            this.extend = extend;
            this.tranId = tranId;
        }
    }
}
