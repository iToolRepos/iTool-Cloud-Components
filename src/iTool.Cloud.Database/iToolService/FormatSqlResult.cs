﻿using iTool.SQL.AIHandle;

using System;
using System.Collections.Generic;

namespace iTool.Cloud.Database.iToolService
{
    public struct FormatSqlResult
    {
        public string sql;
        public SQLActionOptions action;
        public string tableName;
        public string where;
        public long[] keys;
        public List<string> SetFields;

        public FormatSqlResult(string sql, SQLActionOptions action, string tableName, string where, long[] keys, List<string> setFields)
        {
            this.sql = sql;
            this.action = action;
            this.tableName = tableName;
            this.where = where;
            this.keys = keys;
            this.SetFields = setFields;
        }
    }

}
