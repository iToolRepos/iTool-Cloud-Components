﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.Database.KeyGeneratorProvider
{
    public interface IKeyWorkerService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        Task<ushort> GetWorkerIdAsync();
    }
}
