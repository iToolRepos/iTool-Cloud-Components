﻿using iTool.Cloud.Database.KeyGeneratorProvider.Contract;
using iTool.Cloud.Database.KeyGeneratorProvider.Contract.Core;
using System;
using System.Threading;

namespace iTool.Cloud.Database.KeyGeneratorProvider
{
    /// <summary>
    /// 默认实现
    /// </summary>
    internal class DefaultGeneratorProvider : IKeyGenerator
    {
        private ISnowWorker _SnowWorker { get; set; }

        public DefaultGeneratorProvider(UniqueKeyGeneratorOptions options)
        {
            if (options == null)
            {
                throw new ApplicationException("options error.");
            }

            // 1.BaseTime
            if (options.BaseTime < DateTime.Now.AddYears(-50) || options.BaseTime > DateTime.Now)
            {
                throw new ApplicationException("BaseTime error.");
            }

            // 2.WorkerIdBitLength
            int maxLength = options.TimestampType == 0 ? 22 : 31; // （秒级时间戳时放大到31位）
            if (options.WorkerIdBitLength <= 0)
            {
                throw new ApplicationException("WorkerIdBitLength error.(range:[1, 21])");
            }
            if (options.DataCenterIdBitLength + options.WorkerIdBitLength + options.SeqBitLength > maxLength)
            {
                throw new ApplicationException("error：DataCenterIdBitLength + WorkerIdBitLength + SeqBitLength <= " + maxLength);
            }

            // 3.WorkerId & DataCenterId
            var maxWorkerIdNumber = (1 << options.WorkerIdBitLength) - 1;
            if (maxWorkerIdNumber == 0)
            {
                maxWorkerIdNumber = 63;
            }
            if (options.WorkerId < 0 || options.WorkerId > maxWorkerIdNumber)
            {
                throw new ApplicationException("WorkerId error. (range:[0, " + maxWorkerIdNumber + "]");
            }

            var maxDataCenterIdNumber = (1 << options.DataCenterIdBitLength) - 1;
            if (options.DataCenterId < 0 || options.DataCenterId > maxDataCenterIdNumber)
            {
                throw new ApplicationException("DataCenterId error. (range:[0, " + maxDataCenterIdNumber + "]");
            }

            // 4.SeqBitLength
            if (options.SeqBitLength < 2 || options.SeqBitLength > 21)
            {
                throw new ApplicationException("SeqBitLength error. (range:[2, 21])");
            }

            // 5.MaxSeqNumber
            var maxSeqNumber = (1 << options.SeqBitLength) - 1;
            if (maxSeqNumber == 0)
            {
                maxSeqNumber = 63;
            }
            if (options.MaxSeqNumber < 0 || options.MaxSeqNumber > maxSeqNumber)
            {
                throw new ApplicationException("MaxSeqNumber error. (range:[1, " + maxSeqNumber + "]");
            }

            // 6.MinSeqNumber
            if (options.MinSeqNumber < 5 || options.MinSeqNumber > maxSeqNumber)
            {
                throw new ApplicationException("MinSeqNumber error. (range:[5, " + maxSeqNumber + "]");
            }

            if (options.DataCenterIdBitLength == 0 && options.TimestampType == 0)
            {
                _SnowWorker = new SnowWorker(options);
            }
            else
            {
                _SnowWorker = new SnowWorkerOfSecond(options);
            }

            Thread.Sleep(500);
        }

        public long NextKeyOfLong() => _SnowWorker.NextKeyOfLong();
    }
}
