﻿using System;

namespace iTool.Cloud.Database.KeyGeneratorProvider.Contract
{
    public interface IKeyGenerator
    {
        /// <summary>
        /// 生成新的long型Id
        /// </summary>
        long NextKeyOfLong();
    }
}
