﻿using System;

namespace iTool.Cloud.Database.KeyGeneratorProvider.Contract
{
    public class KeyGeneratorOptions
    {
        public KeyGeneratorOptions()
        {

        }

        public KeyGeneratorOptions(ushort workerId)
        {
            WorkerId = workerId;
        }

        public DateTime BaseTime { get; set; } = new DateTime(year: 2022, month: 3, day: 12, hour: 9, minute: 9, second: 9, millisecond: 9, DateTimeKind.Utc);

        public ushort WorkerId { get; internal set; } = 0;

        public int PartitionCount { get; set; } = 7;
        public byte WorkerIdBitLength { get; set; } = 10;
        public byte SeqBitLength { get; set; } = 12;

    }
}
