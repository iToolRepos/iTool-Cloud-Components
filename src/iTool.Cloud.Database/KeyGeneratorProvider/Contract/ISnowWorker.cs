﻿using System;
namespace iTool.Cloud.Database.KeyGeneratorProvider.Contract
{
    internal interface ISnowWorker
    {
        long NextKeyOfLong();
    }
}
