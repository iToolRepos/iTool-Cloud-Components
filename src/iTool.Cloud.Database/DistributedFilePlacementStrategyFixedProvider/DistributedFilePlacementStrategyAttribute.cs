﻿using Orleans.Placement;
using System;

namespace iTool.Cloud.Database.DistributedFilePlacementStrategyFixedProvider
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class DistributedFilePlacementStrategyAttribute : PlacementAttribute
    {
        public DistributedFilePlacementStrategyAttribute() : base(new DistributedFilePlacementStrategy())
        {
        }
    }

}
