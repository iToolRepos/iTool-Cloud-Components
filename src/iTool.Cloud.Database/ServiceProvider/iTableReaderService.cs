﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;
using iTool.ClusterComponent;

using Orleans.Concurrency;

namespace iTool.Cloud.Database.ServiceProvider
{
    public interface iTableReaderService : iToolServiceWithStringKey
    {
        [AlwaysInterleave]
        Task<(string data, int total, string token)> ExecuteReaderAsync(AnalysisExecuteItemOptions query);

        [AlwaysInterleave]
        Task<object> ExecuteScalarAsync(AnalysisExecuteItemOptions query);
    }

    public interface iTableReaderService<TReader> : iToolServiceWithStringKey
        where TReader : class, new()
    {
        [AlwaysInterleave]
        Task<(List<TReader> data, int total, string token)> ExecuteReaderAsync(AnalysisExecuteItemOptions query);
    }
}
