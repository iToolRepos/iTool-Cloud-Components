﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;

namespace iTool.Cloud.Database.ServiceProvider
{
    public interface iTransactionService : iTool.ClusterComponent.iToolServiceWithNoder
    {
        Task BeginTransactionAsync(long tranId,string[] tables, bool isLockTableDB);
        Task CommitTransactionAsync(long tranId);
        Task RollbackAsync(long tranId);
        Task ComplateTransactiondAsync(long tranId);
        Task ErrorTransactiondAsync(long tranId);
    }
}
