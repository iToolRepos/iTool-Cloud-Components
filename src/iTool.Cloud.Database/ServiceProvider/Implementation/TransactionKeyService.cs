﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;
using iTool.ClusterComponent;

namespace iTool.Cloud.Database.ServiceProvider.Implementation
{
    public class TransactionKeyService : iToolServiceBase<long>, iTransactionKeyService
    {
        public async Task<long> GetTransactiondKeyAsync()
        {
            State++;
            await WriteStateAsync();
            return State;
        }
    }
}
