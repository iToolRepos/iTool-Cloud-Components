﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Orleans.Concurrency;

namespace iTool.Cloud.Database.ServiceProvider
{
    /// <summary>
    /// 1 检查文件库
    /// </summary>
    public interface iFileExistsService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        /// <summary>
        /// 返回当前Service 地址
        /// </summary>
        /// <returns></returns>
        Task<int> IsExistsAsync();

        [OneWay]
        Task AcceptNotificationSyncFileDataAsync(string siloAddress);

        [OneWay]
        Task AcceptNotificationDaleteFileDataAsync();
    }
}
