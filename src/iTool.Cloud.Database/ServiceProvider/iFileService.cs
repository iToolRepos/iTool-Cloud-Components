﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Orleans.Concurrency;

namespace iTool.Cloud.Database.ServiceProvider
{
    /// <summary>
    /// 1 文件库
    /// 2 文件碎片库 => 文件库
    /// </summary>
    public interface iFileService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        Task UploadAsync(UploadInfo uploadInfo);

        /// <summary>
        /// 分片上传
        /// </summary>
        Task UploadPieceAsync(UploadPiece uploadInfo);

        /// <summary>
        /// 上传完成文件入库
        /// </summary>
        Task UploadComplatedAsync();

        /// <summary>
        /// 获取文件信息
        /// 不含文件流
        /// </summary>
        /// <returns></returns>
        Task<UploadInfo> GetFileInfoAsync();

        /// <summary>
        /// 获取文件流
        /// </summary>
        /// <returns></returns>
        Task<UploadPiece> GetStreamAsync(int lastNumber = 1);

        Task<byte[]> GetStreamAsync(int width, int height);

        Task DeleteFileAsync();

        Task<List<UploadInfo>> QueryFileInfoAsync(string query);

        Task<bool> IsExistsAsync();

        [OneWay]
        Task CopyFileAsync(string siloAddress);
    }

    public struct UploadPiece
    {
        public int Number { get; set; }
        public byte[] FileStream { get; set; }
        public bool IsEndNUmber { get; set; }
    }

    public struct UploadInfo
    {
        /// <summary>
        /// UploadState 0 创建， 1 上传中， 200 已完成在本库， 201 已完成在分片库
        /// </summary>
        public int UploadState { get; set; }
        public string Key { get; set; }
        public string User { get; set; }
        public string Role { get; set; }
        public string SuffixName { get; set; }
        public string ContentType { get; set; }
        public DateTime CreateDate { get; set; }
        public int TotalLength { get; set; }
        public byte[] FileStream { get; set; }
    }
}
