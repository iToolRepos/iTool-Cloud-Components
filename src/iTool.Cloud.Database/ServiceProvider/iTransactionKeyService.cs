﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;

namespace iTool.Cloud.Database.ServiceProvider
{
    public interface iTransactionKeyService : iTool.ClusterComponent.iToolService
    {
        Task<long> GetTransactiondKeyAsync();
    }
}
