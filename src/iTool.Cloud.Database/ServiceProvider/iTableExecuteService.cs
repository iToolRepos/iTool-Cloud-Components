﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;

using Microsoft.Data.Sqlite;

using Orleans.Concurrency;

namespace iTool.Cloud.Database.ServiceProvider
{
    public interface iTableExecuteService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        Task BatchExecuteNonQueryAsync(List<AnalysisExecuteItemOptions> executeItems);
        Task ExecuteNonQueryNoResultAsync(AnalysisExecuteItemOptions executeItem);
        Task<long> ExecuteNonQueryAsync(AnalysisExecuteItemOptions executeItem);
        Task ExecuteTransactionAsync(List<AnalysisExecuteItemOptions> executeItem, long tranId);
        Task ComplatedTransactionAsync();
        Task<string> GetRegisterServiceAsync();

        /// <summary>
        /// 同步索引
        /// </summary>
        /// <param name="locations">坐标索引</param>
        /// <param name="fields">字段索引</param>
        /// <returns></returns>
        [OneWay]
        Task SyncIndexFieldsAsync(List<string>? locations, List<string>? fields);
    }
}
