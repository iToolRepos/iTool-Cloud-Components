﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;
using iTool.Cloud.Database.SqlStructureProvider.Options;

namespace iTool.Cloud.Database.ServiceProvider
{
    /// <summary>
    /// 从最大的Version Service 激活
    /// </summary>
    public interface iTableSyncLoggerService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        /// <summary>
        /// 获取同步Logger
        /// </summary>
        /// <returns></returns>
        Task<List<BLoggerOptions>?> GetSyncSqlScriptAsync(long lastversion);

        /// <summary>
        /// 获取所有表字段
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<FieldOptions>> GetTableAllFieldAsync();

        /// <summary>
        /// 获取所有索引字段
        /// </summary>
        /// <returns></returns>
        Task<(List<string> locations, List<string> fields)> GetAllIndexFieldAsync();
    }
}
