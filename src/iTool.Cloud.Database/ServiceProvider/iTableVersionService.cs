﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

using iTool.Cloud.Database.Options;

using Microsoft.Data.Sqlite;

using Orleans.Concurrency;

namespace iTool.Cloud.Database.ServiceProvider
{
    /// <summary>
    /// 每个Service 一个激活
    /// </summary>
    public interface iTableVersionService : iTool.ClusterComponent.iToolServiceWithStringKey
    {
        /// <summary>
        /// 接收更新通知
        /// 并且：启动Service 开始同步当前数据
        /// 在每次写入Logger时 推送此通知
        /// </summary>
        /// <returns></returns>
        [OneWay]
        Task AcceptNotificationAsync();

        /// <summary>
        /// 获取当前Serve 最新版本ID
        /// </summary>
        /// <returns></returns>
        Task<long> GetCurrentVersionAsync();
    }
}
