﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.Database.SqlStructureProvider.Options
{
    public struct FieldOptions
    {
        //NULL          值是一个 NULL 值。
        //INTEGER       值是一个带符号的整数，根据值的大小存储在 1、2、3、4、6 或 8 字节中。
        //REAL          值是一个浮点值，存储为 8 字节的 IEEE 浮点数字。
        //TEXT          值是一个文本字符串，使用数据库编码（UTF-8、UTF-16BE 或 UTF-16LE）存储。
        //BLOB          值是一个 blob 数据，完全根据它的输入存储。

        //Boolean       0 | 1 INTEGER
        //Date          TEXT

        public Type ValueType { get; set; }
        public string Name { get; set; }

        public string GetValueType() 
        {
            switch (this.ValueType)
            {
                case var t when t == typeof(long):
                    return "INTEGER";
                case var t when t == typeof(decimal):
                    return "REAL";
                case var t when t == typeof(byte):
                    return "BLOB";
                case var t when t == typeof(string):
                default:
                    return "TEXT";
            }
        }
    }
}
