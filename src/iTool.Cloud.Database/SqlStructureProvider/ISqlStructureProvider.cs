﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.Cloud.Database.SqlStructureProvider.Options;

namespace iTool.Cloud.Database.SqlStructureProvider
{
    public interface ISqlStructureProvider
    {
        //Task AddTableAsync(List<FieldOptions> fields);
        Task CheckStructureAsync(IEnumerable<string> fields);
        Task<IEnumerable<FieldOptions>> GetAllFieldsAsync();
        Task AddFieldAsync(FieldOptions field);
        Task AddFieldsAsync(IEnumerable<FieldOptions> fields);
        //Task DorpFieldAsync(string table, FieldOptions field);
        //Task UpdateFieldAsync(string table, FieldOptions field);
        //Task ExistsFieldAsync(string table, string fieldName);
    }
}
