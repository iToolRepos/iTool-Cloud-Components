﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.SubQuery
{
    /// <summary>
    /// 子查询
    /// </summary>
    public class SqlScalarSubQueryExpressionAnalysis : SqlAnalysisBase
    {
        public SqlScalarSubQueryExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(whereOptions: base.WhereOptions);
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
