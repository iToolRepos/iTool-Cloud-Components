﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.SelectClause
{
    public class SqlSelectClauseAnalysis : SqlAnalysisBase
    {
        public SqlSelectClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            var code = (SqlSelectClause)base.CodeObject;
            ((SqlSelectAnalysisContext)base.context).IsDistinct = code.IsDistinct;
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
