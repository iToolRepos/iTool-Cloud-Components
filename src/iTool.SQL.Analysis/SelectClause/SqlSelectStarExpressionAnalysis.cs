﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.SelectClause
{
    public class SqlSelectStarExpressionAnalysis : SqlAnalysisBase
    {
        public SqlSelectStarExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            var code = (SqlSelectStarExpression)base.CodeObject;

            if (base.context is SqlSelectAnalysisContext sContext)
            {
                sContext.QueryFields.Add(new SQLFieldOptions
                {
                    Name = "*",
                    Schema = code.Qualifier?.ObjectName?.Value
                });
            }
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
