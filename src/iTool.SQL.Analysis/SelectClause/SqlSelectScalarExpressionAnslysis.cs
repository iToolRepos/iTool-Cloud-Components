﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.SelectClause
{
    public class SqlSelectScalarExpressionAnslysis : SqlAnalysisBase
    {
        public SqlSelectScalarExpressionAnslysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(afterAction: (index, _, analysis) =>
            {
                if (index == 0)
                {
                    ((SqlSelectAnalysisContext)base.context).QueryFields[^1].Alias = ((SqlSelectScalarExpression)base.CodeObject).Alias?.Value;
                }
            });
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
