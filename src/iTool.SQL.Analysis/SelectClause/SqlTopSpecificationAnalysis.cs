﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Common;
using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.SelectClause
{
    public class SqlTopSpecificationAnalysis : SqlAnalysisBase
    {
        public SqlTopSpecificationAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            var code = (SqlTopSpecification)base.CodeObject;
            ((SqlSelectAnalysisContext)base.context).TopCount = code.Value.ToString();
            //base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
