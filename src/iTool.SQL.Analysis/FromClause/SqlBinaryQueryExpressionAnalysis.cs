﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.FromClause
{
    public class SqlBinaryQueryExpressionAnalysis : SqlAnalysisBase
    {
        public SqlBinaryQueryExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(afterAction:(index,_, analysis) => {
                if (index == 1)
                {
                    ((SqlSelectAnalysisContext)base.context).QueryTables[^1].JoinOperator = ((SqlBinaryQueryExpression)base.CodeObject).Operator;
                }
            });
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
