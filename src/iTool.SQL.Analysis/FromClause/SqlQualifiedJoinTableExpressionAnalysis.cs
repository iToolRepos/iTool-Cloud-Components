﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.FromClause
{
    public class SqlQualifiedJoinTableExpressionAnalysis : SqlAnalysisBase
    {
        public SqlQualifiedJoinTableExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            var code = (SqlQualifiedJoinTableExpression)base.CodeObject;
            base.Next(afterAction:(index,_, analysis) => {
                if (index == 1)
                {
                    ((SqlSelectAnalysisContext)base.context).QueryTables[^1].JoinOperator = code.JoinOperator;
                }
            });
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
