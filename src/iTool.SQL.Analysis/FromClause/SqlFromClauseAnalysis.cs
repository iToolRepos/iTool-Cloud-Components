﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.FromClause
{
    public class SqlFromClauseAnalysis : SqlAnalysisBase
    {
        public SqlFromClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
