﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.OrderByClause
{
    public class SqlOrderByItemAnalysis : SqlAnalysisBase
    {
        public SqlOrderByItemAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            if (((SqlOrderByItem)base.CodeObject).SortOrder != SqlSortOrder.None)
                ((SqlSelectAnalysisContext)base.context).QueryTables[^1].OrderBy.SortOrder = ((SqlOrderByItem)base.CodeObject).SortOrder;

            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
