﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.OrderByClause
{
    public class SqlOrderByClauseAnalysis : SqlAnalysisBase
    {
        public SqlOrderByClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            ((SqlSelectAnalysisContext)base.context).QueryTables[^1].OrderBy = new Context.Options.SortByOptions();
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
