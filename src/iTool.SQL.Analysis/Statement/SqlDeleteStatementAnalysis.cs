﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System.Linq;

namespace iTool.SQL.Analysis.Statement
{
    public class SqlDeleteStatementAnalysis : SqlAnalysisBase
    {
        public SqlDeleteStatementAnalysis() : base(new SqlDeleteAnalysisContext())
        {
        }

        public override void Analysis()
        {
            base.CodeObject = base.CodeObject?.Children.OfType<SqlDeleteSpecification>().FirstOrDefault();

            if (CodeObject != null) 
            {
                base.Next();
            }
            else
            {
                base.DebugLogger<SqlDeleteSpecification>(string.Format("{0},sql:{1}", "codeObject is null", base.CodeObject.Sql));
            }
        }

        public override bool Validation(SqlCodeObject codeObject)
        {
            base.CodeObject = codeObject
                    ?.Children.OfType<SqlBatch>().FirstOrDefault()
                    ?.Children.OfType<SqlDeleteStatement>().FirstOrDefault();
            return base.CodeObject != null;
        }
    }
}
