﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.Statement
{
    public class SqlInsertStatementAnalysis : SqlAnalysisBase
    {
        public SqlInsertStatementAnalysis() : base(new SqlInsertAnalysisContext())
        {
        }

        public override void Analysis()
        {
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject)
        {
            base.CodeObject = codeObject
                    ?.Children.OfType<SqlBatch>().FirstOrDefault()
                    ?.Children.OfType<SqlInsertStatement>().FirstOrDefault()
                    ?.Children.OfType<SqlInsertSpecification>().FirstOrDefault();
            return this.CodeObject != null;
        }
    }
}
