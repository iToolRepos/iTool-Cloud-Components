﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.Statement
{
    internal class SqlUpdateStatementAnalysis : SqlAnalysisBase
    {
        public SqlUpdateStatementAnalysis() : base(new SqlUpdateAnalysisContext())
        {
        }

        public override void Analysis()
        {
            base.CodeObject = base.CodeObject?.Children.OfType<SqlUpdateSpecification>().FirstOrDefault();

            if (CodeObject != null)
            {
                base.Next();
            }
            else
            {
                base.DebugLogger<SqlDeleteSpecification>(string.Format("{0},sql:{1}", "codeObject is null", base.CodeObject.Sql));
            }
        }

        public override bool Validation(SqlCodeObject codeObject)
        {
            base.CodeObject = codeObject
                    ?.Children.OfType<SqlBatch>().FirstOrDefault()
                    ?.Children.OfType<SqlUpdateStatement>().FirstOrDefault();
            return this.CodeObject != null;
        }
    }
}
