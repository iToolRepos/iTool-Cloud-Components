﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.Statement
{
    internal class SqlSelectStatementAnalysis : SqlAnalysisBase
    {

        public SqlSelectStatementAnalysis() : base(new SqlSelectAnalysisContext())
        {
        }

        public override void Analysis()
        {
            base.Next(); 
        }

        public override bool Validation(SqlCodeObject codeObject)
        {
            base.CodeObject = codeObject
                    ?.Children.OfType<SqlBatch>().FirstOrDefault()
                    ?.Children.OfType<SqlSelectStatement>().FirstOrDefault()
                    ?.Children.OfType<SqlSelectSpecification>().FirstOrDefault();

            if (base.CodeObject != null)
            {
                base.CodeObject = codeObject
                    ?.Children.OfType<SqlBatch>().FirstOrDefault();
            }

            return this.CodeObject != null;
        }
    }
}
