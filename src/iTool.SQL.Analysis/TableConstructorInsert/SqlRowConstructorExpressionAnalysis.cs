﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.TableConstructorInsert
{
    public class SqlRowConstructorExpressionAnalysis : SqlAnalysisBase
    {
        public SqlRowConstructorExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(beforeAction:(index,_, analysis) => 
            {
                if (_ is SqlLiteralExpression literal)
                {
                    ((SqlInsertAnalysisContext)base.context).Sets[index].Value = literal;
                    return false;
                }
                return true;
            });
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
