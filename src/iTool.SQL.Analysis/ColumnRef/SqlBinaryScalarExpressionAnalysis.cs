﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.ColumnRef
{
    public class SqlBinaryScalarExpressionAnalysis : SqlAnalysisBase
    {
        public SqlBinaryScalarExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            // where
            // set
            // select
            base.context.Errors.Add("not support expression: " + base.CodeObject.Sql + ",This will affect the efficiency of execution");
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
