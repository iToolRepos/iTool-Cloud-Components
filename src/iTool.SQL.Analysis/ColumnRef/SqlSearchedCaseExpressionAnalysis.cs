﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.ColumnRef
{
    public class SqlSearchedCaseExpressionAnalysis : SqlAnalysisBase
    {
        public SqlSearchedCaseExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.context.Errors.Add("not support expression: " + base.CodeObject.Sql + ",This will affect the efficiency of execution");
            return;

            //StringBuilder where = base.GetWhereBuilder();
            var caseWhen = (SqlSearchedCaseExpression)base.CodeObject;
            int count = caseWhen.Children.Count();
            //where.Append(" case");
            base.Next(beforeAction: (index, _, analysis) =>
            {
                if ((count - 1) == index)
                {
                    //string nbsp = string.Empty;
                    //if (_ is SqlLiteralExpression)
                    //    nbsp = " ";
                    //where.Append(" else"+ nbsp);
                }
                return true;
            });
            //where.Append(" end");
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
