﻿using System;
using System.Linq;
using System.Text;

using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;
using iTool.SQL.Analysis.Context.SQLWhereOptions;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.ColumnRef
{
    public class SqlColumnOrPropertyRefExpressionAnalysis : SqlAnalysisBase
    {
        public SqlColumnOrPropertyRefExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            var tableRef = base.CodeObject.Children.OfType<SqlObjectIdentifier>().First();
            SQLFieldOptions fieldOptions = new SQLFieldOptions 
            {
                Schema = tableRef.SchemaName.Value,
                Name = tableRef.ObjectName.Value
            };

            if (!base.context.Fields.Any(item => item.Name.Equals(fieldOptions.Name) && item.Schema == fieldOptions.Schema))
            {
                base.context.Fields.Add(fieldOptions);
            }

            var where = (WhereOptions)base.WhereOptions;

            switch (base.CodeObject.Parent)
            {
                case var t when t is SqlAggregateFunctionCallExpression && base.CodeObject.Parent.Parent is SqlSelectScalarExpression:
                    ((SqlSelectAnalysisContext)base.context).QueryFields[^1].Schema = fieldOptions.Schema;
                    ((SqlSelectAnalysisContext)base.context).QueryFields[^1].Name = fieldOptions.Name;
                    return;

                case var t when t is SqlAggregateFunctionCallExpression && base.CodeObject.Parent.Parent is SqlColumnAssignment:
                    ((SqlUpdateAnalysisContext)base.context).Sets[^1].Value += $"{fieldOptions.Schema}.{fieldOptions.Name})";
                    return;

                case var t when t is SqlSelectScalarExpression || base.CodeObject.Parent.Parent is SqlSelectScalarExpression:
                    ((SqlSelectAnalysisContext)base.context).QueryFields.Add(new Context.Options.SQLFieldOptions
                    {
                        Schema = fieldOptions.Schema,
                        Name = fieldOptions.Name
                    });
                    break;

                case var t when t is SqlInsertSpecification:
                    ((SqlInsertAnalysisContext)base.context).Sets.Add(new SQLSetFieldOptions
                    {
                        Schema = fieldOptions.Schema,
                        Name = fieldOptions.Name
                    });
                    break;

                //case var t when t is SqlAggregateFunctionCallExpression:

                //    if (base.WhereOptions.Left == null)
                //        base.WhereOptions.Left = fieldOptions;
                //    else if (base.WhereOptions.Right == null)
                //        base.WhereOptions.Right = fieldOptions;

                //    base.GetWhereBuilder().Append($"{fieldOptions.Schema}.{fieldOptions.Name}");
                //    return;

                case var t when t is SqlOrderByItem:
                    ((SqlSelectAnalysisContext)base.context).QueryTables[^1].OrderBy.Fields.Add(new SQLFieldOptions
                    {
                        Schema = fieldOptions.Schema,
                        Name = fieldOptions.Name
                    });
                    return;

                case var t when t is SqlSimpleGroupByItem:
                    ((SqlSelectAnalysisContext)base.context).QueryTables[^1].GroupBy.Fields.Add(new SQLFieldOptions
                    {
                        Schema = fieldOptions.Schema,
                        Name = fieldOptions.Name
                    });
                    return;

                case var t when t is SqlColumnAssignment:
                    ((SqlUpdateAnalysisContext)base.context).Sets.Add(new SQLSetFieldOptions 
                    {
                        Schema = fieldOptions.Schema,
                        Name = fieldOptions.Name
                    });
                    return;

                case var t when t is SqlAggregateFunctionCallExpression:
                default:

                    if (base.WhereProperty != null)
                    {
                        // 自定义函数
                        if (base.WhereProperty is SearchIndexOptions search)
                        {
                            search.Fields.Add(fieldOptions);
                        }
                        // 有可能是带函数
                        else if (base.WhereProperty is SQLFieldOptions field)
                        {
                            field.Name = fieldOptions.Name;
                            field.Schema = fieldOptions.Schema;
                        }
                        else if(base.WhereProperty is BetweenValueOptions between)
                        {
                            where.Left = fieldOptions;
                        }
                    }
                    else
                    {
                        if (where.Left == null)
                            where.Left = fieldOptions;
                        else if (where.Right == null)
                            where.Right = fieldOptions;
                    }

                    //base.GetWhereBuilder().Append($" {fieldOptions.Schema}.{fieldOptions.Name}");
                    return;
            }
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
