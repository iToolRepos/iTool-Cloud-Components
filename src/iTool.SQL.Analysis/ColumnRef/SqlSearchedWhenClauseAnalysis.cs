﻿using iTool.SQL.Analysis.Context;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.ColumnRef
{
    public class SqlSearchedWhenClauseAnalysis : SqlAnalysisBase
    {
        public SqlSearchedWhenClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            //StringBuilder where = base.GetWhereBuilder();

            var clause = (SqlSearchedWhenClause)base.CodeObject;
            var list = clause.Children.ToList();

            base.Next(beforeAction: (index, _, analysis) =>
            {
                //if (index == 0)
                //    where.Append(" when");
                return true;
            }, afterAction: (index, code, analysis) =>
            {
                if (index == 0)
                {
                    //string nbsp = string.Empty;
                    //if (list[index + 1] is SqlLiteralExpression)
                    //    nbsp = " ";
                    //where.Append(" then" + nbsp);
                }
            });
            //base.CodeObject = clause.ThenExpression;
            //base.context.WhereBuilder.Append(" then");
            //base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
