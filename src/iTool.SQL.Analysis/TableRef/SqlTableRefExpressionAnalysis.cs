﻿using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.TableRef
{
    public class SqlTableRefExpressionAnalysis : SqlAnalysisBase
    {
        public SqlTableRefExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(beforeAction: (index, code, analysis) =>
            {
                if (index == 1)
                {
                    var key = (SqlIdentifier)code;
                    ((SqlSelectAnalysisContext)base.context).QueryTables[^1].Alias = key.Value;
                    return false;
                }
                return true;
            });
        }


        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
