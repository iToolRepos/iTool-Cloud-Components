﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.GroupByClause
{
    public class SqlSimpleGroupByItemAnalysis : SqlAnalysisBase
    {
        public SqlSimpleGroupByItemAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
