﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.GroupByClause
{
    public class SqlGroupByClauseAnalysis : SqlAnalysisBase
    {
        public SqlGroupByClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            ((SqlSelectAnalysisContext)base.context).QueryTables[^1].GroupBy = new GroupByOptions();
            //base.WhereBuilder = ((SqlSelectAnalysisContext)base.context).QueryTables[^1].GroupBy.WhereBuilder;
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
