﻿using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;
using iTool.SQL.Analysis.Context.SQLWhereOptions;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.WhereClause
{
    public class SqlBinaryBooleanExpressionAnalysis : SqlAnalysisBase
    {
        public SqlBinaryBooleanExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {

            var groupWheres = base.GetGroupWhere();

            base.WhereOptions = base.WhereOptions ?? groupWheres[^1];

            // And 和 Or 的一个处理
            //switch (((SqlBinaryBooleanExpression)base.CodeObject).Operator)
            //{
            //    case SqlBooleanOperatorType.And:
            //        break;
            //    case SqlBooleanOperatorType.Or:
            //        break;
            //}

            // 添加一个SubWhere
            if (base.CodeObject.Sql.StartsWith('('))
            {
                var sub = new SubWhereOptions();
                sub.Parent = base.WhereOptions;

                Console.WriteLine(base.WhereOptions.GetType());

                //if (base.WhereOptions is SubWhereOptions pSub)
                if (base.WhereOptions is GroupWhereOptions pGroup)
                    {
                    var group = new GroupWhereOptions();
                    group.Wheres.Add(sub);
                    pGroup.Wheres.Add(sub); 
                }
                else if(base.WhereOptions is SubWhereOptions pSub)
                {
                    var group = new GroupWhereOptions();
                    group.Wheres.Add(sub);
                    pSub.Wheres.Add(group);
                }

                base.WhereOptions = sub;
                //base.WhereOptions = groupWheres[^1].Wheres[^1];
            }

            //StringBuilder where = base.GetWhereBuilder();
            //where.Append("(");

            base.Next(beforeAction: (index, codeObject,analysis) =>
            {
                if (index == 1)
                {
                    var _operator = ((SqlBinaryBooleanExpression)base.CodeObject).Operator;
                    if (_operator == SqlBooleanOperatorType.Or)
                    {
                        var group = new GroupWhereOptions();
                        if (base.WhereOptions is GroupWhereOptions)
                        {
                            if (base.WhereOptions.Parent == null)
                            {
                                groupWheres.Add(group);
                            }
                            else
                            {
                                group.Parent = base.WhereOptions;
                                ((GroupWhereOptions)base.WhereOptions).Wheres.Add(group);
                            }
                        }
                        else if (base.WhereOptions is SubWhereOptions)
                        {
                            group.Parent = base.WhereOptions;
                            ((SubWhereOptions)base.WhereOptions).Wheres.Add(group);
                        }
                        analysis.SetWhereOptions(group);
                    }
                    //where.Append($" {_operator}");
                }
                return true;
            },whereOptions: base.WhereOptions);

            //where.Append(")");
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
