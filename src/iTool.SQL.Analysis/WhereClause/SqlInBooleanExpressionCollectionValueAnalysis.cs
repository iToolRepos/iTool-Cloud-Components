﻿using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;
using iTool.SQL.Analysis.Context.SQLWhereOptions;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.WhereClause
{
    public class SqlInBooleanExpressionCollectionValueAnalysis : SqlAnalysisBase
    {
        public SqlInBooleanExpressionCollectionValueAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            //StringBuilder where = base.GetWhereBuilder();
            var expression = (SqlInBooleanExpressionCollectionValue)base.CodeObject;
            var where = (WhereOptions)base.WhereOptions;
            where.Right = new ValueOptions[base.CodeObject.Children.Count()];

            //where.Append(" (");
            base.Next(afterAction: (index, _, analysis) =>
            {
                //where.Append(",");
            },whereOptions: base.WhereOptions);
            //where[^1] = ')';
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
