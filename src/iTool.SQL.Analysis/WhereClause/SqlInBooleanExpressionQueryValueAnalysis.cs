﻿using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.SQLWhereOptions;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTool.SQL.Analysis.WhereClause
{
    public class SqlInBooleanExpressionQueryValueAnalysis : SqlAnalysisBase
    {
        public SqlInBooleanExpressionQueryValueAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            //var expression = CodeObject as SqlInBooleanExpressionQueryValue;
            //context.WhereBuilder.Append($" in");
            // （下一个表） 子查询
            base.Next(whereOptions: base.WhereOptions);

        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
