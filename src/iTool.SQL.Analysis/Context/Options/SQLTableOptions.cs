﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.Context.Options
{
    public class SQLTableOptions
    {
        public string? Alias { get; set; }
        public string Name { get; set; }
        public string? Schema { get; set; }

        public long? Limit { get; set; }
        public long? Offset { get; set; }
        /// <summary>
        /// SqlBinaryQueryOperatorType(union ..) | SqlJoinOperatorType(join ..)
        /// </summary>
        public object? JoinOperator { get; set; }

        public SortByOptions OrderBy { get; set; }
        public GroupByOptions GroupBy { get; set; }

        //public StringBuilder WhereBuilder { get; set; } = new StringBuilder();
        public List<GroupWhereOptions> GroupWheres { get; set; } = new List<GroupWhereOptions> { new GroupWhereOptions() };

    }
}
