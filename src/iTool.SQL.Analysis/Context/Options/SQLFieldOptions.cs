﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.Options
{
    public  class SQLFieldOptions
    {
        public string Alias { get; set; }
        public string FN { get; set; }
        public string Name { get; set; }
        public string? Schema { get; set; }
        public object? Value { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(FN))
            {
                return this.Name;
            }
            else
            {
                return string.Format("{0}({1})", this.FN, this.Name);
            }
        }
    }
}
