﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.Options
{
    public enum SQLActionOptions
    {
        SELECT, UPDATE, INSERT, DELETE
    }
}
