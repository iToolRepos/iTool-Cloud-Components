﻿using iTool.SQL.Analysis.Context.SQLWhereOptions;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.Options
{
    public class WhereBase 
    {
        internal WhereBase Parent { get; set; }
    }
    public class WhereOptions : WhereBase
    {
        public object Left { get; set; }
        public object Right { get; set; }
        public ComparisonBooleanExpressionType ComparisonBooleanExpressionType { get; set; }
    }

    public class SubWhereOptions : WhereBase
    {
        public List<GroupWhereOptions> Wheres { get; set; } = new List<GroupWhereOptions>();
    }

    public class GroupWhereOptions : WhereBase
    {
        public new List<WhereBase> Wheres { get; set; } = new List<WhereBase>();
    }
}
