﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.Options
{
    public class GroupByOptions
    {
        public List<SQLFieldOptions> Fields { get; set; } = new List<SQLFieldOptions>();
        //public StringBuilder WhereBuilder { get; set; } = new StringBuilder();
        public List<GroupWhereOptions> GroupWheres { get; set; } = new List<GroupWhereOptions> { new GroupWhereOptions() };
    }
}
