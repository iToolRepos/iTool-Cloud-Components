﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.Context.Options
{
    public class SortByOptions
    {
        public List<SQLFieldOptions> Fields { get; set; } = new List<SQLFieldOptions>();
        public SqlSortOrder SortOrder { get; set; }
    }
}
