﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.Context.Options
{
    public class SQLSetFieldOptions : SQLFieldOptions
    {
        //SqlLiteralExpression
        public SqlAssignmentOperatorType Operator { get; set; }
    }
}
