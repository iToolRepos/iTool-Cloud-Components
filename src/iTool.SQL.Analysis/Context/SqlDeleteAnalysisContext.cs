﻿using iTool.SQL.Analysis.Context.Options;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context
{
    public class SqlDeleteAnalysisContext : SqlAnalysisContext
    {
        public SqlBinaryBooleanExpression? Condition { get; set; }
        public SqlDeleteAnalysisContext()
        {
            base.SQLAction = SQLActionOptions.DELETE;
        }
    }
}
