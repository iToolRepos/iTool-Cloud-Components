﻿using iTool.SQL.Analysis.Context.Options;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System.Collections.Generic;

namespace iTool.SQL.Analysis.Context
{
    public class SqlUpdateAnalysisContext : SqlAnalysisContext
    {
        public SqlBinaryBooleanExpression? Condition { get; set; }
        public List<SQLSetFieldOptions> Sets { get; set; } = new List<SQLSetFieldOptions>();
        public SqlUpdateAnalysisContext()
        {
            base.SQLAction = SQLActionOptions.UPDATE;
        }
    }
}
