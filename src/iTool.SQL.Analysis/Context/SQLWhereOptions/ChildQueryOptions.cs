﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public class ChildQueryOptions
    {
        public ComparisonBooleanExpressionType ComparisonBooleanExpressionType { get; set; }
        public string Query { get; set; }
        public string LinkTag { get; set; }
    }
}
