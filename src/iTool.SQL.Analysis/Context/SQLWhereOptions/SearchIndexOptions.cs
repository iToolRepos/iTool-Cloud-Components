﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context.Options;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    /// <summary>
    /// 自定义索引
    /// 只能在Where中使用
    /// </summary>
    public class SearchIndexOptions
    {
        public List<SQLFieldOptions> Fields { get; set; } = new List<SQLFieldOptions>();
    }
}
