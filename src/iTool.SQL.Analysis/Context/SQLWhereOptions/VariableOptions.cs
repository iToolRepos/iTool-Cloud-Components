﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public class VariableOptions //: ValueOptions
    {
        public string VariableName { get; set; }
        public override string ToString()
        {
            return VariableName;
        }
    }
}
