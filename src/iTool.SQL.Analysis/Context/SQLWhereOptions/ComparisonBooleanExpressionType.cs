﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public  static partial class ComparisonBooleanExpressionTypeExtention
    {
        public static string ToString(this ComparisonBooleanExpressionType expressionType) 
        {
            switch (expressionType)
            {
                case ComparisonBooleanExpressionType.ValueEqual:
                case ComparisonBooleanExpressionType.Equals:
                    return "=";
                case ComparisonBooleanExpressionType.LessThan:
                    return "<";
                case ComparisonBooleanExpressionType.NotEqual:
                    return "!=";
                case ComparisonBooleanExpressionType.GreaterThan:
                    return ">";
                case ComparisonBooleanExpressionType.GreaterThanOrEqual:
                    return ">=";
                case ComparisonBooleanExpressionType.LessOrGreaterThan:
                    return "<>";
                case ComparisonBooleanExpressionType.LessThanOrEqual:
                    return "<=";
                case ComparisonBooleanExpressionType.NotLessThan:
                    return ">=";
                case ComparisonBooleanExpressionType.NotGreaterThan:
                    return "<=";
                //case ComparisonBooleanExpressionType.RightStarEqualJoin:
                //    break;
                //case ComparisonBooleanExpressionType.LeftStarEqualJoin:
                //    break;
                //case ComparisonBooleanExpressionType.IsDistinctFrom:
                //    break;
                //case ComparisonBooleanExpressionType.IsNotDistinctFrom:
                //    break;
                case ComparisonBooleanExpressionType.Between:
                    return "between";
                case ComparisonBooleanExpressionType.NotBetween:
                    return "not between";
                case ComparisonBooleanExpressionType.Exists:
                    return "exists";
                case ComparisonBooleanExpressionType.NotExists:
                    return "not exists";
                case ComparisonBooleanExpressionType.In:
                    return "in";
                case ComparisonBooleanExpressionType.NotIn:
                    return "not in";
                case ComparisonBooleanExpressionType.IsNull:
                    return "is null";
                case ComparisonBooleanExpressionType.IsNotNull:
                    return "is not null";
                case ComparisonBooleanExpressionType.IsLike:
                    return "like";
                case ComparisonBooleanExpressionType.IsNotLike:
                    return "not like";
                default:
                    throw new Exception(expressionType.ToString());
            }
        }
    }

    public enum ComparisonBooleanExpressionType
    {
        Equals,
        LessThan,
        ValueEqual,
        NotEqual,
        GreaterThan,
        GreaterThanOrEqual,
        LessOrGreaterThan,
        LessThanOrEqual,
        NotLessThan,
        NotGreaterThan,
        RightStarEqualJoin,
        LeftStarEqualJoin,
        IsDistinctFrom,
        IsNotDistinctFrom,

        Between,
        NotBetween,

        Exists,
        NotExists,

        In,
        NotIn,

        IsNull,
        IsNotNull,

        IsLike,
        IsNotLike
    }
}
