﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public struct ValueOptions
    {
        public Type ValueType { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            if (this.ValueType == typeof(string))
            {
                return string.Format("'{0}'", this.Value);
            }
            else
            {
                return this.Value;
            }
        }
    }
}
