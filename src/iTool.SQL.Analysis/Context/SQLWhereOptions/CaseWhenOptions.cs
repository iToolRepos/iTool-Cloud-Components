﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context.Options;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public class WhenOptions
    {
        public List<GroupWhereOptions> WhereGroups { get; set; }
        public object Then { get; set; }
    }

    [Obsolete("暂时不支持")]
    public class CaseWhenOptions
    {
        public List<WhenOptions> WhenOptions { get; set; } = new List<WhenOptions>();
        public object Else { get; set; }
    }
}
