﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public class BetweenValueOptions
    {
        public ValueOptions Start { get; set; }
        public ValueOptions End { get; set; }

        public override string ToString()
        {
            if (this.Start.ValueType == typeof(string))
            {
                return string.Format("'{0}' and '{1}'", this.Start.Value, this.End.Value);
            }
            else
            {
                return string.Format("{0} and {1}", this.Start.Value, this.End.Value);
            }
        }
    }
}
