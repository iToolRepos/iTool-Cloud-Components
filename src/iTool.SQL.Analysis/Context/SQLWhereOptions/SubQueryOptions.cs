﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context.Options;

namespace iTool.SQL.Analysis.Context.SQLWhereOptions
{
    public class SubQueryOptions
    {
        public SubQueryOptions() { }
        public SubQueryOptions(List<SqlSelectAnalysisContext>? ChildrenQuerys) {
            this.Index = ChildrenQuerys?.Count ?? 0;
        }
        /// <summary>
        /// 子查询索引Key
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 外部依赖Fields, 比如join 等
        /// </summary>
        public List<SQLFieldOptions> DependenceFields { get; set; } = new List<SQLFieldOptions>();

        public override string ToString()
        {
            return $"$SubQuery_{Index}";
        }
    }
}
