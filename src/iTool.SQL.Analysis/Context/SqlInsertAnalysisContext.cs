﻿using iTool.SQL.Analysis.Context.Options;
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.SQL.Analysis.Context
{
    public class SqlInsertAnalysisContext : SqlAnalysisContext
    {
        public List<SQLSetFieldOptions> Sets { get; set; } = new List<SQLSetFieldOptions>();
        public SqlInsertAnalysisContext()
        {
            base.SQLAction = SQLActionOptions.INSERT;
        }
    }
}
