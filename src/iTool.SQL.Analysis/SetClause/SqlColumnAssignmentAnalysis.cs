﻿using System;
using System.Collections.Generic;
using System.Text;

using iTool.SQL.Analysis.Context;
using iTool.SQL.Analysis.Context.Options;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.Analysis.SetClause
{
    public class SqlColumnAssignmentAnalysis : SqlAnalysisBase
    {
        public SqlColumnAssignmentAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next(beforeAction: (index, code, analysis) =>
            {
                if (index == 1 && code is SqlLiteralExpression)
                {
                    ((SqlUpdateAnalysisContext)base.context).Sets[^1].Operator = ((SqlColumnAssignment)base.CodeObject).Operator;
                    ((SqlUpdateAnalysisContext)base.context).Sets[^1].Value = (SqlLiteralExpression)code;
                    return false;
                }
                return true;
            });
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
