﻿using System.Collections.Generic;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle.TableRef
{
    public class SqlTableRefExpressionAnalysis : SqlAnalysisBase
    {
        public SqlTableRefExpressionAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            // dbo.users asd 
            // users asd
            var expression = (SqlTableRefExpression)base.CodeObject;

            //expression.Alias;
            //expression.ObjectIdentifier.ObjectName;

            var tableName = expression.ObjectIdentifier.ObjectName.ToString();
            if (string.IsNullOrWhiteSpace(tableName))
            {
                throw new System.Exception("Invalid table name");
            }

            base.Context.Tables.Add(new KeyValuePair<string, string>(expression.Alias?.ToString().ToUpper(), string.Intern(tableName.ToUpper())));

            //expression.StartLocation.ColumnNumber;
            //expression.EndLocation.ColumnNumber;

            //System.Console.WriteLine(this.GetType().Name);
            //System.Console.WriteLine($"ObjectName:{expression.ObjectIdentifier.ObjectName},Alias:{expression.Alias}");
            //System.Console.WriteLine(base.CodeObject.Sql + "\n");
        }


        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
