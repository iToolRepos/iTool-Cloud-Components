﻿
using System.Collections.Generic;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle.TableRef
{
    /// <summary>
    /// 子查询
    /// </summary>
    public class SqlQuerySpecificationAnalysis : SqlAnalysisBase
    {
        public SqlQuerySpecificationAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            //System.Console.WriteLine(this.GetType());

            if (base.Context.Fields.Count > 0)
            {
                base.Context.Fields.Add(null);
                base.Context.SubIndex++;
            }
            

            base.Next();
            // -1
            base.Context.Fields.Add(new KeyValuePair<string, string>(null, null));
            if(base.Context.SubIndex>0) base.Context.SubIndex--;

        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
