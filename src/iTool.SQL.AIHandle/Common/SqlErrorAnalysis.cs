﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle.Common
{
    internal class SqlErrorAnalysis : SqlAnalysisBase
    {
        public SqlErrorAnalysis(SqlAnalysisContext context, SqlCodeObject? CodeObject = null) : base(context)
        {
            if (CodeObject != null)
            {
                base.CodeObject = CodeObject;
            }
        }

        public override void Analysis()
        {
            this.Context.Errors.AddLast("Incorrect syntax near '" + base.CodeObject.Sql + "'");
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
