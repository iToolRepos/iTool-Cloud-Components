﻿
using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle.Common
{
    public class CommonAnalysis : SqlAnalysisBase
    {
        public CommonAnalysis(SqlAnalysisContext context, SqlCodeObject? CodeObject = null) : base(context)
        {
            if (CodeObject != null)
            {
                base.CodeObject = CodeObject;
            }
        }

        public override void Analysis() => base.Next();

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
