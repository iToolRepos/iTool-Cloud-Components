﻿using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle.WhereClause
{
    public class SqlWhereClauseAnalysis : SqlAnalysisBase
    {
        public SqlWhereClauseAnalysis(SqlAnalysisContext context) : base(context)
        {
        }

        public override void Analysis()
        {
            base.Next();
        }

        public override bool Validation(SqlCodeObject codeObject) => true;
    }
}
