﻿using System;
using System.Linq;

using iTool.SQL.AIHandle.ColumnRef;
using iTool.SQL.AIHandle.Common;
using iTool.SQL.AIHandle.OrderByClause;
using iTool.SQL.AIHandle.TableRef;
using iTool.SQL.AIHandle.WhereClause;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle
{
    public class SqlAnalysisFactory
    {

        public static SqlAnalysisBase CreateStatementAnalysisInstance(string sql)
        {
            //Console.WriteLine(sql);
            var result = SqlUtils.Parse(sql);

            //if (result.Errors.Any())
            //{
            //    foreach (var item in result.Errors)
            //    {
            //        Console.WriteLine(item.Message);
            //    }
            //}

            // logger
            //SqlUtils.IterateSqlNode(result.Script);

            var statement = result.Script
                    ?.Children.OfType<SqlBatch>().FirstOrDefault();

            if (statement == null)
            {
                throw new Exception("no support:" + sql);
            }

            SQLActionOptions sQLAction;

            if (statement.Children.OfType<SqlDeleteStatement>().Any())
            {
                sQLAction = SQLActionOptions.DELETE;
            }
            else if(statement.Children.OfType<SqlInsertStatement>().Any())
            {
                sQLAction = SQLActionOptions.INSERT;
            }
            else if (statement.Children.OfType<SqlUpdateStatement>().Any())
            {
                sQLAction = SQLActionOptions.UPDATE;
            }
            else if (statement.Children.OfType<SqlSelectStatement>().Any())
            {
                sQLAction = SQLActionOptions.SELECT;
            }
            else
            {
                throw new Exception("no support:" + sql);
            }

            return new CommonAnalysis(new SqlAnalysisContext(sQLAction), statement);
        }

        public static SqlAnalysisBase GetNextSqlAnalysis(Type type, SqlAnalysisContext context)
        {

            switch (type)
            {
                case var t when t == typeof(SqlTableRefExpression):
                    return new SqlTableRefExpressionAnalysis(context);

                case var t when t == typeof(SqlColumnRefExpression):
                    return new SqlColumnRefExpressionAnalysis(context);

                case var t when t == typeof(SqlQuerySpecification):
                    return new SqlQuerySpecificationAnalysis(context);

                case var t when t == typeof(SqlWhereClause):
                    return new SqlWhereClauseAnalysis(context);

                case var t when t == typeof(SqlTableExpressionError):
                    return new SqlErrorAnalysis(context);

                case var t when t == typeof(SqlOrderByItem):
                    return new SqlOrderByItemAnalysis(context);

                case var t when t.FullName.Contains("SqlBuiltinScalarFunctionCallExpression"):
                    return new SqlBuiltinScalarFunctionCallAnalysis(context);

                case var t when t.FullName.Contains("SqlColumnOrPropertyRefExpression"):
                    return new SqlColumnOrPropertyRefExpressionAnalysis(context);

                default:
                    return new CommonAnalysis(context);
            }

        }
    }
}
