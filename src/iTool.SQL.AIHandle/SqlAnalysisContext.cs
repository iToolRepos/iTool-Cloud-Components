﻿using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.SqlServer.Management.SqlParser.SqlCodeDom;

namespace iTool.SQL.AIHandle
{
    public enum SQLActionOptions
    {
        SELECT, UPDATE, INSERT, DELETE
    }

    public enum IndexTypeOptions
    {
        OnlySize, PageAndSize, OnlyToken, TokenAndSize, None
    }

    public enum IndexModeOptions
    {
        Fields, Shoube, Must, MustNot, MustDistMap, MustNotDistMap, ShoubeDistMap
    }

    public struct SearchIndexOptions
    {
        public string Sql;
        public bool IsHasNot;
        public IndexTypeOptions IndexType;
        public List<ChildIndexFn> ChildIndexFns;
        public string Table;

        public string PreviouToken;
        public int Page;
        public int Size;

        public SearchIndexOptions(string sql, bool isHasNot, IndexTypeOptions indexType, List<ChildIndexFn> childIndexFns, string table,string previouToken,int page,int size)
        {
            this.Sql = sql;
            this.IsHasNot = isHasNot;
            this.IndexType = indexType;
            this.ChildIndexFns = childIndexFns;
            this.Table = table;
            this.PreviouToken= previouToken;
            this.Page = page;   
            this.Size = size;
        }
    }

    public class ChildIndexFn 
    {
        public ChildIndexFn()
        {
            this.Fields = new List<string>();
        }
        public IndexModeOptions IndexMode { get; set; }
        public List<string> Fields { get; set; }
        public List<string> Values { get; set; }
    }

    public class SortByOptions
    {
        public SortByOptions() 
        {
            this.Fields = new List<string>();
        }
        public string TableName { get; set; }
        public int SubIndex { get; set; }
        public string OrderCase { get; set; }
        public bool IsSortDist { get; set; }
        public List<string> Fields { get; set; }
        public List<string> Values { get; set; }
        public SqlSortOrder SortOrder { get; set; }
    }

    public class SqlAnalysisContext
    {
        public List<string> CustomFunctions { get; set; }
        public string WhereCase { get; set; }
        public List<SortByOptions> SortByOptions { get; set; }
        public SQLActionOptions SQLAction { get; set; }
        public int SubIndex { get; set; }
        
        public SqlAnalysisContext(SQLActionOptions SQLAction) 
        {
            this.SQLAction = SQLAction;
            this.Tables = new List<KeyValuePair<string, string>>();
            this.Fields = new List<KeyValuePair<string, string>?>();
            this.SetFields = new List<string>();
            
            this.Indexs = new List<SearchIndexOptions>();
            //this.Indexs = new List<(string sql, List<string> fields, string value, bool hasNot, int subIndex, IndexTypeOptions indexType, List<ChildIndexFn> childIndexFns)>();
        }
        public List<KeyValuePair<string, string>> Tables { get; set; }
        public List<KeyValuePair<string, string>?> Fields { get; set; }
        public List<string> SetFields { get; set; }

        public List<SearchIndexOptions> Indexs { get; set; }

        public LinkedList<string> Errors { get; set; } = new LinkedList<string>();
    }
}