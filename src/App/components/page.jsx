var React = require('react')

module.exports = class extends React.Component {
  render() {
    return (
      <div>
        <section className="content-header">
        </section>
        <section className="content" style={{ position: 'relative' }}>
          {this.props.children}
        </section>
      </div>
    )
  }
}
