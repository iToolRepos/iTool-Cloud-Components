var React = require('react')
const ThemeButtons = require('./theme-buttons.jsx')
const CheckboxFilter = require('./checkbox-filter.jsx')
const Panel = require('./panel.jsx')

module.exports = props => (
  <Panel title="偏好设置">
    <div>
      <p>
      以下首选项可用于自定义iTool Cloud仪表板。选定的首选项本地保存在此浏览器中。所选首选项不会影响其他浏览器或用户。
      </p>
      <p>
      为 System Service 或 Dashboard Service 选择“隐藏”选项
      将从计数器、图形和表格中排除相应类型，
      但概览页面上的集群分析图和Noder概览页面上的节点分析图除外。
      </p>
      <div
        style={{
          alignItems: 'center',
          display: 'grid',
          grid: '1fr 1fr / auto 1fr',
          gridGap: '0px 30px'
        }}
      >
        <div>
          <h4>Dashboard Service</h4>
        </div>
        <div>
          <CheckboxFilter
            onChange={props.changeSettings}
            settings={props.settings}
            preference="dashboard"
          />
        </div>
        <div>
          <h4>System Service</h4>
        </div>
        <div>
          <CheckboxFilter
            onChange={props.changeSettings}
            settings={props.settings}
            preference="system"
          />
        </div>

        {/* <div>
          <h4>Theme</h4>
        </div>
        <div>
          <ThemeButtons
            defaultTheme={props.defaultTheme}
            light={props.light}
            dark={props.dark}
          />
        </div> */}

      </div>
    </div>
  </Panel>
)
