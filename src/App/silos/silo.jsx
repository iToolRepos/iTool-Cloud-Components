const React = require('react')
const Gauge = require('../components/gauge-widget.jsx')
const PropertiesWidget = require('../components/properties-widget.jsx')
const GrainBreakdown = require('../components/grain-table.jsx')
const ChartWidget = require('../components/multi-series-chart-widget.jsx')
const Panel = require('../components/panel.jsx')
const Chart = require('../components/time-series-chart.jsx')

const SiloGraph = props => {
  const values = []
  const timepoints = []
  Object.keys(props.stats).forEach(key => {
    values.push(props.stats[key])
    timepoints.push(props.stats[key].period)
  })

  if (!values.length) {
    return null
  }

  while (values.length < 100) {
    values.unshift({ count: 0, elapsedTime: 0, period: 0, exceptionCount: 0 })
    timepoints.unshift('')
  }

  return (
    <div>
      <Chart
        timepoints={timepoints}
        series={[
          values.map(z => z.exceptionCount),
          values.map(z => z.count),
          values.map(z => (z.count === 0 ? 0 : z.elapsedTime / z.count))
        ]}
      />
    </div>
  )
}

module.exports = class Silo extends React.Component {
  hasData(value) {
    for (var i = 0; i < value.length; i++) {
      if (value[i] !== null) return true
    }
    return false
  }

  querySeries(lambda) {
    return this.props.data.map(function(x) {
      if (!x) return 0
      return lambda(x)
    })
  }

  hasSeries(lambda) {
    var hasValue = false

    for (var key in this.props.data) {
      var value = this.props.data[key]
      if (value && lambda(value)) {
        hasValue = true
      }
    }

    return hasValue
  }

  render() {
    if (!this.hasData(this.props.data)) {
      return (
        <Panel title="Error">
          <div>
            <p className="lead">No data available for this silo</p>
            <p>
              <a href="#/silos">Show all silos</a>
            </p>
          </div>
        </Panel>
      )
    }

    var last = this.props.data[this.props.data.length - 1]
    var properties = {
      客户端: last.clientCount || '0',
      '收到的消息': last.receivedMessages || '0',
      '发送的消息': last.sentMessages || '0',
      '接收队列': last.receiveQueueLength || '0',
      '请求队列': last.requestQueueLength || '0',
      '发送队列': last.sendQueueLength || '0'
    }

    var grainStats = (
      this.props.dashboardCounters.simpleGrainStats || []
    ).filter(function(x) {
      return x.siloAddress === this.props.silo
    }, this)

    var status = (this.props.dashboardCounters.hosts || {})[this.props.silo]
    var silo =
      this.props.dashboardCounters.hosts.filter(
        x => x.siloAddress === this.props.silo
      )[0] || {}

    var configuration = {
      'Host name': silo.hostName,
      'Role name': silo.roleName,
      'Noder name': silo.siloName,
      'Proxy port': silo.proxyPort,
      'Update zone': silo.updateZone,
      'Fault zone': silo.faultZone
    }

    if (this.props.siloProperties.orleansVersion) {
      configuration[
        'Orleans version'
      ] = this.props.siloProperties.orleansVersion
    }

    if (this.props.siloProperties.hostVersion) {
      configuration['Host version'] = this.props.siloProperties.hostVersion
    }

    var cpuGauge
    var memGauge

    if (this.hasSeries(x => x.cpuUsage > 0)) {
      cpuGauge = (
        <div>
          <Gauge
            value={last.cpuUsage}
            max={100}
            title="CPU Usage"
            description={Math.floor(last.cpuUsage) + '% 利用率'}
          />
          <ChartWidget series={[this.querySeries(x => x.cpuUsage)]} />
        </div>
      )
    } else {
      cpuGauge = (
        <div style={{ textAlign: 'center' }}>
          <h4>CPU Usage</h4>

          <div style={{ lineHeight: '40px' }}>No data available</div>
        </div>
      )
    }

    if (this.hasSeries(x => x.totalPhysicalMemory - x.availableMemory > 0)) {
      memGauge = (
        <div>
          <Gauge
            value={
              (last.totalPhysicalMemory || 0) - (last.availableMemory || 0)
            }
            max={last.totalPhysicalMemory || 1}
            title="Memory Usage"
            description={
              Math.floor((last.availableMemory || 0) / (1024 * 1024)) +
              ' MB 可用空间'
            }
          />
          <ChartWidget
            series={[
              this.querySeries(
                x => (x.totalPhysicalMemory - x.availableMemory) / (1024 * 1024)
              )
            ]}
          />
        </div>
      )
    } else {
      memGauge = (
        <div style={{ textAlign: 'center' }}>
          <h4>Memory Usage</h4>

          <div style={{ lineHeight: '40px' }}>No data available</div>
        </div>
      )
    }

    return (
      <div>
        <Panel title="Overview">
          <div className="row">
            <div className="col-md-4">{cpuGauge}</div>
            <div className="col-md-4">{memGauge}</div>
            <div className="col-md-4">
              <Gauge
                value={last.recentlyUsedActivationCount}
                max={last.activationCount}
                title="Services Usage"
                description={
                  last.activationCount +
                  ' 个活跃Service, 最近使用率：' +
                  Math.floor(
                    (last.recentlyUsedActivationCount * 100) /
                      last.activationCount
                  ) +
                  '%'
                }
              />
              <ChartWidget
                series={[
                  this.querySeries(x => x.activationCount),
                  this.querySeries(x => x.recentlyUsedActivationCount)
                ]}
              />
            </div>
          </div>
        </Panel>

        <Panel title="节点分析">
          <div>
            <span>
              <strong style={{ color: '#783988', fontSize: '25px' }}>/</strong>{' '}
              每秒的请求数
              <br />
              <strong style={{ color: '#EC1F1F', fontSize: '25px' }}>
                /
              </strong>{' '}
              失败的请求
            </span>
            <span className="pull-right">
              <strong style={{ color: '#EC971F', fontSize: '25px' }}>/</strong>{' '}
              平均响应时间/ms
            </span>
            <SiloGraph stats={this.props.siloStats} />
          </div>
        </Panel>

        <div className="row">
          <div className="col-md-6">
            <Panel title="节点计数器">
              <div>
                <PropertiesWidget data={properties} />
              </div>
            </Panel>
          </div>
          <div className="col-md-6">
            <Panel title="节点属性">
              <PropertiesWidget data={configuration} />
            </Panel>
          </div>
        </div>

        <Panel title="根据类型激活">
          <GrainBreakdown data={grainStats} silo={this.props.silo} />
        </Panel>
      </div>
    )
  }
}
/*

dateTime: "2015-12-30T17:02:32.6695724Z"

cpuUsage: 11.8330326
activationCount: 4
availableMemory: 4301320000
totalPhysicalMemory: 8589934592
memoryUsage: 8618116
recentlyUsedActivationCount: 2


clientCount: 0
isOverloaded: false

receiveQueueLength: 0
requestQueueLength: 0
sendQueueLength: 0

receivedMessages: 0
sentMessages: 0

*/
