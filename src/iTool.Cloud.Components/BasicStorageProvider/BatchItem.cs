﻿using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public class BatchItem<T>
    {
        public BatchItem()
        {
        }

        public T Body { get; set; }
        public TaskCompletionSource<Task> TaskSource { get; set; }
        
        public BatchItem(T body) 
        {
            this.TaskSource = new TaskCompletionSource<Task>();
            this.Body = body;
        }

        public BatchItem(T body, TaskCompletionSource<Task> source)
        {
            this.TaskSource = source;
            this.Body = body;
        }
    }
}