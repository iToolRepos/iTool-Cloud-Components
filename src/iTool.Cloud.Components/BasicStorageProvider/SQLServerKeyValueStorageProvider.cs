﻿using iTool.Common.Options;
using Newtonsoft.Json;
using Orleans.Configuration;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public class SQLServerKeyValueStorageProvider : IKeyValueStorageProvider
    {
        readonly NewtonJsonSerializerSettings serializerSettings;
        readonly AdoNetOptions options;
        string connection;

        public SQLServerKeyValueStorageProvider(AdoNetOptions options)
        {
            this.serializerSettings = new NewtonJsonSerializerSettings();
            this.options = options;
            this.connection = this.options.GetConnection();
        }

        public async Task AddOrUpdateAsync(string key, object value, Type type)
        {
            try
            {
                // 这里初始化已经调用过 GetOrAddAsync， 所以直接Update
                string valueOfString = JsonConvert.SerializeObject(value, this.serializerSettings.Create());
                await using (SqlConnection conn = new SqlConnection(this.connection))
                {
                    await using (SqlCommand command = new SqlCommand($"update ClusterCacheTable set [value] = @value where [key] = @key", conn))
                    {
                        command.Parameters.AddRange(new SqlParameter[] 
                        {
                            new SqlParameter("key",key),
                            new SqlParameter("value",valueOfString)
                        });
                        await conn.OpenAsync();
                        await command.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<T> GetAsync<T>(string key)
        {
            try
            {
                var result = await this.GetAsync(key, typeof(T));
                return (T)result.value;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<object> GetOrAddAsync(string key, Func<object> func, Type type)
        {
            try
            {
                var result = await this.GetAsync(key, type);

                // 如果存在则返回
                if (result.isExists)
                {
                    return result.value;
                }

                // 构造默认值
                var value = func.Invoke();
                string valueOfString = JsonConvert.SerializeObject(value, this.serializerSettings.Create());
                await using (SqlConnection conn = new SqlConnection(this.connection))
                {
                    await using (SqlCommand command = new SqlCommand($"insert into ClusterCacheTable([key],[value]) values(@key,@value)", conn))
                    {
                        command.Parameters.AddRange(new SqlParameter[]
                        {
                            new SqlParameter("key",key),
                            new SqlParameter("value",valueOfString)
                        });
                        await conn.OpenAsync();
                        await command.ExecuteNonQueryAsync();
                    }
                }

                return value;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task RemoveAsync(string key)
        {
            await using (SqlConnection conn = new SqlConnection(this.connection))
            {
                await using (SqlCommand command = new SqlCommand($"delete from [ClusterCacheTable] where [key] = @key", conn))
                {
                    command.Parameters.AddRange(new SqlParameter[]
                    {
                            new SqlParameter("key",key)
                    });
                    await conn.OpenAsync();
                    await command.ExecuteNonQueryAsync();
                }
            }
        }


        #region Private

        private async Task<(bool isExists, string value)> GetAsync(string key)
        {
            await using (SqlConnection conn = new SqlConnection(this.connection))
            {
                await using (SqlCommand command = new SqlCommand($"select top 1 [value] from ClusterCacheTable where [key] = @key", conn))
                {
                    command.Parameters.AddRange(new SqlParameter[]
                    {
                            new SqlParameter("key",key)
                    });
                    await conn.OpenAsync();
                    var value = (string)(await command.ExecuteScalarAsync() ?? string.Empty);

                    if (string.IsNullOrWhiteSpace(value))
                    {
                        return (false, default(string));
                    }
                    else
                    {
                        return (true, value);
                    }
                }
            }
        }

        private async Task<(bool isExists, object value)> GetAsync(string key, Type type)
        {

            var result = await this.GetAsync(key);

            if (result.value == default(string))
            {
                return (result.isExists, iUtils.DefaultForType(type));
            }

            return (result.isExists, JsonConvert.DeserializeObject(result.value, type, this.serializerSettings.Create()));
        }
        #endregion


    }
}
