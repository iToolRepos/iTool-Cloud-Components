﻿using Newtonsoft.Json;
using Orleans.Streams;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public class StateSubscribeStreamHandler : SubscribeQueueHandler<string>
    {
        Action<string> onMessage;

        public StateSubscribeStreamHandler(string topic)
            : base(topic, "MultipleStateSubscribeStreamHandler")
        {

        }

        public void SetOnMessage(Action<string> onMessage)
        {
            this.onMessage = onMessage;
        }

        public override Task OnErrorAsync(Exception ex)
        {
            return Task.CompletedTask;
        }

        public override Task OnMessageAsync(string message, StreamSequenceToken token)
        {
            Console.WriteLine("OnMessageAsync:" + message);
            this.onMessage.Invoke(message);
            return Task.CompletedTask;
        }
    }




    public static class GlobalStateManagementWithIntegerKey<T,TState>
        where TState : class
    {
        static ConcurrentDictionary<long, object> _stateCache;
        static ConcurrentDictionary<long, StateSubscribeStreamHandler> _subscribe;
        static object _lock = new object();

        static GlobalStateManagementWithIntegerKey() 
        {
            _stateCache = new ConcurrentDictionary<long, object>();
            _subscribe = new ConcurrentDictionary<long, StateSubscribeStreamHandler>();
        }

        public async static Task OnActivateAsync(long key, object state)
        {
            Console.WriteLine("OnActivateAsync:{0},{1}", key, string.Empty);
            string message = JsonConvert.SerializeObject(state);
            state = JsonConvert.DeserializeObject<TState>(message);

            if (_stateCache.ContainsKey(key))
            {
                _stateCache[key] = state;
                return;
            }
            else
            {
                _stateCache.TryAdd(key, state);
            }

            // 订阅
            if (!_subscribe.ContainsKey(key))
            {
                lock (_lock)
                {
                    if (!_subscribe.ContainsKey(key))
                    {
                        var subscribe = new StateSubscribeStreamHandler(key.ToString());
                        _subscribe.TryAdd(key, subscribe);
                    }
                    else
                    {
                        return;
                    }
                }

                _subscribe[key].SetOnMessage(message =>
                {
                    try
                    {
                        Console.WriteLine("SetOnMessage:" + message);
                        switch (message)
                        {
                            case "deactivate":
                                break;

                            default:
                                var state = JsonConvert.DeserializeObject<TState>(message);
                                _stateCache[key] = state;
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("sync state error:" + ex.Message);
                    }
                });

                await _subscribe[key].StartAsync();
                Console.WriteLine("_subscribe _" + key);
            }

        }

        public async static Task OnDeactivateAsync(long key)
        {
            if (_stateCache.ContainsKey(key))
            {
                _stateCache.TryRemove(key,out _);
            }

            try
            {
                if (_subscribe.ContainsKey(key))
                {
                    var subscribe = _subscribe[key];
                    _subscribe.TryRemove(key, out _);
                    await subscribe.UnsubscribeAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("UnsubscribeAsync:" + ex.Message);
            }
        }

        public static object ReadState(long key) 
        {
            if (_stateCache.ContainsKey(key))
            {
                return _stateCache[key];
            }
            else
            {
                return null;
            }
        }
    }
}
