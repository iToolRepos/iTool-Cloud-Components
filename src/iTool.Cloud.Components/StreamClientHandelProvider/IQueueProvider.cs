﻿using Orleans.Streams;
using System;

namespace iTool.ClusterComponent
{
    public interface IQueueProvider : IStreamProvider
    {
    }

    public class QueueProvider : IQueueProvider
    {
        private readonly IStreamProvider streamProvider;
        public string Name => this.streamProvider.Name;
        public bool IsRewindable => this.streamProvider.IsRewindable;
        public QueueProvider(IStreamProvider streamProvider) 
        {
            this.streamProvider = streamProvider;
        }

        public IAsyncStream<T> GetStream<T>(Guid streamId, string streamNamespace)
        {
            return this.streamProvider.GetStream<T>(streamId, streamNamespace);
        }
    }

}
