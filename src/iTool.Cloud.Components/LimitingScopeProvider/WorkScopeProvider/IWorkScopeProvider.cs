﻿using iTool.ClusterComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IWorkScopeProvider
    {
        string WorkScopeName { get; }

        IClusterService ClusterService { get; }

        Task<IWorkUnitProvider> CreateWorkUnitScopeAsync();

        Task NextAsync();
    }
}
