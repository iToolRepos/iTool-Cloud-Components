﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IWorkUnitProvider:IDisposable, IAsyncDisposable
    {
        IWorkScopeProvider WorkScopeProvider { get; }

        Task ApplyExcuterAsync();
    }
}
