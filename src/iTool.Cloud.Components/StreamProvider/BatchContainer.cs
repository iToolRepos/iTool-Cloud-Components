using iTool.Common;
using Orleans.Providers;
using Orleans.Providers.Streams.Common;
using Orleans.Runtime;
using Orleans.Serialization;
using Orleans.Streams;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iTool.ClusterComponent
{
    [Serializable]
    internal class BatchContainer<TSerializer> : IBatchContainer, IOnDeserialized
        where TSerializer : class, IMemoryMessageBodySerializer
    {
        [NonSerialized]
        private TSerializer serializer;
        private readonly EventSequenceToken realToken;
        public StreamSequenceToken SequenceToken => realToken;
        public MessageData MessageData { get; set; }
        public long SequenceNumber => realToken.SequenceNumber;

        public Guid StreamGuid => MessageData.Guid;

        public string StreamNamespace => MessageData.Namespace;

        // Payload is local cache of deserialized payloadBytes.  Should never be serialized as part of batch container.  During batch container serialization raw payloadBytes will always be used.
        [NonSerialized] private MemoryMessageBody payload;

        private MemoryMessageBody Payload()
        {
            this.serializer = this.serializer ?? iBox.GetService<TSerializer>("iToolProvider");
            return payload ?? (payload = serializer.Deserialize(MessageData.Payload));
        }

        public BatchContainer(MessageData messageData, TSerializer serializer)
        {
            this.serializer = serializer;
            MessageData = messageData;
            realToken = new EventSequenceToken(messageData.SequenceNumber);
        }

        public IEnumerable<Tuple<T, StreamSequenceToken>> GetEvents<T>()
        {
            return Payload().Events.Cast<T>().Select((e, i) => Tuple.Create<T, StreamSequenceToken>(e, realToken.CreateSequenceTokenForEvent(i)));
        }

        public bool ImportRequestContext()
        {
            var context = Payload().RequestContext;
            if (context != null)
            {
                RequestContextExtensions.Import(context);
                return true;
            }
            return false;
        }

        public void OnDeserialized(ISerializerContext context)
        {
            this.serializer = MessageBodySerializerFactory<TSerializer>.GetOrCreateSerializer(context.ServiceProvider);
        }

        public bool ShouldDeliver(IStreamIdentity stream, object filterData, StreamFilterPredicate shouldReceiveFunc) => true;
    }
}
