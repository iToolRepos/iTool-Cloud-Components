using Microsoft.Extensions.DependencyInjection;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Providers;
using System;

namespace iTool.ClusterComponent
{
    public interface IMStreamConfigurator : INamedServiceConfigurator { }

    public static class MemoryStreamConfiguratorExtensions
    {
        public static void ConfigurePartitioning(this IMStreamConfigurator configurator, int numOfQueues = HashRingStreamQueueMapperOptions.DEFAULT_NUM_QUEUES)
        {
            configurator.Configure<HashRingStreamQueueMapperOptions>(ob => ob.Configure(options => options.TotalQueueCount = numOfQueues));
        }

        public static void ConfigureCache(this IMStreamConfigurator configurator, int cacheSize = SimpleQueueCacheOptions.DEFAULT_CACHE_SIZE)
        {
            configurator.Configure<SimpleQueueCacheOptions>(ob => ob.Configure(options => options.CacheSize = cacheSize));
        }
    }

    public interface ISiloMStreamConfigurator : IMStreamConfigurator, ISiloRecoverableStreamConfigurator { }

    public class SiloMStreamConfigurator<TSerializer> : SiloRecoverableStreamConfigurator, ISiloMStreamConfigurator
          where TSerializer : class, IMemoryMessageBodySerializer
    {
        public SiloMStreamConfigurator(
            string name, Action<Action<IServiceCollection>> configureServicesDelegate)
            : base(name, configureServicesDelegate, AdapterFactory<TSerializer>.Create)
        {
            this.ConfigureDelegate(services =>
            {
                services.ConfigureNamedOptionForLogging<HashRingStreamQueueMapperOptions>(name);
            });
        }
    }

    public interface IClusterClientMStreamConfigurator : IMemoryStreamConfigurator, IClusterClientPersistentStreamConfigurator { }

    public class ClusterClientMStreamConfigurator<TSerializer> : ClusterClientPersistentStreamConfigurator, IClusterClientMStreamConfigurator
          where TSerializer : class, IMemoryMessageBodySerializer
    {
        public ClusterClientMStreamConfigurator(string name, IClientBuilder builder)
         : base(name, builder, AdapterFactory<TSerializer>.Create)
        {
        }
    }
}
