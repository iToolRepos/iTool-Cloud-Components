﻿
using Orleans;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IStreamQueueConsume : IGrainWithGuidCompoundKey
    {
        Task<bool> IsCanConsumeMessageAsync(string messageKey);
        Task<bool> ConsumeMessageErrorAsync(string messageKey);
    }
}
