﻿using Orleans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public class StreamQueueConsume : Orleans.Grain, IStreamQueueConsume
    {
        private List<string> list;

        public StreamQueueConsume() 
        {
            list = new List<string>(600000);
        }

        public Task<bool> ConsumeMessageErrorAsync(string messageKey)
        {
            if (list.Contains(messageKey))
            {
                return Task.FromResult(list.Remove(messageKey));
            }
            else
            {
                return Task.FromResult(false);
            }
        }

        public Task<bool> IsCanConsumeMessageAsync(string messageKey)
        {
            if (list.Contains(messageKey))
            {
                return Task.FromResult(false);
            }
            else
            {
                if (list.Count > 599000)
                {
                    list.RemoveRange(0, 200000);
                }
                list.Add(messageKey);
                return Task.FromResult(true);
            }
        }
    }
}
