﻿using Orleans;
using Orleans.Providers;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    [StorageProvider(ProviderName = "Default")]
    public class StreamSequenceNumber : Grain<long>, IStreamSequenceNumber
    {
        public async Task<long> GetSequenceNumber()
        {
            this.State++;
            await WriteStateAsync();
            return this.State;
        }
    }
}
