﻿
using Orleans;
using Orleans.Concurrency;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    /// <summary>
    /// Interface for In-memory stream queue grain.
    /// </summary>
    public interface IStreamQueueGrain : IGrainWithGuidCompoundKey
    {
        /// <summary>
        /// Enqueue an event.
        /// </summary>
        [AlwaysInterleave]
        Task Enqueue(MessageData data);

        Task<IEnumerable<MessageData>> Dequeue(int maxCount);
    }
}
