﻿using Orleans;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    /// <summary>
    /// Interface for In-memory stream queue grain.
    /// </summary>
    public interface IStreamSequenceNumber : IGrainWithIntegerKey
    {
        Task<long> GetSequenceNumber();
    }
}
