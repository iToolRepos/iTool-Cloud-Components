﻿
using Orleans;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IStreamQueueOffset : IGrainWithGuidCompoundKey
    {
        Task<long> GetNextOffset();
        Task SetNextStartOffset(long offset);
        Task ReSetOffset(long offset);
    }
}
