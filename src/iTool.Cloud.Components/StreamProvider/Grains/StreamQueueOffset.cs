﻿using Orleans;
using Orleans.Providers;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    [StorageProvider(ProviderName = "Default")]
    public class StreamQueueOffset : Grain<long>,  IStreamQueueOffset
    {
        public Task<long> GetNextOffset()
        {
            return Task.FromResult(this.State);
        }

        public async Task ReSetOffset(long offset)
        {
            this.State = offset;
            await this.WriteStateAsync();
        }

        public async Task SetNextStartOffset(long nextOffset)
        {
            this.State = nextOffset;
            await this.WriteStateAsync();
        }
    }
}
