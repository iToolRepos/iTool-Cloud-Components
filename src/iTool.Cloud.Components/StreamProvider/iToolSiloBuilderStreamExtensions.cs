﻿using Orleans;
using Orleans.Hosting;
using Orleans.Providers;
using System;

namespace iTool.ClusterComponent
{
    public static class iToolSiloBuilderStreamExtensions
    {
        public static ISiloHostBuilder UseStreams(this ISiloHostBuilder builder, string name, Action<ISiloMStreamConfigurator> configure = null)
        {
            //the constructor wire up DI with all default components of the streams , so need to be called regardless of configureStream null or not
            var memoryStreamConfiguretor = new SiloMStreamConfigurator<DefaultMemoryMessageBodySerializer>(name, configureDelegate => builder.ConfigureServices(configureDelegate));
            configure?.Invoke(memoryStreamConfiguretor);


            return builder;
        }

        //public static ISiloHostBuilder UseStreams<TStorage>(this ISiloHostBuilder builder, string name, TStorage storageProvider, Action<ISiloMStreamConfigurator> configure = null)
        //    where TStorage : class, IQueueStorageProvider
        //{
        //    builder.ConfigureServices(services => services.AddSingleton(storageProvider));
        //    //the constructor wire up DI with all default components of the streams , so need to be called regardless of configureStream null or not
        //    var memoryStreamConfiguretor = new SiloMStreamConfigurator<DefaultMemoryMessageBodySerializer>(name, configureDelegate => builder.ConfigureServices(configureDelegate));
        //    configure?.Invoke(memoryStreamConfiguretor);


        //    return builder;
        //}


        public static IClientBuilder UseStreams(this IClientBuilder builder, string name, Action<IClusterClientMStreamConfigurator> configure = null)
            //where TStorage : class, IQueueStorageProvider
        {
            //builder.ConfigureServices(services => services.AddSingleton(storageProvider));
            //the constructor wire up DI with all default components of the streams , so need to be called regardless of configureStream null or not
            var memoryStreamConfigurator = new ClusterClientMStreamConfigurator<DefaultMemoryMessageBodySerializer>(name, builder);
            configure?.Invoke(memoryStreamConfigurator);
            return builder;
        }
    }
}
