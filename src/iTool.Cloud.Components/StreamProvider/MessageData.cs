
using Orleans.Streams;
using System;

namespace iTool.ClusterComponent
{
    /// <summary>
    /// Represents the event sent and received from an In-Memory queue grain. 
    /// </summary>
    [Serializable]
    public struct MessageData
    {
        /// <summary>
        /// Stream Guid of the event data.
        /// </summary>
        //[NonSerialized]
        //public IStreamIdentity StreamId;
        //new StreamIdentity(this.Guid, this.Namespace)

        public Guid Guid;

        public string Namespace;

        /// <summary>
        /// Position of even in stream.
        /// </summary>
        public long SequenceNumber;

        /// <summary>
        /// Time message was read from message queue
        /// </summary>
        public DateTime DequeueTimeUtc;

        /// <summary>
        /// Time message was written to message queue
        /// </summary>
        public DateTime EnqueueTimeUtc;

        /// <summary>
        /// Serialized event data.
        /// </summary>
        //public ArraySegment<byte> Payload;
        public byte[] Payload;

        internal static MessageData Create(IStreamIdentity streamId, ArraySegment<byte> arraySegment)
        {
            return new MessageData
            {
                //StreamId = streamId,
                Guid = streamId.Guid,
                Namespace = streamId.Namespace,
                EnqueueTimeUtc = DateTime.UtcNow,
                Payload = arraySegment.Array
            };
        }
    }
}
