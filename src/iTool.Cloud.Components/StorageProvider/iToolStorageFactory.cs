﻿using Microsoft.Extensions.DependencyInjection;
using Orleans.Configuration;
using Orleans.Storage;
using System;

namespace iTool.ClusterComponent
{
    public class iToolStorageFactory
    {
        public IGrainStorage Create(IServiceProvider services, string providerName)
        {            
            return ActivatorUtilities.CreateInstance<iToolStorageHandel>(services, providerName);
        }
    }
}
