﻿using Orleans.Placement;
using System;

namespace iTool.ClusterComponent
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class ClusterPlacementStrategyAttribute : PlacementAttribute
    {
        public ClusterPlacementStrategyAttribute() : base(new ClusterPlacementStrategy())
        {
        }
    }

}
