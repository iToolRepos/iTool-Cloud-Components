﻿using iTool.Common.Options;
using Orleans.Runtime;
using Orleans.Runtime.Placement;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public class ClusterSynchronizationProvider : IPlacementDirector
    {
        public Task<SiloAddress> OnAddActivation(PlacementStrategy strategy, PlacementTarget target, IPlacementContext context)
        {
            // ip:port@hash
            //172.24.96.1:20000@401551942
            string targetHost = target.GrainIdentity.PrimaryKeyString;
            if (targetHost.IndexOf('/') > -1)
            {
                return Task.FromResult(SiloAddress.FromParsableString(targetHost.Split('/')[0]));
            }
            return  Task.FromResult(SiloAddress.FromParsableString(targetHost));
        }
    }
}
