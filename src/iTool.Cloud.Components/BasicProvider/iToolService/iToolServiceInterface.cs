﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.ClusterComponent
{
    /// <summary>
    /// 空主键
    /// </summary>
    public interface iToolService : IGrainWithIntegerKey
    {
    }

    /// <summary>
    /// 服务节点
    /// </summary>
    public interface iToolServiceWithNoder : IGrainWithStringKey
    {
    }

    /// <summary>
    /// int 主键
    /// </summary>
    public interface iToolServiceWithIntegerKey : IGrainWithIntegerKey
    {
    }

    /// <summary>
    /// Guid 主键
    /// </summary>
    public interface iToolServiceWithGuidKey : IGrainWithGuidKey
    {
    }

    /// <summary>
    /// String 主键
    /// </summary>
    public interface iToolServiceWithStringKey : IGrainWithStringKey
    {
    }

    /// <summary>
    /// 复合主键（int+string）
    /// </summary>
    public interface iToolServiceWithIntegerCompoundKey : IGrainWithIntegerCompoundKey
    {
    }

    /// <summary>
    /// 复合主键（guid+string）
    /// </summary>
    public interface iToolServiceWithGuidCompoundKey : IGrainWithGuidCompoundKey
    {
    }
}
