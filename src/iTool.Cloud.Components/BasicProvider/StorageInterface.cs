﻿using iTool.Common.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IKeyValueStorageProvider
    {
        Task<T> GetAsync<T>(string key);
        Task<object> GetOrAddAsync(string key, Func<object> func, Type type);
        Task AddOrUpdateAsync(string key, object value, Type type);
        Task RemoveAsync(string key);
    }

    public interface IQueueStorageProvider
    {
        string QueueId { get; set; }
        Task<IEnumerable<MessageData>> DequeueAsync(long startOffset, int maxCount);
        Task EnqueueAsync(MessageData payload);
        Task ShutdownAsync();
        IQueueStorageProvider CreateInstance(AdoNetOptions options, string queueId);

    }
}
