﻿using Orleans.Concurrency;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface ILimitingService : iToolServiceWithIntegerCompoundKey
    {

        [AlwaysInterleave]
        Task FreedOneAsync();

        Task ApplyExcuterAsync();
        Task ApplyExcuterAsync(long inputLimiting);

    }
}
