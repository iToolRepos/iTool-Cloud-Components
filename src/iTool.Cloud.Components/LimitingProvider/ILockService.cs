﻿using Orleans.Concurrency;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface ILockService : iToolServiceWithStringKey
    {
        Task GetLockAsync();

        [AlwaysInterleave]
        Task FreedLockAsync();
    }
}
