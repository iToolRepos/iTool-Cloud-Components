﻿using iTool.ClusterComponent;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace iTool.ClusterComponent
{
    public class GatewayStatisticOptions : StatisticsResult
    {
        public List<KeyValuePair<string, long>> CloudFunction { get; set; }
        public List<KeyValuePair<string, long>> HttpRequest { get; set; }
    }
}
