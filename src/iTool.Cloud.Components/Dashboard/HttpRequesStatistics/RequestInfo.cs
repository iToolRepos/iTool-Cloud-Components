﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.ClusterComponent
{
    public class RequestInfo
    {
        public RequestInfo()
        {
            this.StartTime = DateTime.UtcNow;
            this.RequestId = Guid.NewGuid();
        }

        public Guid RequestId { get; }
        private DateTime StartTime { get; }
        public TimeSpan ExcuterTime { get; private set; }
        public string Template { get; set; }
        public string URL { get; set; }
        public bool IsSuccessful { get; private set; }
        public Exception Exception { get; private set; }

        public void RequestSucceeded()
        {
            this.ExcuterTime = DateTime.UtcNow - this.StartTime;
            this.IsSuccessful = true;
        }

        public void RequestError(Exception exception) 
        {
            this.ExcuterTime = DateTime.UtcNow - this.StartTime;
            this.IsSuccessful = false;
            this.Exception = exception;
        }
    }
}
