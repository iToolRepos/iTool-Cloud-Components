﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Orleans.Concurrency;

namespace iTool.ClusterComponent
{
    public interface IHttpRequestStatisticsService : iToolService
    {
        [OneWay]
        Task RequestAsync(RequestInfo clientInfo);

        [ReadOnly]
        Task<Dictionary<string, long>> GetRequestStatisticsAsync();
    }
}
