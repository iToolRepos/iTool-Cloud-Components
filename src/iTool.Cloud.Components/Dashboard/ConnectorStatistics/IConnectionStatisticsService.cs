﻿using System.Collections.Concurrent;
using System.Threading.Tasks;

using Orleans.Concurrency;

namespace iTool.ClusterComponent
{
    /// <summary>
    /// 用户连接统计
    /// </summary>
    public interface IConnectionStatisticsService : iToolService
    {
        /// <summary>
        /// 新连接
        /// </summary>
        /// <returns></returns>
        Task Connection(string server, ConnectorInfo clientInfo);

        /// <summary>
        /// 断开连接
        /// </summary>
        /// <returns></returns>
        Task Disconnect(string server, ConnectorInfo clientInfo);

        /// <summary>
        /// 获取与服务器对应的连接数量
        /// </summary>
        /// <returns></returns>
        [ReadOnly]
        Task<ConcurrentDictionary<string, long>> GetNoderConnectCount();

        /// <summary>
        /// 获取所有连接数量
        /// </summary>
        /// <returns></returns>
        [ReadOnly]
        Task<long> GetSumCount();
        //Task<long> GetSumCount(string server);

        /// <summary>
        /// 获取指定服务器连接数量
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        //Task<long> GetConnectCountByServer(string server);

        [ReadOnly]
        Task<long> GetDayActiveCount();

        [ReadOnly]
        Task<StatisticsResult> GetStatisticsAsync();

    }
}
