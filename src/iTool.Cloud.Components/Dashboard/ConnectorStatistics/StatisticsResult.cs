﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace iTool.ClusterComponent
{

    public class StatisticsResult
    {
        /// <summary>
        /// 在线用户数量
        /// ip/count
        /// </summary>
        public long OnlineCount { get; set; }

        /// <summary>
        /// 日活用户数量
        /// </summary>
        public long DayOnlineCount { get; set; }


        /// <summary>
        /// 客户端类型
        /// browser/app/小程序
        /// </summary>
        public Dictionary<string, long> ClientTypes { get; set; }
        /// <summary>
        /// 客户端环境
        /// ios/安卓/...
        /// </summary>
        public Dictionary<string, long> PlatformTypes { get; set; }

        /// <summary>
        /// 客户端位置-省
        /// </summary>
        public Dictionary<string, long> ProvinceTypes { get; set; }

        
    }
    
}
