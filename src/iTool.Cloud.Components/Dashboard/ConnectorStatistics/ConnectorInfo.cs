﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.ClusterComponent
{
    public class ConnectorInfo
    {
        public string UUID { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Point Point { get; set; }
        /// <summary>
        /// 客户端环境
        /// </summary>
        public string Platform { get; set; }
        /// <summary>
        /// 客户端类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 来路地址
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// 客户端IP
        /// </summary>
        public string Ip { get; set; }
    }

    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
