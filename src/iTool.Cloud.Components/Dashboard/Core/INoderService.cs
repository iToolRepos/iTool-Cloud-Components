﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using Orleans.Runtime;
using iTool.ClusterComponent.Model;

namespace iTool.ClusterComponent
{
    public interface INoderService : iToolServiceWithStringKey
    {
        [OneWay]
        Task SetVersion(string orleans, string host);

        [OneWay]
        Task ReportCounters(Immutable<StatCounter[]> stats);

        Task<Immutable<Dictionary<string,string>>> GetExtendedProperties();

        Task<Immutable<SiloRuntimeStatistics[]>> GetRuntimeStatistics();

        Task<Immutable<StatCounter[]>> GetCounters();
    }
}
