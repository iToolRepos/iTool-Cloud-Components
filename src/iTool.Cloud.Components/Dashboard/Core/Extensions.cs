﻿using iTool.Clustering.Center;
using Microsoft.AspNetCore.Http;
using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;
using System;
using System.Globalization;
using System.Net;

namespace iTool.ClusterComponent
{
    public static class Extensions
    {
        public static string PrimaryKeyAsString(this GrainReference grainRef, ClusterOptions clusterOptions = null)
        {

            return (clusterOptions == null ? String.Empty : $"{clusterOptions.ToKeyString()},") +grainRef.ToKeyString();

            //if (grainRef.IsPrimaryKeyBasedOnLong()) // Long
            //{
            //    var longKey = grainRef.GetPrimaryKeyLong(out var longExt);

            //    return longExt != null ? $"{longKey}:{longExt}" : longKey.ToString();
            //}

            //var stringKey = grainRef.GetPrimaryKeyString();

            //if (stringKey == null) // Guid
            //{
            //    var guidKey = grainRef.GetPrimaryKey(out var guidExt).ToString();

            //    return guidExt != null ? $"{guidKey}:{guidExt}" : guidKey;
            //}

            //return stringKey;
        }

        public static string ToPeriodString(this DateTime value)
        {
            return value.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static string ToISOString(this DateTime value)
        {
            return value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
        }

        public static string ToValue(this PathString path)
        {
            return WebUtility.UrlDecode(path.ToString().Substring(1));
        }
    }
}
