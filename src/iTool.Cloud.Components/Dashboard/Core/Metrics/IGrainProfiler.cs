﻿using System;
using System.Runtime.CompilerServices;

namespace iTool.ClusterComponent.Metrics
{
    public interface IGrainProfiler
    {
        void Track(double elapsedMs, Type grainType, [CallerMemberName] string methodName = null, bool failed = false);
    }
}
