﻿namespace iTool.ClusterComponent.Metrics.TypeFormatting
{
    enum TokenType
    {
        TypeNameSection,
        GenericCount,
        GenericArrayStart,
        GenericArrayEnd,
        TypeArrayStart,
        TypeArrayEnd,
        GenericSeparator,
        TypeSectionSeparator
    }
}
