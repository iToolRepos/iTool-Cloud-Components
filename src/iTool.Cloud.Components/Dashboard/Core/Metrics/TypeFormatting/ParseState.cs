﻿namespace iTool.ClusterComponent.Metrics.TypeFormatting
{
    enum ParseState
    {
        TypeNameSection,
        GenericCount,
        GenericArray,
        TypeArray
    }
}
