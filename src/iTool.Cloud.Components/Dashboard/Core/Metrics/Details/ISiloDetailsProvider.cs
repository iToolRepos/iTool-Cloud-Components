﻿using System.Threading.Tasks;
using iTool.ClusterComponent.Model;

namespace iTool.ClusterComponent.Metrics.Details
{
    public interface ISiloDetailsProvider
    {
        Task<SiloDetails[]> GetSiloDetails();
    }
}