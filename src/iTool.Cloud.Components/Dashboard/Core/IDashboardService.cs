﻿using iTool.ClusterComponent.Model;
using iTool.ClusterComponent.Model.History;
using Orleans.Concurrency;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iTool.ClusterComponent
{
    public interface IDashboardService : iToolService
    {
        [OneWay]
        Task InitAsync();

        [OneWay]
        Task SubmitTracing(string siloAddress, Immutable<SiloGrainTraceEntry[]> grainCallTime);

        Task<Immutable<DashboardCounters>> GetCounters();

        Task<Immutable<Dictionary<string, long>>> GetCloudFunctionTracing();

        Task<Immutable<Dictionary<string, Dictionary<string, GrainTraceEntry>>>> GetGrainTracing(string grain);

        Task<Immutable<Dictionary<string, GrainTraceEntry>>> GetClusterTracing();

        Task<Immutable<Dictionary<string, GrainTraceEntry>>> GetSiloTracing(string address);

        Task<Immutable<Dictionary<string, GrainMethodAggregate[]>>> TopGrainMethods();

        Task<StatisticsResult> GetConnectionStatistics();
        Task<Immutable<Dictionary<string, long>>> GetHttpRequestStatistics();
    }
}