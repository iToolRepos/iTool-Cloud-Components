﻿using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using iTool.ClusterComponent.Model;

namespace iTool.ClusterComponent
{
    public interface IDashboardRemindersService : iToolService
    {
        Task<Immutable<ReminderResponse>> GetReminders(int pageNumber, int pageSize);
    }
}