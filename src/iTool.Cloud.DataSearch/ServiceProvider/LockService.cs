﻿using System.Threading.Tasks;

using iTool.ClusterComponent;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    public class LockService : iToolServiceBase, ILockService
    {
        private bool isLock = false;
        public async Task DisposeAsync()
        {
            this.isLock = false;
            //this.PlanDispose();
            await Task.CompletedTask;
        }

        public Task<bool> IsLockedAsync()
        {
            return Task.FromResult(this.isLock);
        }

        public Task<bool> ObtainAsync()
        {
            this.isLock = true;
            return Task.FromResult(this.isLock);
        }

        public void OnActivateAsync(long hash, string name)
        {
            this.isLock = false;
        }

        public async Task FlushAsync()
        {
            await this.DisposeAsync();
        }
    }
}
