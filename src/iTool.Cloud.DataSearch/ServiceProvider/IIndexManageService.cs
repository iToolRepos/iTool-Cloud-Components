﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.ClusterComponent;
using Lucene.Net.Index;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    /// <summary>
    /// 索引管理
    /// </summary>
    public interface IIndexManageService : IFactoryService, iToolServiceWithStringKey
    {
        Task<(List<string> locations, List<string> fields)> GetAllIndexFieldsAsync();
        Task<bool> IsNeedReBuildIndexByFieldsAsync(List<string> locations, params string[] fields);
        Task ReBuildAllAsync();
        Task AddIndexAsync(params long[] ids);
        Task RemoveIndexAsync(params long[] ids);
        Task ModifyIndexAsync(params long[] ids);
        Task RemoveAllAsync();
        /// <summary>
        /// 没参数就检查是否需要索引
        /// 有参数就检查 参数是否在索引中
        /// </summary>
        Task<bool> IsHasIndexAsync(params string[] fields);
        Task CommitAsync();
        Task RollbackAsync();
    }

    public struct Location 
    {
        public string longitude;
        public string latitude;
        public override string ToString()
        {
            return string.Format("{0}&{1}", this.longitude, this.latitude);
        }
    }
}
