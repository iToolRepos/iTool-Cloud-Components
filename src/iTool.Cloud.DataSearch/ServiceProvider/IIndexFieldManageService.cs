﻿using System.IO;
using System.Threading.Tasks;

using iTool.ClusterComponent;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    public interface IIndexFieldManageService : IFactoryService, iToolServiceWithIntegerCompoundKey
    {
        Task FlushBufferAsync(long position, byte[] segment, int length);
        Task<byte[]> ReadInternalAsync(long position, int offset, int length);
        Task DeleteFileAsync();
        Task FlushAsync();
        Task<long> LengthAsync();
    }
}
