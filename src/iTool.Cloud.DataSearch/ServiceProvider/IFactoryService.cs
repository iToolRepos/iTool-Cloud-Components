﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    public interface IFactoryService
    {
        void OnActivateAsync(long hash, string name);
        Task FlushAsync();
    }
}
