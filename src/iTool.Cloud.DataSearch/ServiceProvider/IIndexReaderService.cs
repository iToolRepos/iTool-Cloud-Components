﻿using System.Collections.Generic;
using System.Threading.Tasks;

using iTool.ClusterComponent;
using iTool.SQL.AIHandle;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    /// <summary>
    /// 索引查询
    /// </summary>
    public interface IIndexReaderService : IFactoryService, iToolServiceWithStringKey
    {
        Task<SearchResponse> MultipleConditionsAsync(SearchIndexOptions searchIndex, SortByOptions? SortByOptions, params ChildIndexFn[] searchs);
        Task<SearchResponse> ContainsAsync(SearchIndexOptions searchIndex, string keyword, params string[] fields);
        Task<SearchResponse> LeftContainsAsync(SearchIndexOptions searchIndex, string keyword, params string[] fields);
        Task<SearchResponse> RightContainsAsync(SearchIndexOptions searchIndex, string keyword, params string[] fields);
    }
}
