﻿using System.Threading.Tasks;

using iTool.ClusterComponent;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    public interface ILuceneDirectoryService : IFactoryService, iToolServiceWithIntegerKey
    {
        Task<string[]> ListAllAsync();
        Task<bool> FileExistsAsync(string name);
        Task DeleteFileAsync(string name);
        Task AddFileAsync(string name);
    }
}
