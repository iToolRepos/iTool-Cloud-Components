﻿using System.Threading.Tasks;

using iTool.ClusterComponent;

namespace iTool.Cloud.DataSearch.ServiceProvider
{
    public interface ILockService : IFactoryService, iToolServiceWithIntegerCompoundKey
    {
        Task<bool> IsLockedAsync();
        Task<bool> ObtainAsync();
        Task DisposeAsync();
    }
}
