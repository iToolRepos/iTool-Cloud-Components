﻿using System;
using System.Buffers;
using System.Threading;

using iTool.Cloud.DataSearch.ServiceProvider;
using iTool.Common;

using Lucene.Net.Store;

using Orleans;

namespace iTool.Cloud.DataSearch.DirectoryProvider
{
    /// <summary>
    /// 写入数据
    /// </summary>
    public class iToolCloudIndexOutput : BufferedIndexOutputX
    {
        public override long Length 
        {
            get 
            {
                try
                {
                    var service = this.iIndexOutputService.LengthAsync();
                    return service.Result;
                }
                catch (Exception)
                {
                    Thread.Sleep(10);
                    this.iIndexOutputService =
                        this.isLocalService ? IDirectoryServiceFactory<IndexFieldManageService>.GetService(tableNameHash, fieldName) :
                        this.iCluster.GetGrain<IIndexFieldManageService>(this.tableNameHash, fieldName);
                    return this.Length;
                }
            }
        }

        readonly IClusterClient iCluster;
        private IIndexFieldManageService iIndexOutputService;
        readonly long tableNameHash;
        readonly string fieldName;
        private readonly bool isLocalService;


        public iToolCloudIndexOutput(long tableNameHash, string fieldName, IOContext context,int? estimatedSize, bool isLocalService) 
            : base(estimatedSize == null ? 1024_0_0 : (estimatedSize > 1024_0_0 ? 1024_0_0 : (int)estimatedSize))
        {
            this.isLocalService = isLocalService;
            this.tableNameHash = tableNameHash;
            this.fieldName = fieldName;
            this.iCluster = iBox.GetService<IClusterClient>("IClusterService");
            this.iIndexOutputService =
                        this.isLocalService ? IDirectoryServiceFactory<IndexFieldManageService>.GetService(tableNameHash, fieldName) :
                this.iCluster.GetGrain<IIndexFieldManageService>(this.tableNameHash, fieldName);
        }


        /// <summary>
        /// 写入数据
        /// </summary>
        /// <param name="b">All</param>
        /// <param name="offset">开始索引位置</param>
        /// <param name="len">数据长度</param>
        protected internal override void FlushBuffer(byte[] b, int offset, int len)
        {
            if (b.Length == 0 || len == 0)
            {
                return;
            }

            var segment = new byte[len];
            Buffer.BlockCopy(b, offset, segment, 0, len);
            try
            {
                this.iIndexOutputService.FlushBufferAsync(base.Position, segment, len).Wait();
            }
            catch (Exception ex)
            {
                Thread.Sleep(10);
                this.iIndexOutputService =
                    this.isLocalService ? IDirectoryServiceFactory<IndexFieldManageService>.GetService(tableNameHash, fieldName) :
                    this.iCluster.GetGrain<IIndexFieldManageService>(tableNameHash, fieldName);
                this.FlushBuffer(b, offset, len);
                Console.WriteLine("Exception" + ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if(this.isLocalService) IDirectoryServiceFactory<IndexFieldManageService>.Dispose(tableNameHash, fieldName);
            base.Dispose(disposing);
            //await this.iIndexOutputService.DisposeAsync();
        }

        [Obsolete]
        public override void Seek(long pos)
        {
            Console.WriteLine("Seek:" + pos);
        }
    }
}
