﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.DataSearch
{
    public struct SearchResponse
    {
        public long[] Keys { get; set; }
        // 索引结果分页
        public int TotalHits { get; set; }
        // Last Doc Item
        public SearchScoreDocItemOptions? LastScoreDocItem { get; set; }
    }
}
