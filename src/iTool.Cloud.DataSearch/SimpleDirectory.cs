﻿
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

using iTool.Cloud.DataSearch.DirectoryProvider;

using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Index.Extensions;
using Lucene.Net.QueryParsers.Classic;
using Lucene.Net.Search;
using Lucene.Net.Util;

using static Lucene.Net.Documents.Field;

namespace iTool.Cloud.DataSearch
{
    public static class SimpleDirectory
    {

        public static void SetIndexSql()
        {

            var connectionstring = "Data Source=42.193.106.232,2433;database=LePin;uid=sa;pwd=zhuJIAN320;MultipleActiveResultSets=true;";

            var analyzer = new JieBaAnalyzer(TokenizerMode.Search);
            var directory = new iToolCloudDirectory("test", true);
            var indexWriter = new IndexWriter(directory, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));

            Console.WriteLine("write Document");
            var old = DateTime.Now;

            using (SqlConnection connection = new SqlConnection(connectionstring))
            {
                connection.Open();

                for (int i = 0; i < 10_000; i++)
                {
                    using (SqlCommand command = new SqlCommand(@"SELECT  [id],[title],[info] FROM [LePin].[dbo].[LePin_JobInfo]", connection))
                    {
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Document doc = new Document();
                            doc.Add(new Int32Field("id", reader.GetInt32(0), Store.YES));
                            doc.Add(new TextField("title", reader.GetString(1), Field.Store.NO));
                            doc.Add(new TextField("info", reader.GetString(2), Field.Store.NO));
                            indexWriter.AddDocument(doc);
                        }

                        indexWriter.Flush(true, true);
                        indexWriter.Dispose(true);
                        indexWriter = new IndexWriter(directory, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));
                    }
                }
            }

            Console.WriteLine((DateTime.Now - old).TotalMilliseconds);

        }

        public static void reader(string search)
        {
            Console.WriteLine("write Document");
            var old = DateTime.Now;

            var directory = new iToolCloudDirectory("test", true);
            var analyzer = new JieBaAnalyzer(TokenizerMode.Search);
            using (var indexer = DirectoryReader.Open(directory))
            {
                var searcher = new IndexSearcher(indexer);

                Console.WriteLine("indexer.NumDocs:" + indexer.NumDocs);

                //var qp = new QueryParser(LuceneVersion.LUCENE_48, "title", analyzer);

                MultiFieldQueryParser queryParser = new MultiFieldQueryParser(LuceneVersion.LUCENE_48, new string[] { "title", "info" }, analyzer);

                var query = queryParser.Parse(search);

                Console.WriteLine(string.Format("query> {0}", query.ToString()).Trim());

                var tds = searcher.Search(query, 20);

                Console.WriteLine("TotalHits: " + tds.TotalHits);
                foreach (var sd in tds.ScoreDocs)
                {
                    Console.WriteLine(sd.Score);
                    var doc = searcher.Doc(sd.Doc);
                    var id = doc.Get("id");
                    Console.WriteLine(id);
                }
            }

            Console.WriteLine((DateTime.Now - old).TotalMilliseconds);
        }

        public static void test()
        {
            Lucene.Net.Store.Directory directory = new iToolCloudDirectory("test", true);
            //directory = new Lucene.Net.Store.SimpleFSDirectory("./XXXX");
            Analyzer analyzer = new JieBaAnalyzer(TokenizerMode.Search);

            var config = new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer);
            config.SetRAMBufferSizeMB(100.0);
            //NoMergeScheduler、SerialMergeScheduler、ConcurrentMergeScheduler
            config.SetMergeScheduler(NoMergeScheduler.INSTANCE);
            config.SetMergePolicy(NoMergePolicy.COMPOUND_FILES);
            config.SetMaxBufferedDocs(20000);

            var indexWriter = new IndexWriter(directory, config);



            Console.WriteLine("write Document");
            var old1 = DateTime.Now;
            var old = DateTime.Now;
            object _lock = new object();
            long indexxx = 0;
            Parallel.For(200_000_000, 200_100_000, new ParallelOptions { MaxDegreeOfParallelism = 1 }, index =>
            {
                Document doc = new Document();
                doc.Add(new Int64Field("id", index * 1_000_000_000, Store.YES));
                doc.Add(new TextField("title", "比如按照某一个字段进行排序等功能,lucene又在3.x引入了docvalues 正排索引的概念,为了支持数值查询或者地理位置查询等多维度的查询," + index, Field.Store.NO));
                lock (_lock)
                {
                    indexWriter.AddDocument(doc);
                    indexxx++;
                }
                if (indexxx % 20000 == 0)
                {
                    lock (_lock)
                    {
                        indexWriter.Flush(false, true);
                        indexWriter.Dispose(false);
                        Console.WriteLine("index:" + indexxx + ",,,,,,TotalMilliseconds:" + (DateTime.Now - old).TotalMilliseconds);
                        old = DateTime.Now;

                        var config1 = new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer);
                        config1.SetRAMBufferSizeMB(100.0);
                        var scheduler = new ConcurrentMergeScheduler();
                        scheduler.SetMaxMergesAndThreads(8, 8);
                        scheduler.SetMergeThreadPriority(4);
                        config.SetMergeScheduler(scheduler);
                        // config.SetMergePolicy(NoMergePolicy.COMPOUND_FILES);
                        config1.SetMaxBufferedDocs(20000);

                        indexWriter = new IndexWriter(directory, config1);
                    }
                }
            });

            indexWriter.Flush(false, true);
            indexWriter.Dispose(false);

            Console.WriteLine("TotalMilliseconds:" + (DateTime.Now - old1).TotalMilliseconds);


            using (var indexer = DirectoryReader.Open(directory))
            {
                var searcher = new IndexSearcher(indexer);

                Console.WriteLine("indexer.NumDocs:" + indexer.NumDocs);

                var qp = new QueryParser(LuceneVersion.LUCENE_48, "title", new JieBaAnalyzer(TokenizerMode.Search));
                var query = qp.Parse("需要排序等功能");

                Console.WriteLine(string.Format("query> {0}", query.ToString()).Trim());

                var tds = searcher.Search(query, 20);

                Console.WriteLine("TotalHits: " + tds.TotalHits);
                foreach (var sd in tds.ScoreDocs)
                {
                    Console.WriteLine(sd.Score);
                    var doc = searcher.Doc(sd.Doc);
                    var id = doc.Get("id");
                    Console.WriteLine(id);
                }
            }

        }

        public static void test1()
        {
            Lucene.Net.Store.Directory directory = new iToolCloudDirectory("test1", true);
            Analyzer analyzer = new JieBaAnalyzer(TokenizerMode.Search);
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer);
            //indexWriterConfig.SetMaxBufferedDocs(2000);
            var indexWriter = new IndexWriter(directory, indexWriterConfig);
            if (indexWriter.NumDocs == 0)
            {
                Document doc = new Document();
                doc.Add(new Int64Field("id", 0, Store.YES));
                indexWriter.AddDocument(doc);
                indexWriter.Commit();
                //indexWriter.Flush(true, true);
            }
        }

        public static void reader1(string search)
        {
            Lucene.Net.Store.Directory directory = new iToolCloudDirectory("test1", true);
            var analyzer = new JieBaAnalyzer(TokenizerMode.Search);
            var reader = DirectoryReader.Open(directory);
            var searcher = new IndexSearcher(reader);

            MultiFieldQueryParser queryParser = new MultiFieldQueryParser(LuceneVersion.LUCENE_48, new string[] { "title", "info" }, analyzer);
            var query = queryParser.Parse(search);
            Console.WriteLine(string.Format("query> {0}", query.ToString()).Trim());
            var tds = searcher.Search(query, 20);
            Console.WriteLine("TotalHits: " + tds.TotalHits);
            foreach (var sd in tds.ScoreDocs)
            {
                Console.WriteLine(sd.Score);
                var doc = searcher.Doc(sd.Doc);
                var id = doc.Get("id");
                Console.WriteLine(id);
            }
        }
    }
}
