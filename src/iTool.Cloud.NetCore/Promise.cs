﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.NetCore
{
    public class Promise
    {

        private event EventHandler<string> ThenAction;
        private event EventHandler<string> CacheAction;

        public Promise Then(Action<string> action) 
        {
            this.ThenAction += (e,msg) => action.Invoke(msg);
            return this;
        }


        public Promise Cache(Action<string> action)
        {
            this.CacheAction += (e, msg) => action.Invoke(msg);
            return this;
        }


        internal void InvokeThen(string body)
        {
            this.ThenAction?.Invoke(null,body);
        }


        internal void InvokeCache(string info)
        {
            this.CacheAction?.Invoke(null,info);

            if (this.CacheAction == null)
            {
                throw new Exception(info);
            }
        }


    }
}
