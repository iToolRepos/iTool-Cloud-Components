﻿using iTool.Utils;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace iTool.Cloud.NetCore
{


    public abstract class AbstractiToolClientInfo : AbstractiToolCommand
    {

        public ConnectorInfo ConnectorInfo { get; set; }

        private string BAIDU_AK = "2bGfqiQp4Nx4cqhSVGIB6ojeiOGGcPYV";
        private string BAIDU_URL = "https://api.map.baidu.com/location/ip";

        private string GAODE_AK = "ebe633c52cb24ad01d9f624eeacffc1f";
        private string GAODE_URL = "https://restapi.amap.com/v3/ip";


        protected async Task Ini()
        {
            this.Token = await this.GetLocalStorageAsync<string>("_wstoken");

            this.ConnectorInfo = await this.GetLocalStorageAsync<ConnectorInfo>("itool_client_info");

            if (this.ConnectorInfo == default(ConnectorInfo) || string.IsNullOrWhiteSpace(this.ConnectorInfo.City))
            {
                await this.GaoDeMapByIp();
                await this.SetLocalStorageAsync("itool_client_info", this.ConnectorInfo.TryToJson());
            }
        }


        private async Task GaoDeMapByIp() 
        {
            try
            {
                HttpClient client = new HttpClient();

                var response = await client.GetAsync($"{GAODE_URL}?key={GAODE_AK}");
                var result = await response.Content.ReadAsStringAsync();
                var address = result.TryToEntity<GaoDeAddress>();
                var clientInfo = await this.GetClientPlatformAndType();

                if (address.status == "1")
                {
                    this.ConnectorInfo = new ConnectorInfo
                    {
                        UUID = Guid.NewGuid().ToString(),
                        City = address.city,
                        Province = address.province,
                        Point = null,
                        Platform = clientInfo.platform,  // "ios/android/windows"
                        Type = clientInfo.type // "minapp"
                    };
                }
                else
                {
                    this.ConnectorInfo = new ConnectorInfo
                    {
                        UUID = Guid.NewGuid().ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                this.ConnectorInfo =  new ConnectorInfo
                {
                    UUID = Guid.NewGuid().ToString()
                };
            }
        }

        private async Task<ConnectorInfo> BaiduMapByIp()
        {
            try
            {
                HttpClient client = new HttpClient();

                var response = client.GetAsync($"{BAIDU_URL}?ak={BAIDU_AK}&coor=bd09ll").Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var address = result.TryToEntity<Address>();
                var clientInfo = await this.GetClientPlatformAndType();

                if (address.status == 0)
                {
                    return new ConnectorInfo
                    {
                        UUID = Guid.NewGuid().ToString(),
                        City = address.content.address_detail.city,
                        Province = address.content.address_detail.province,
                        Point = address.content.point,
                        Platform = clientInfo.platform,  // "ios/android/windows"
                        Type = clientInfo.type // "minapp"
                    };
                }
                else
                {
                    return new ConnectorInfo
                    {
                        UUID = Guid.NewGuid().ToString()
                    };
                }
            }
            catch (Exception)
            {
                return new ConnectorInfo
                {
                    UUID = Guid.NewGuid().ToString()
                };
            }
        }


        // 获取Token


        // 获取地理位置

    }






    public class ConnectorInfo
    {
        public string UUID { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 坐标
        /// </summary>
        public Point Point { get; set; }
        /// <summary>
        /// 客户端环境
        /// </summary>
        public string Platform { get; set; }
        /// <summary>
        /// 客户端类型
        /// </summary>
        public string Type { get; set; }
    }

    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }
    }


    public class GaoDeAddress
    {
        public string adcode { get; set; }
        public string city { get; set; }
        public string info { get; set; }
        public string infocode { get; set; }
        public string province { get; set; }
        public string rectangle { get; set; } // "120.8397067,30.77980118;122.1137989,31.66889673"
        public string status { get; set; }
    }

    class Address
    {
        public int status { get; set; }
        public AddressContent content { get; set; }
    }

    class AddressContent
    {
        public string address { get; set; }
        public AddressDetail address_detail { get; set; }
        public Point point { get; set; }
    }

    class AddressDetail
    {
        public string city { get; set; }
        public string province { get; set; }
    }
}
