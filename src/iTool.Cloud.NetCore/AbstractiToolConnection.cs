﻿using iTool.Common.Responses;
using iTool.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.NetCore
{
    public class TaskBody 
    {
        public string Token { set; get; }
        public object Promise { set; get; }
        public object Body { set; get; }
    }


    public abstract class AbstractiToolConnection
    {
        static long taskIndex = 10_000;
        static object taskIndexLock = new object();

        public abstract event EventHandler<string> OnReceiveEvent;

        public string Token { get; set; }
        protected ConcurrentQueue<TaskBody> Tasks { get; set; }
        protected ConcurrentDictionary<string, TaskBody> PendingTasks { get; set; }

        public AbstractiToolConnection()
        {
            this.Tasks = new ConcurrentQueue<TaskBody>();
            this.PendingTasks = new ConcurrentDictionary<string, TaskBody>();
            this.WhileTaskAsync();
            this.OnReceiveEvent += (e, msg) =>
            {
                var entity = msg.TryToEntity<DoWorkResponse>();

                if (entity == null) return;

                // 用户登录 或者切换登录
                if (!string.IsNullOrWhiteSpace(entity.WSToken))
                {
                    this.Token = entity.WSToken;
                    this.SetLocalStorageAsync("_wstoken", this.Token);
                }

                // 退出登录
                if (entity.IsOutLogin)
                {
                    this.Token = string.Empty;
                    this.SetLocalStorageAsync("_wstoken", this.Token);
                }

                if (this.PendingTasks.TryGetValue(entity.Token, out TaskBody taskBody))
                {
                    if (entity.Code == Common.Models.CodeResult.SUCCESS)
                    {
                        if (taskBody.Promise is Promise promise)
                        {
                            promise.InvokeThen(entity.Body.GetType() == typeof(string)  ? (string)entity.Body : entity.Body.TryToJson());
                        }
                        else
                        {
                            ((TaskCompletionSource<string>)taskBody.Promise).TrySetResult(entity.Body.GetType() == typeof(string) ? (string)entity.Body : entity.Body.TryToJson());
                        }
                    }
                    else
                    {
                        if (taskBody.Promise is Promise promise)
                        {
                            promise.InvokeCache(entity.Info);
                        }
                        else
                        {
                            ((TaskCompletionSource<string>)taskBody.Promise).TrySetException(new Exception(entity.Info));
                        }

                    }
                }
            };
        }

        public abstract Task<bool> SendAsync(string msg);

        public abstract Task<T> GetLocalStorageAsync<T>(string key);
        public abstract Task SetLocalStorageAsync(string key, string value);
        public abstract Task<(string platform, string type)> GetClientPlatformAndType();
        public abstract Task<string> AESEncryptAsync(string key, string input);


        private async void WhileTaskAsync()
        {
            while (true)
            {
                while (this.Tasks.TryDequeue(out TaskBody taskBody))
                {
                    if (await this.SendAsync(taskBody.Body.TryToJson()))
                    {
                        this.PendingTasks.TryAdd(taskBody.Token, taskBody);
                    }
                    else
                    {
                        this.Tasks.Enqueue(taskBody);
                        await Task.Delay(300);
                    }
                }

                await Task.Delay(100);
            }
        }

        protected string BuildTaskToken() 
        {
            lock (taskIndexLock)
            {
                if (taskIndex > 100_000)
                {
                    taskIndex = 10_000;
                }
                taskIndex++;
            }
            return Guid.NewGuid()+taskIndex.ToString();
        }

    }
}
