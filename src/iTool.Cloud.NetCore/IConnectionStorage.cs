﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.NetCore
{
    public interface IConnectionStorage
    {
        Task<string> AESEncryptAsync(string key, string input);
        Task<(string platform, string type)> GetClientPlatformAndType();
        Task<T> GetLocalStorageAsync<T>(string key);
        Task SetLocalStorageAsync(string key, string value);
    }
}
