﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.NetCore
{
    public class iToolConfiguration
    {
        public iToolConfiguration(string host) 
        {
            this.GatewayHost = host + "/connector";
        }

        public iToolConfiguration(string host, bool ssl)
        {
            this.UsedSSL = ssl;
            this.GatewayHost = host + "/connector";
        }

        private bool UsedSSL { get; set; }

        private string GatewayHost { get; set; }


        public string GetConnactionUrlOfString()
        {
            return $"{(this.UsedSSL ? "wss" : "ws")}://{this.GatewayHost}";
        }

    }
}
