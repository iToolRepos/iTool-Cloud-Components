﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTool.Cloud.NetCore
{
    public enum ApiJsonType
    {
        Get,
        Register,
        Modify,
        Remove
    }
}
