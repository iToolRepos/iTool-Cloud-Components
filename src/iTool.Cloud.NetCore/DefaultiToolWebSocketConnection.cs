﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iTool.Cloud.NetCore
{
    public class DefaultiToolWebSocketConnection : AbstractiToolWebSocketConnection
    {
        private readonly IConnectionStorage connectionStorage;

        public DefaultiToolWebSocketConnection(IConnectionStorage connectionStorage, iToolConfiguration configuration) : base(configuration.GetConnactionUrlOfString())
        {
            this.connectionStorage = connectionStorage;
        }

        public override Task<string> AESEncryptAsync(string key, string input) => this.connectionStorage.AESEncryptAsync(key, input);

        public override Task<(string platform, string type)> GetClientPlatformAndType() => this.connectionStorage.GetClientPlatformAndType();

        public override Task<T> GetLocalStorageAsync<T>(string key) => this.connectionStorage.GetLocalStorageAsync<T>(key);

        public override Task SetLocalStorageAsync(string key, string value) => this.connectionStorage.SetLocalStorageAsync(key, value);
    }
}
